/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity Comment.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/Comment")
public class CommentRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Comment BO.  if not key provided, calls create, otherwise calls save
     * @param		Comment comment
     * @return		Comment
     */
	@RequestMapping("/save")
    public Comment save( @RequestBody Comment comment )
    {
    	// assign locally
    	this.comment = comment;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Comment BO
     * @param		Long commentId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="comment.commentId", required=false) Long commentId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	CommentBusinessDelegate delegate = CommentBusinessDelegate.getCommentInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new CommentPrimaryKey( commentId ) );
        		LOGGER.info( "CommentController:delete() - successfully deleted Comment with key " + comment.getCommentPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new CommentPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "CommentController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "CommentController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Comment BO
     * @param		Long commentId
     * @return		Comment
     */    
    @RequestMapping("/load")
    public Comment load( @RequestParam(value="comment.commentId", required=true) Long commentId )
    {    	
        CommentPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Comment.load pk is " + commentId );
        	if ( commentId != null )
        	{
        		pk = new CommentPrimaryKey( commentId );
        		
        		// load the Comment
	            this.comment = CommentBusinessDelegate.getCommentInstance().getComment( pk );
	            
	            LOGGER.info( "CommentController:load() - successfully loaded - " + this.comment.toString() );             
			}
			else
			{
	            LOGGER.info( "CommentController:load() - unable to locate the primary key as an attribute or a selection for - " + comment.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "CommentController:load() - failed to load Comment using Id " + commentId + ", " + exc.getMessage() );
            return null;
        }

        return comment;

    }

    /**
     * Handles loading all Comment business objects
     * @return		List<Comment>
     */
    @RequestMapping("/loadAll")
    public List<Comment> loadAll()
    {                
        List<Comment> commentList = null;
        
    	try
        {                        
            // load the Comment
            commentList = CommentBusinessDelegate.getCommentInstance().getAllComment();
            
            if ( commentList != null )
                LOGGER.info(  "CommentController:loadAllComment() - successfully loaded all Comments" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "CommentController:loadAll() - failed to load all Comments - " + exc.getMessage() );
        	return null;
            
        }

        return commentList;
                            
    }


// findAllBy methods


    /**
     * save Source on Comment
     * @param		Long	commentId
     * @param		Long	childId
     * @param		Comment comment
     * @return		Comment
     */     
	@RequestMapping("/saveSource")
	public Comment saveSource( @RequestParam(value="comment.commentId", required=true) Long commentId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody Comment comment )
	{
		if ( load( commentId ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			CommentBusinessDelegate parentDelegate = CommentBusinessDelegate.getCommentInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "CommentController:saveSource() failed to get Referrer using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			comment.setSource( child );
		
			try
			{
				// save it
				parentDelegate.saveComment( comment );
			}
			catch( Exception exc )
			{
				LOGGER.info( "CommentController:saveSource() failed saving parent Comment - " + exc.getMessage() );
			}
		}
		
		return comment;
	}

    /**
     * delete Source on Comment
     * @param		Long commentId
     * @return		Comment
     */     
	@RequestMapping("/deleteSource")
	public Comment deleteSource( @RequestParam(value="comment.commentId", required=true) Long commentId )
	
	{
		if ( load( commentId ) == null )
			return( null );

		if ( comment.getSource() != null )
		{
			ReferrerPrimaryKey pk = comment.getSource().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			comment.setSource( null );
			try
			{
				CommentBusinessDelegate parentDelegate = CommentBusinessDelegate.getCommentInstance();

				// save it
				comment = parentDelegate.saveComment( comment );
			}
			catch( Exception exc )
			{
				LOGGER.info( "CommentController:deleteSource() failed to save Comment - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "CommentController:deleteSource() failed  - " + exc.getMessage() );
			}
		}
		
		return comment;
	}
	


    /**
     * Handles creating a Comment BO
     * @return		Comment
     */
    protected Comment create()
    {
        try
        {       
			this.comment = CommentBusinessDelegate.getCommentInstance().createComment( comment );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "CommentController:create() - exception Comment - " + exc.getMessage());        	
        	return null;
        }
        
        return this.comment;
    }

    /**
     * Handles updating a Comment BO
     * @return		Comment
     */    
    protected Comment update()
    {
    	// store provided data
        Comment tmp = comment;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	comment.copyShallow( tmp );
    	
        try
        {                        	        
			// create the CommentBusiness Delegate            
			CommentBusinessDelegate delegate = CommentBusinessDelegate.getCommentInstance();
            this.comment = delegate.saveComment( comment );
            
            if ( this.comment != null )
                LOGGER.info( "CommentController:update() - successfully updated Comment - " + comment.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "CommentController:update() - successfully update Comment - " + exc.getMessage());        	
        	return null;
        }
        
        return this.comment;
        
    }


    /**
     * Returns true if the comment is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( comment != null && comment.getCommentPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Comment load()
    {
    	return( load( new Long( comment.getCommentPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Comment comment = null;
    private static final Logger LOGGER = Logger.getLogger(Comment.class.getName());
    
}


