/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity Answer.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/Answer")
public class AnswerRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Answer BO.  if not key provided, calls create, otherwise calls save
     * @param		Answer answer
     * @return		Answer
     */
	@RequestMapping("/save")
    public Answer save( @RequestBody Answer answer )
    {
    	// assign locally
    	this.answer = answer;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Answer BO
     * @param		Long answerId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="answer.answerId", required=false) Long answerId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	AnswerBusinessDelegate delegate = AnswerBusinessDelegate.getAnswerInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new AnswerPrimaryKey( answerId ) );
        		LOGGER.info( "AnswerController:delete() - successfully deleted Answer with key " + answer.getAnswerPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new AnswerPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "AnswerController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AnswerController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Answer BO
     * @param		Long answerId
     * @return		Answer
     */    
    @RequestMapping("/load")
    public Answer load( @RequestParam(value="answer.answerId", required=true) Long answerId )
    {    	
        AnswerPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Answer.load pk is " + answerId );
        	if ( answerId != null )
        	{
        		pk = new AnswerPrimaryKey( answerId );
        		
        		// load the Answer
	            this.answer = AnswerBusinessDelegate.getAnswerInstance().getAnswer( pk );
	            
	            LOGGER.info( "AnswerController:load() - successfully loaded - " + this.answer.toString() );             
			}
			else
			{
	            LOGGER.info( "AnswerController:load() - unable to locate the primary key as an attribute or a selection for - " + answer.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "AnswerController:load() - failed to load Answer using Id " + answerId + ", " + exc.getMessage() );
            return null;
        }

        return answer;

    }

    /**
     * Handles loading all Answer business objects
     * @return		List<Answer>
     */
    @RequestMapping("/loadAll")
    public List<Answer> loadAll()
    {                
        List<Answer> answerList = null;
        
    	try
        {                        
            // load the Answer
            answerList = AnswerBusinessDelegate.getAnswerInstance().getAllAnswer();
            
            if ( answerList != null )
                LOGGER.info(  "AnswerController:loadAllAnswer() - successfully loaded all Answers" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "AnswerController:loadAll() - failed to load all Answers - " + exc.getMessage() );
        	return null;
            
        }

        return answerList;
                            
    }


// findAllBy methods


    /**
     * save Question on Answer
     * @param		Long	answerId
     * @param		Long	childId
     * @param		Answer answer
     * @return		Answer
     */     
	@RequestMapping("/saveQuestion")
	public Answer saveQuestion( @RequestParam(value="answer.answerId", required=true) Long answerId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody Answer answer )
	{
		if ( load( answerId ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "AnswerController:saveQuestion() failed to get Question using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			answer.setQuestion( child );
		
			try
			{
				// save it
				parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:saveQuestion() failed saving parent Answer - " + exc.getMessage() );
			}
		}
		
		return answer;
	}

    /**
     * delete Question on Answer
     * @param		Long answerId
     * @return		Answer
     */     
	@RequestMapping("/deleteQuestion")
	public Answer deleteQuestion( @RequestParam(value="answer.answerId", required=true) Long answerId )
	
	{
		if ( load( answerId ) == null )
			return( null );

		if ( answer.getQuestion() != null )
		{
			QuestionPrimaryKey pk = answer.getQuestion().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			answer.setQuestion( null );
			try
			{
				AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();

				// save it
				answer = parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:deleteQuestion() failed to save Answer - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:deleteQuestion() failed  - " + exc.getMessage() );
			}
		}
		
		return answer;
	}
	
    /**
     * save Response on Answer
     * @param		Long	answerId
     * @param		Long	childId
     * @param		Answer answer
     * @return		Answer
     */     
	@RequestMapping("/saveResponse")
	public Answer saveResponse( @RequestParam(value="answer.answerId", required=true) Long answerId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody Answer answer )
	{
		if ( load( answerId ) == null )
			return( null );
		
		if ( childId != null )
		{
			TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
			AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();			
			TheResponse child 							= null;

			try
			{
				child = childDelegate.getTheResponse( new TheResponsePrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "AnswerController:saveResponse() failed to get TheResponse using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			answer.setResponse( child );
		
			try
			{
				// save it
				parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:saveResponse() failed saving parent Answer - " + exc.getMessage() );
			}
		}
		
		return answer;
	}

    /**
     * delete Response on Answer
     * @param		Long answerId
     * @return		Answer
     */     
	@RequestMapping("/deleteResponse")
	public Answer deleteResponse( @RequestParam(value="answer.answerId", required=true) Long answerId )
	
	{
		if ( load( answerId ) == null )
			return( null );

		if ( answer.getResponse() != null )
		{
			TheResponsePrimaryKey pk = answer.getResponse().getTheResponsePrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			answer.setResponse( null );
			try
			{
				AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();

				// save it
				answer = parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:deleteResponse() failed to save Answer - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				TheResponseBusinessDelegate childDelegate = TheResponseBusinessDelegate.getTheResponseInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AnswerController:deleteResponse() failed  - " + exc.getMessage() );
			}
		}
		
		return answer;
	}
	


    /**
     * Handles creating a Answer BO
     * @return		Answer
     */
    protected Answer create()
    {
        try
        {       
			this.answer = AnswerBusinessDelegate.getAnswerInstance().createAnswer( answer );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AnswerController:create() - exception Answer - " + exc.getMessage());        	
        	return null;
        }
        
        return this.answer;
    }

    /**
     * Handles updating a Answer BO
     * @return		Answer
     */    
    protected Answer update()
    {
    	// store provided data
        Answer tmp = answer;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	answer.copyShallow( tmp );
    	
        try
        {                        	        
			// create the AnswerBusiness Delegate            
			AnswerBusinessDelegate delegate = AnswerBusinessDelegate.getAnswerInstance();
            this.answer = delegate.saveAnswer( answer );
            
            if ( this.answer != null )
                LOGGER.info( "AnswerController:update() - successfully updated Answer - " + answer.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AnswerController:update() - successfully update Answer - " + exc.getMessage());        	
        	return null;
        }
        
        return this.answer;
        
    }


    /**
     * Returns true if the answer is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( answer != null && answer.getAnswerPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Answer load()
    {
    	return( load( new Long( answer.getAnswerPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Answer answer = null;
    private static final Logger LOGGER = Logger.getLogger(Answer.class.getName());
    
}


