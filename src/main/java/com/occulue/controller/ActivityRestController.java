/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity Activity.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/Activity")
public class ActivityRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Activity BO.  if not key provided, calls create, otherwise calls save
     * @param		Activity activity
     * @return		Activity
     */
	@RequestMapping("/save")
    public Activity save( @RequestBody Activity activity )
    {
    	// assign locally
    	this.activity = activity;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Activity BO
     * @param		Long activityId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="activity.activityId", required=false) Long activityId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	ActivityBusinessDelegate delegate = ActivityBusinessDelegate.getActivityInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new ActivityPrimaryKey( activityId ) );
        		LOGGER.info( "ActivityController:delete() - successfully deleted Activity with key " + activity.getActivityPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ActivityPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "ActivityController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ActivityController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Activity BO
     * @param		Long activityId
     * @return		Activity
     */    
    @RequestMapping("/load")
    public Activity load( @RequestParam(value="activity.activityId", required=true) Long activityId )
    {    	
        ActivityPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Activity.load pk is " + activityId );
        	if ( activityId != null )
        	{
        		pk = new ActivityPrimaryKey( activityId );
        		
        		// load the Activity
	            this.activity = ActivityBusinessDelegate.getActivityInstance().getActivity( pk );
	            
	            LOGGER.info( "ActivityController:load() - successfully loaded - " + this.activity.toString() );             
			}
			else
			{
	            LOGGER.info( "ActivityController:load() - unable to locate the primary key as an attribute or a selection for - " + activity.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "ActivityController:load() - failed to load Activity using Id " + activityId + ", " + exc.getMessage() );
            return null;
        }

        return activity;

    }

    /**
     * Handles loading all Activity business objects
     * @return		List<Activity>
     */
    @RequestMapping("/loadAll")
    public List<Activity> loadAll()
    {                
        List<Activity> activityList = null;
        
    	try
        {                        
            // load the Activity
            activityList = ActivityBusinessDelegate.getActivityInstance().getAllActivity();
            
            if ( activityList != null )
                LOGGER.info(  "ActivityController:loadAllActivity() - successfully loaded all Activitys" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "ActivityController:loadAll() - failed to load all Activitys - " + exc.getMessage() );
        	return null;
            
        }

        return activityList;
                            
    }


// findAllBy methods


    /**
     * save User on Activity
     * @param		Long	activityId
     * @param		Long	childId
     * @param		Activity activity
     * @return		Activity
     */     
	@RequestMapping("/saveUser")
	public Activity saveUser( @RequestParam(value="activity.activityId", required=true) Long activityId, 
											@RequestParam(value="childIds", required=true) Long childId, @RequestBody Activity activity )
	{
		if ( load( activityId ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ActivityBusinessDelegate parentDelegate = ActivityBusinessDelegate.getActivityInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	LOGGER.info( "ActivityController:saveUser() failed to get User using id " + childId + " - " + exc.getMessage() );
            	return null;
            }
	
			activity.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveActivity( activity );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ActivityController:saveUser() failed saving parent Activity - " + exc.getMessage() );
			}
		}
		
		return activity;
	}

    /**
     * delete User on Activity
     * @param		Long activityId
     * @return		Activity
     */     
	@RequestMapping("/deleteUser")
	public Activity deleteUser( @RequestParam(value="activity.activityId", required=true) Long activityId )
	
	{
		if ( load( activityId ) == null )
			return( null );

		if ( activity.getUser() != null )
		{
			UserPrimaryKey pk = activity.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			activity.setUser( null );
			try
			{
				ActivityBusinessDelegate parentDelegate = ActivityBusinessDelegate.getActivityInstance();

				// save it
				activity = parentDelegate.saveActivity( activity );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ActivityController:deleteUser() failed to save Activity - " + exc.getMessage() );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "ActivityController:deleteUser() failed  - " + exc.getMessage() );
			}
		}
		
		return activity;
	}
	


    /**
     * Handles creating a Activity BO
     * @return		Activity
     */
    protected Activity create()
    {
        try
        {       
			this.activity = ActivityBusinessDelegate.getActivityInstance().createActivity( activity );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ActivityController:create() - exception Activity - " + exc.getMessage());        	
        	return null;
        }
        
        return this.activity;
    }

    /**
     * Handles updating a Activity BO
     * @return		Activity
     */    
    protected Activity update()
    {
    	// store provided data
        Activity tmp = activity;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	activity.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ActivityBusiness Delegate            
			ActivityBusinessDelegate delegate = ActivityBusinessDelegate.getActivityInstance();
            this.activity = delegate.saveActivity( activity );
            
            if ( this.activity != null )
                LOGGER.info( "ActivityController:update() - successfully updated Activity - " + activity.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "ActivityController:update() - successfully update Activity - " + exc.getMessage());        	
        	return null;
        }
        
        return this.activity;
        
    }


    /**
     * Returns true if the activity is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( activity != null && activity.getActivityPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Activity load()
    {
    	return( load( new Long( activity.getActivityPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Activity activity = null;
    private static final Logger LOGGER = Logger.getLogger(Activity.class.getName());
    
}


