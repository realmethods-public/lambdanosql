/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/** 
 * Implements Struts action processing for business entity Question.
 *
 * @author dev@realmethods.com
 */
@RestController
@RequestMapping("/Question")
public class QuestionRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Question BO.  if not key provided, calls create, otherwise calls save
     * @param		Question question
     * @return		Question
     */
	@RequestMapping("/save")
    public Question save( @RequestBody Question question )
    {
    	// assign locally
    	this.question = question;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Question BO
     * @param		Long questionId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="question.questionId", required=false) Long questionId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	QuestionBusinessDelegate delegate = QuestionBusinessDelegate.getQuestionInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new QuestionPrimaryKey( questionId ) );
        		LOGGER.info( "QuestionController:delete() - successfully deleted Question with key " + question.getQuestionPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new QuestionPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "QuestionController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Question BO
     * @param		Long questionId
     * @return		Question
     */    
    @RequestMapping("/load")
    public Question load( @RequestParam(value="question.questionId", required=true) Long questionId )
    {    	
        QuestionPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Question.load pk is " + questionId );
        	if ( questionId != null )
        	{
        		pk = new QuestionPrimaryKey( questionId );
        		
        		// load the Question
	            this.question = QuestionBusinessDelegate.getQuestionInstance().getQuestion( pk );
	            
	            LOGGER.info( "QuestionController:load() - successfully loaded - " + this.question.toString() );             
			}
			else
			{
	            LOGGER.info( "QuestionController:load() - unable to locate the primary key as an attribute or a selection for - " + question.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "QuestionController:load() - failed to load Question using Id " + questionId + ", " + exc.getMessage() );
            return null;
        }

        return question;

    }

    /**
     * Handles loading all Question business objects
     * @return		List<Question>
     */
    @RequestMapping("/loadAll")
    public List<Question> loadAll()
    {                
        List<Question> questionList = null;
        
    	try
        {                        
            // load the Question
            questionList = QuestionBusinessDelegate.getQuestionInstance().getAllQuestion();
            
            if ( questionList != null )
                LOGGER.info(  "QuestionController:loadAllQuestion() - successfully loaded all Questions" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "QuestionController:loadAll() - failed to load all Questions - " + exc.getMessage() );
        	return null;
            
        }

        return questionList;
                            
    }


// findAllBy methods



    /**
     * save Responses on Question
     * @param		Long questionId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		Question
     */     
	@RequestMapping("/saveResponses")
	public Question saveResponses( @RequestParam(value="question.questionId", required=false) Long questionId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( questionId ) == null )
			return( null );
		
		TheResponsePrimaryKey pk 					= null;
		TheResponse child							= null;
		TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
		QuestionBusinessDelegate parentDelegate = QuestionBusinessDelegate.getQuestionInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new TheResponsePrimaryKey( childId );
			
			try
			{
				// find the TheResponse
				child = childDelegate.getTheResponse( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "QuestionController:saveResponses() failed get child TheResponse using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Responses 
			question.getResponses().add( child );				
		}
		else
		{
			// clear out the Responses but 
			question.getResponses().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new TheResponsePrimaryKey( id );
					try
					{
						// find the TheResponse
						child = childDelegate.getTheResponse( pk );
						// add it to the Responses List
						question.getResponses().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "QuestionController:saveResponses() failed get child TheResponse using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the Question
			parentDelegate.saveQuestion( question );
		}
		catch( Exception exc )
		{
			LOGGER.info( "QuestionController:saveResponses() failed saving parent Question - " + exc.getMessage() );
		}

		return question;
	}

    /**
     * delete Responses on Question
     * @param		Long questionId
     * @param		Long[] childIds
     * @return		Question
     */     	
	@RequestMapping("/deleteResponses")
	public Question deleteResponses( @RequestParam(value="question.questionId", required=true) Long questionId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( questionId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			TheResponsePrimaryKey pk 					= null;
			TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
			QuestionBusinessDelegate parentDelegate = QuestionBusinessDelegate.getQuestionInstance();
			Set<TheResponse> children					= question.getResponses();
			TheResponse child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new TheResponsePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getTheResponse( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "QuestionController:deleteResponses() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of TheResponse back to the question
			question.setResponses( children );			
			// save it 
			try
			{
				question = parentDelegate.saveQuestion( question );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "QuestionController:deleteResponses() failed to save the Question - " + exc.getMessage() );
			}
		}
		
		return question;
	}


    /**
     * Handles creating a Question BO
     * @return		Question
     */
    protected Question create()
    {
        try
        {       
			this.question = QuestionBusinessDelegate.getQuestionInstance().createQuestion( question );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionController:create() - exception Question - " + exc.getMessage());        	
        	return null;
        }
        
        return this.question;
    }

    /**
     * Handles updating a Question BO
     * @return		Question
     */    
    protected Question update()
    {
    	// store provided data
        Question tmp = question;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	question.copyShallow( tmp );
    	
        try
        {                        	        
			// create the QuestionBusiness Delegate            
			QuestionBusinessDelegate delegate = QuestionBusinessDelegate.getQuestionInstance();
            this.question = delegate.saveQuestion( question );
            
            if ( this.question != null )
                LOGGER.info( "QuestionController:update() - successfully updated Question - " + question.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "QuestionController:update() - successfully update Question - " + exc.getMessage());        	
        	return null;
        }
        
        return this.question;
        
    }


    /**
     * Returns true if the question is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( question != null && question.getQuestionPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Question load()
    {
    	return( load( new Long( question.getQuestionPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Question question = null;
    private static final Logger LOGGER = Logger.getLogger(Question.class.getName());
    
}


