/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity ReferenceEngine.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.ReferenceEngine", noClassnameStored = true )
 public class ReferenceEngine extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public ReferenceEngine() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the ReferenceEnginePrimaryKey
     * @return ReferenceEnginePrimaryKey   
     */
    public ReferenceEnginePrimaryKey getReferenceEnginePrimaryKey() 
    {    
    	ReferenceEnginePrimaryKey key = new ReferenceEnginePrimaryKey(); 
		key.setReferenceEngineId( this.referenceEngineId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the name
  	* @return String	
	*/                    		    	    	    
	public String getName() 	    	   
	{
		return this.name;		
	}
	
	/**
              	* Assigns the name
    	* @param name	String
    	*/
    	public void setName( String name )
    	{
    		this.name = name;
    	}	
               /**
    * Returns the active
  	* @return Boolean	
	*/                    		    	    	    
	public Boolean getActive() 	    	   
	{
		return this.active;		
	}
	
	/**
              	* Assigns the active
    	* @param active	Boolean
    	*/
    	public void setActive( Boolean active )
    	{
    		this.active = active;
    	}	
               /**
    * Returns the MainQuestionGroup
  	* @return QuestionGroup	
	*/                    		    	    	    
	public QuestionGroup getMainQuestionGroup() 	    	   
	{
		return this.mainQuestionGroup;		
	}
	
	/**
              	* Assigns the mainQuestionGroup
    	* @param mainQuestionGroup	QuestionGroup
    	*/
    	public void setMainQuestionGroup( QuestionGroup mainQuestionGroup )
    	{
    		this.mainQuestionGroup = mainQuestionGroup;
    	}	
               /**
    * Returns the Purpose
  	* @return Purpose	
	*/                    		    	    	    
	public Purpose getPurpose() 	    	   
	{
		return this.purpose;		
	}
	
	/**
              	* Assigns the purpose
    	* @param purpose	Purpose
    	*/
    	public void setPurpose( Purpose purpose )
    	{
    		this.purpose = purpose;
    	}	
               /**
    * Returns the referenceEngineId
  	* @return Long	
	*/                    		    	    	    
	public Long getReferenceEngineId() 	    	   
	{
		return this.referenceEngineId;		
	}
	
	/**
              	* Assigns the referenceEngineId
    	* @param referenceEngineId	Long
    	*/
    	public void setReferenceEngineId( Long referenceEngineId )
    	{
    		this.referenceEngineId = referenceEngineId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	ReferenceEngine		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( ReferenceEngine object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ReferenceEngine:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.referenceEngineId = object.getReferenceEngineId();
        this.name = object.getName();
        this.active = object.getActive();
        this.purpose = object.getPurpose();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	ReferenceEngine		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( ReferenceEngine object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ReferenceEngine:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
    	if ( object.getMainQuestionGroup() != null)
    	{
    		this.mainQuestionGroup = new QuestionGroup();
    		this.mainQuestionGroup.copyShallow( object.getMainQuestionGroup() );
    	}
    	else
    		this.mainQuestionGroup = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "referenceEngineId = " + this.referenceEngineId + ", ");
		returnString.append( "name = " + this.name + ", ");
		returnString.append( "active = " + this.active + ", ");
		returnString.append( "purpose = " + this.purpose + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "ReferenceEngine" );
		
			identity.append(  "::" );
		identity.append( referenceEngineId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("ReferenceEngine");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof ReferenceEngine)) 
	        return false;
	        
		ReferenceEngine bo = (ReferenceEngine)object;
		
		return( getReferenceEnginePrimaryKey().equals( bo.getReferenceEnginePrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long referenceEngineId = null;
 public String name = null;
 public Boolean active = new Boolean(false);
protected QuestionGroup mainQuestionGroup = null;
 protected Purpose purpose = Purpose.getDefaultValue();
// ~AIB

}
