/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.*;

/**
 * ActivityType enumerator class.  
 * 
 * Enumerated types are handled on behalf of Hiberate as VARCHARs.  The necessary
 * methods that implement Hibernat's UserType interface assume that Enumerated
 * types contain one or more values, each named uniquely and declared (modeled) with
 * order, although the order is assumed.
 * 
// AIB : #enumDocumentation()
     * Encapsulates data for business entity ActivityType.
    // ~AIB
 * @author dev@realmethods.com
 */
public enum ActivityType
{
														referenceStarted,referenceNotificationExpired,referenceCompleted,referenceGroupChecked,commentCreated,referenceMadePublic,referenceMadePrivate;

//************************************************************************
// Access Methods
//************************************************************************

    public static List<ActivityType> getValues()
    {
        return Arrays.asList(ActivityType.values());
    }


    public static ActivityType getDefaultValue()
    {
        return( referenceStarted );
    }

    
//************************************************************************
// Helper Methods
//************************************************************************

//************************************************************************
// static implementations
//************************************************************************
    
    public static ActivityType whichOne( String name ) 
    {
        if ( name.equalsIgnoreCase("referenceStarted" ) )
        {
            return (ActivityType.referenceStarted);
        }
        if ( name.equalsIgnoreCase("referenceNotificationExpired" ) )
        {
            return (ActivityType.referenceNotificationExpired);
        }
        if ( name.equalsIgnoreCase("referenceCompleted" ) )
        {
            return (ActivityType.referenceCompleted);
        }
        if ( name.equalsIgnoreCase("referenceGroupChecked" ) )
        {
            return (ActivityType.referenceGroupChecked);
        }
        if ( name.equalsIgnoreCase("commentCreated" ) )
        {
            return (ActivityType.commentCreated);
        }
        if ( name.equalsIgnoreCase("referenceMadePublic" ) )
        {
            return (ActivityType.referenceMadePublic);
        }
        if ( name.equalsIgnoreCase("referenceMadePrivate" ) )
        {
            return (ActivityType.referenceMadePrivate);
        }
        else
        {
            return (getDefaultValue());
        }
    }

//************************************************************************
// Protected / Private Methods
//************************************************************************
    
//************************************************************************
// Attributes
//************************************************************************

}

