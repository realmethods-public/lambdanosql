/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity ReferrerGroup.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.ReferrerGroup", noClassnameStored = true )
 public class ReferrerGroup extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public ReferrerGroup() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the ReferrerGroupPrimaryKey
     * @return ReferrerGroupPrimaryKey   
     */
    public ReferrerGroupPrimaryKey getReferrerGroupPrimaryKey() 
    {    
    	ReferrerGroupPrimaryKey key = new ReferrerGroupPrimaryKey(); 
		key.setReferrerGroupId( this.referrerGroupId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the name
  	* @return String	
	*/                    		    	    	    
	public String getName() 	    	   
	{
		return this.name;		
	}
	
	/**
              	* Assigns the name
    	* @param name	String
    	*/
    	public void setName( String name )
    	{
    		this.name = name;
    	}	
               /**
    * Returns the dateTimeLastViewedExternally
  	* @return java.util.Date	
	*/                    		    	    	    
	public java.util.Date getDateTimeLastViewedExternally() 	    	   
	{
		return this.dateTimeLastViewedExternally;		
	}
	
	/**
              	* Assigns the dateTimeLastViewedExternally
    	* @param dateTimeLastViewedExternally	java.util.Date
    	*/
    	public void setDateTimeLastViewedExternally( java.util.Date dateTimeLastViewedExternally )
    	{
    		this.dateTimeLastViewedExternally = dateTimeLastViewedExternally;
    	}	
               /**
    * Returns the References
  	* @return Set<TheReference>	
	*/                    		    	    	    
	public Set<TheReference> getReferences() 	    	   
	{
		return this.references;		
	}
	
	/**
              	* Assigns the references
    	* @param references	Set<TheReference>
    	*/
    	public void setReferences( Set<TheReference> references )
    	{
    		this.references = references;
    	}	
               /**
    * Returns the referrerGroupId
  	* @return Long	
	*/                    		    	    	    
	public Long getReferrerGroupId() 	    	   
	{
		return this.referrerGroupId;		
	}
	
	/**
              	* Assigns the referrerGroupId
    	* @param referrerGroupId	Long
    	*/
    	public void setReferrerGroupId( Long referrerGroupId )
    	{
    		this.referrerGroupId = referrerGroupId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	ReferrerGroup		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( ReferrerGroup object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ReferrerGroup:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.referrerGroupId = object.getReferrerGroupId();
        this.name = object.getName();
        this.dateTimeLastViewedExternally = object.getDateTimeLastViewedExternally();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	ReferrerGroup		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( ReferrerGroup object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ReferrerGroup:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
        if ( object.getReferences() != null )
        {
    		this.references = new HashSet<TheReference>();
    		TheReference tmp = null;
        	for ( TheReference listEntry : object.getReferences() )
        	{
        		tmp = new TheReference();
        		tmp.copyShallow( listEntry );
        		this.references.add( tmp );
        	}
        }
        else
        	this.references = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "referrerGroupId = " + this.referrerGroupId + ", ");
		returnString.append( "name = " + this.name + ", ");
		returnString.append( "dateTimeLastViewedExternally = " + this.dateTimeLastViewedExternally + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "ReferrerGroup" );
		
			identity.append(  "::" );
		identity.append( referrerGroupId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("ReferrerGroup");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof ReferrerGroup)) 
	        return false;
	        
		ReferrerGroup bo = (ReferrerGroup)object;
		
		return( getReferrerGroupPrimaryKey().equals( bo.getReferrerGroupPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long referrerGroupId = null;
 public String name = null;
 public java.util.Date dateTimeLastViewedExternally = null;
protected Set<TheReference> references = null;
// ~AIB

}
