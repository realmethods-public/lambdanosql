/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity TheReference.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.TheReference", noClassnameStored = true )
 public class TheReference extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public TheReference() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the TheReferencePrimaryKey
     * @return TheReferencePrimaryKey   
     */
    public TheReferencePrimaryKey getTheReferencePrimaryKey() 
    {    
    	TheReferencePrimaryKey key = new TheReferencePrimaryKey(); 
		key.setTheReferenceId( this.theReferenceId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the dateLastUpdated
  	* @return java.util.Date	
	*/                    		    	    	    
	public java.util.Date getDateLastUpdated() 	    	   
	{
		return this.dateLastUpdated;		
	}
	
	/**
              	* Assigns the dateLastUpdated
    	* @param dateLastUpdated	java.util.Date
    	*/
    	public void setDateLastUpdated( java.util.Date dateLastUpdated )
    	{
    		this.dateLastUpdated = dateLastUpdated;
    	}	
               /**
    * Returns the active
  	* @return Boolean	
	*/                    		    	    	    
	public Boolean getActive() 	    	   
	{
		return this.active;		
	}
	
	/**
              	* Assigns the active
    	* @param active	Boolean
    	*/
    	public void setActive( Boolean active )
    	{
    		this.active = active;
    	}	
               /**
    * Returns the dateTimeLastViewedExternally
  	* @return java.util.Date	
	*/                    		    	    	    
	public java.util.Date getDateTimeLastViewedExternally() 	    	   
	{
		return this.dateTimeLastViewedExternally;		
	}
	
	/**
              	* Assigns the dateTimeLastViewedExternally
    	* @param dateTimeLastViewedExternally	java.util.Date
    	*/
    	public void setDateTimeLastViewedExternally( java.util.Date dateTimeLastViewedExternally )
    	{
    		this.dateTimeLastViewedExternally = dateTimeLastViewedExternally;
    	}	
               /**
    * Returns the makeViewableToUser
  	* @return Boolean	
	*/                    		    	    	    
	public Boolean getMakeViewableToUser() 	    	   
	{
		return this.makeViewableToUser;		
	}
	
	/**
              	* Assigns the makeViewableToUser
    	* @param makeViewableToUser	Boolean
    	*/
    	public void setMakeViewableToUser( Boolean makeViewableToUser )
    	{
    		this.makeViewableToUser = makeViewableToUser;
    	}	
               /**
    * Returns the dateLastSent
  	* @return java.util.Date	
	*/                    		    	    	    
	public java.util.Date getDateLastSent() 	    	   
	{
		return this.dateLastSent;		
	}
	
	/**
              	* Assigns the dateLastSent
    	* @param dateLastSent	java.util.Date
    	*/
    	public void setDateLastSent( java.util.Date dateLastSent )
    	{
    		this.dateLastSent = dateLastSent;
    	}	
               /**
    * Returns the numDaysToExpire
  	* @return Integer	
	*/                    		    	    	    
	public Integer getNumDaysToExpire() 	    	   
	{
		return this.numDaysToExpire;		
	}
	
	/**
              	* Assigns the numDaysToExpire
    	* @param numDaysToExpire	Integer
    	*/
    	public void setNumDaysToExpire( Integer numDaysToExpire )
    	{
    		this.numDaysToExpire = numDaysToExpire;
    	}	
               /**
    * Returns the QuestionGroup
  	* @return QuestionGroup	
	*/                    		    	    	    
	public QuestionGroup getQuestionGroup() 	    	   
	{
		return this.questionGroup;		
	}
	
	/**
              	* Assigns the questionGroup
    	* @param questionGroup	QuestionGroup
    	*/
    	public void setQuestionGroup( QuestionGroup questionGroup )
    	{
    		this.questionGroup = questionGroup;
    	}	
               /**
    * Returns the Status
  	* @return ReferenceStatus	
	*/                    		    	    	    
	public ReferenceStatus getStatus() 	    	   
	{
		return this.status;		
	}
	
	/**
              	* Assigns the status
    	* @param status	ReferenceStatus
    	*/
    	public void setStatus( ReferenceStatus status )
    	{
    		this.status = status;
    	}	
               /**
    * Returns the Type
  	* @return Purpose	
	*/                    		    	    	    
	public Purpose getType() 	    	   
	{
		return this.type;		
	}
	
	/**
              	* Assigns the type
    	* @param type	Purpose
    	*/
    	public void setType( Purpose type )
    	{
    		this.type = type;
    	}	
               /**
    * Returns the User
  	* @return User	
	*/                    		    	    	    
	public User getUser() 	    	   
	{
		return this.user;		
	}
	
	/**
              	* Assigns the user
    	* @param user	User
    	*/
    	public void setUser( User user )
    	{
    		this.user = user;
    	}	
               /**
    * Returns the Referrer
  	* @return Referrer	
	*/                    		    	    	    
	public Referrer getReferrer() 	    	   
	{
		return this.referrer;		
	}
	
	/**
              	* Assigns the referrer
    	* @param referrer	Referrer
    	*/
    	public void setReferrer( Referrer referrer )
    	{
    		this.referrer = referrer;
    	}	
               /**
    * Returns the Answers
  	* @return Set<Answer>	
	*/                    		    	    	    
	public Set<Answer> getAnswers() 	    	   
	{
		return this.answers;		
	}
	
	/**
              	* Assigns the answers
    	* @param answers	Set<Answer>
    	*/
    	public void setAnswers( Set<Answer> answers )
    	{
    		this.answers = answers;
    	}	
               /**
    * Returns the LastQuestionAnswered
  	* @return Question	
	*/                    		    	    	    
	public Question getLastQuestionAnswered() 	    	   
	{
		return this.lastQuestionAnswered;		
	}
	
	/**
              	* Assigns the lastQuestionAnswered
    	* @param lastQuestionAnswered	Question
    	*/
    	public void setLastQuestionAnswered( Question lastQuestionAnswered )
    	{
    		this.lastQuestionAnswered = lastQuestionAnswered;
    	}	
               /**
    * Returns the theReferenceId
  	* @return Long	
	*/                    		    	    	    
	public Long getTheReferenceId() 	    	   
	{
		return this.theReferenceId;		
	}
	
	/**
              	* Assigns the theReferenceId
    	* @param theReferenceId	Long
    	*/
    	public void setTheReferenceId( Long theReferenceId )
    	{
    		this.theReferenceId = theReferenceId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	TheReference		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( TheReference object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" TheReference:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.theReferenceId = object.getTheReferenceId();
        this.dateLastUpdated = object.getDateLastUpdated();
        this.active = object.getActive();
        this.dateTimeLastViewedExternally = object.getDateTimeLastViewedExternally();
        this.makeViewableToUser = object.getMakeViewableToUser();
        this.dateLastSent = object.getDateLastSent();
        this.numDaysToExpire = object.getNumDaysToExpire();
        this.status = object.getStatus();
        this.type = object.getType();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	TheReference		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( TheReference object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" TheReference:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
    	if ( object.getQuestionGroup() != null)
    	{
    		this.questionGroup = new QuestionGroup();
    		this.questionGroup.copyShallow( object.getQuestionGroup() );
    	}
    	else
    		this.questionGroup = null;
    	if ( object.getUser() != null)
    	{
    		this.user = new User();
    		this.user.copyShallow( object.getUser() );
    	}
    	else
    		this.user = null;
    	if ( object.getReferrer() != null)
    	{
    		this.referrer = new Referrer();
    		this.referrer.copyShallow( object.getReferrer() );
    	}
    	else
    		this.referrer = null;
        if ( object.getAnswers() != null )
        {
    		this.answers = new HashSet<Answer>();
    		Answer tmp = null;
        	for ( Answer listEntry : object.getAnswers() )
        	{
        		tmp = new Answer();
        		tmp.copyShallow( listEntry );
        		this.answers.add( tmp );
        	}
        }
        else
        	this.answers = null;
    	if ( object.getLastQuestionAnswered() != null)
    	{
    		this.lastQuestionAnswered = new Question();
    		this.lastQuestionAnswered.copyShallow( object.getLastQuestionAnswered() );
    	}
    	else
    		this.lastQuestionAnswered = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "theReferenceId = " + this.theReferenceId + ", ");
		returnString.append( "dateLastUpdated = " + this.dateLastUpdated + ", ");
		returnString.append( "active = " + this.active + ", ");
		returnString.append( "dateTimeLastViewedExternally = " + this.dateTimeLastViewedExternally + ", ");
		returnString.append( "makeViewableToUser = " + this.makeViewableToUser + ", ");
		returnString.append( "dateLastSent = " + this.dateLastSent + ", ");
		returnString.append( "numDaysToExpire = " + this.numDaysToExpire + ", ");
		returnString.append( "status = " + this.status + ", ");
		returnString.append( "type = " + this.type + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "TheReference" );
		
			identity.append(  "::" );
		identity.append( theReferenceId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("TheReference");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof TheReference)) 
	        return false;
	        
		TheReference bo = (TheReference)object;
		
		return( getTheReferencePrimaryKey().equals( bo.getTheReferencePrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long theReferenceId = null;
 protected java.util.Date dateLastUpdated = null;
 protected Boolean active = new Boolean("true");
 protected java.util.Date dateTimeLastViewedExternally = null;
 public Boolean makeViewableToUser = new Boolean("false");
 public java.util.Date dateLastSent = null;
 public Integer numDaysToExpire = new Integer("7");
protected QuestionGroup questionGroup = null;
 protected ReferenceStatus status = ReferenceStatus.getDefaultValue();
 protected Purpose type = Purpose.getDefaultValue();
protected User user = null;
protected Referrer referrer = null;
protected Set<Answer> answers = null;
protected Question lastQuestionAnswered = null;
// ~AIB

}
