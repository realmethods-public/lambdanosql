/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.*;

/**
 * ReferenceStatus enumerator class.  
 * 
 * Enumerated types are handled on behalf of Hiberate as VARCHARs.  The necessary
 * methods that implement Hibernat's UserType interface assume that Enumerated
 * types contain one or more values, each named uniquely and declared (modeled) with
 * order, although the order is assumed.
 * 
// AIB : #enumDocumentation()
     * Encapsulates data for business entity ReferenceStatus.
    // ~AIB
 * @author dev@realmethods.com
 */
public enum ReferenceStatus
{
						notYetStarted,inProgress,completed;

//************************************************************************
// Access Methods
//************************************************************************

    public static List<ReferenceStatus> getValues()
    {
        return Arrays.asList(ReferenceStatus.values());
    }


    public static ReferenceStatus getDefaultValue()
    {
        return( notYetStarted );
    }

    
//************************************************************************
// Helper Methods
//************************************************************************

//************************************************************************
// static implementations
//************************************************************************
    
    public static ReferenceStatus whichOne( String name ) 
    {
        if ( name.equalsIgnoreCase("notYetStarted" ) )
        {
            return (ReferenceStatus.notYetStarted);
        }
        if ( name.equalsIgnoreCase("inProgress" ) )
        {
            return (ReferenceStatus.inProgress);
        }
        if ( name.equalsIgnoreCase("completed" ) )
        {
            return (ReferenceStatus.completed);
        }
        else
        {
            return (getDefaultValue());
        }
    }

//************************************************************************
// Protected / Private Methods
//************************************************************************
    
//************************************************************************
// Attributes
//************************************************************************

}

