/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.*;

/**
 * Purpose enumerator class.  
 * 
 * Enumerated types are handled on behalf of Hiberate as VARCHARs.  The necessary
 * methods that implement Hibernat's UserType interface assume that Enumerated
 * types contain one or more values, each named uniquely and declared (modeled) with
 * order, although the order is assumed.
 * 
// AIB : #enumDocumentation()
     * Encapsulates data for business entity Purpose.
    // ~AIB
 * @author dev@realmethods.com
 */
public enum Purpose
{
								character,business,employment,residency;

//************************************************************************
// Access Methods
//************************************************************************

    public static List<Purpose> getValues()
    {
        return Arrays.asList(Purpose.values());
    }


    public static Purpose getDefaultValue()
    {
        return( character );
    }

    
//************************************************************************
// Helper Methods
//************************************************************************

//************************************************************************
// static implementations
//************************************************************************
    
    public static Purpose whichOne( String name ) 
    {
        if ( name.equalsIgnoreCase("character" ) )
        {
            return (Purpose.character);
        }
        if ( name.equalsIgnoreCase("business" ) )
        {
            return (Purpose.business);
        }
        if ( name.equalsIgnoreCase("employment" ) )
        {
            return (Purpose.employment);
        }
        if ( name.equalsIgnoreCase("residency" ) )
        {
            return (Purpose.residency);
        }
        else
        {
            return (getDefaultValue());
        }
    }

//************************************************************************
// Protected / Private Methods
//************************************************************************
    
//************************************************************************
// Attributes
//************************************************************************

}

