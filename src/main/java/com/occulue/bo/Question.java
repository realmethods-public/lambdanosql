/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity Question.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.Question", noClassnameStored = true )
 public class Question extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public Question() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the QuestionPrimaryKey
     * @return QuestionPrimaryKey   
     */
    public QuestionPrimaryKey getQuestionPrimaryKey() 
    {    
    	QuestionPrimaryKey key = new QuestionPrimaryKey(); 
		key.setQuestionId( this.questionId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the weight
  	* @return java.lang.Double	
	*/                    		    	    	    
	public java.lang.Double getWeight() 	    	   
	{
		return this.weight;		
	}
	
	/**
              	* Assigns the weight
    	* @param weight	java.lang.Double
    	*/
    	public void setWeight( java.lang.Double weight )
    	{
    		this.weight = weight;
    	}	
               /**
    * Returns the questionText
  	* @return String	
	*/                    		    	    	    
	public String getQuestionText() 	    	   
	{
		return this.questionText;		
	}
	
	/**
              	* Assigns the questionText
    	* @param questionText	String
    	*/
    	public void setQuestionText( String questionText )
    	{
    		this.questionText = questionText;
    	}	
               /**
    * Returns the identifier
  	* @return String	
	*/                    		    	    	    
	public String getIdentifier() 	    	   
	{
		return this.identifier;		
	}
	
	/**
              	* Assigns the identifier
    	* @param identifier	String
    	*/
    	public void setIdentifier( String identifier )
    	{
    		this.identifier = identifier;
    	}	
               /**
    * Returns the mustBeAnswered
  	* @return Boolean	
	*/                    		    	    	    
	public Boolean getMustBeAnswered() 	    	   
	{
		return this.mustBeAnswered;		
	}
	
	/**
              	* Assigns the mustBeAnswered
    	* @param mustBeAnswered	Boolean
    	*/
    	public void setMustBeAnswered( Boolean mustBeAnswered )
    	{
    		this.mustBeAnswered = mustBeAnswered;
    	}	
               /**
    * Returns the responseExclusive
  	* @return Boolean	
	*/                    		    	    	    
	public Boolean getResponseExclusive() 	    	   
	{
		return this.responseExclusive;		
	}
	
	/**
              	* Assigns the responseExclusive
    	* @param responseExclusive	Boolean
    	*/
    	public void setResponseExclusive( Boolean responseExclusive )
    	{
    		this.responseExclusive = responseExclusive;
    	}	
               /**
    * Returns the Responses
  	* @return Set<TheResponse>	
	*/                    		    	    	    
	public Set<TheResponse> getResponses() 	    	   
	{
		return this.responses;		
	}
	
	/**
              	* Assigns the responses
    	* @param responses	Set<TheResponse>
    	*/
    	public void setResponses( Set<TheResponse> responses )
    	{
    		this.responses = responses;
    	}	
               /**
    * Returns the questionId
  	* @return Long	
	*/                    		    	    	    
	public Long getQuestionId() 	    	   
	{
		return this.questionId;		
	}
	
	/**
              	* Assigns the questionId
    	* @param questionId	Long
    	*/
    	public void setQuestionId( Long questionId )
    	{
    		this.questionId = questionId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	Question		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( Question object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Question:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.questionId = object.getQuestionId();
        this.weight = object.getWeight();
        this.questionText = object.getQuestionText();
        this.identifier = object.getIdentifier();
        this.mustBeAnswered = object.getMustBeAnswered();
        this.responseExclusive = object.getResponseExclusive();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	Question		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( Question object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Question:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
        if ( object.getResponses() != null )
        {
    		this.responses = new HashSet<TheResponse>();
    		TheResponse tmp = null;
        	for ( TheResponse listEntry : object.getResponses() )
        	{
        		tmp = new TheResponse();
        		tmp.copyShallow( listEntry );
        		this.responses.add( tmp );
        	}
        }
        else
        	this.responses = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "questionId = " + this.questionId + ", ");
		returnString.append( "weight = " + this.weight + ", ");
		returnString.append( "questionText = " + this.questionText + ", ");
		returnString.append( "identifier = " + this.identifier + ", ");
		returnString.append( "mustBeAnswered = " + this.mustBeAnswered + ", ");
		returnString.append( "responseExclusive = " + this.responseExclusive + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "Question" );
		
			identity.append(  "::" );
		identity.append( questionId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("Question");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof Question)) 
	        return false;
	        
		Question bo = (Question)object;
		
		return( getQuestionPrimaryKey().equals( bo.getQuestionPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long questionId = null;
 public java.lang.Double weight = null;
 public String questionText = null;
 public String identifier = null;
 public Boolean mustBeAnswered = new Boolean("true");
 public Boolean responseExclusive = new Boolean("true");
protected Set<TheResponse> responses = null;
// ~AIB

}
