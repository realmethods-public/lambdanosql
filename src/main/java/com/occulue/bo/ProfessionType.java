/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.*;

/**
 * ProfessionType enumerator class.  
 * 
 * Enumerated types are handled on behalf of Hiberate as VARCHARs.  The necessary
 * methods that implement Hibernat's UserType interface assume that Enumerated
 * types contain one or more values, each named uniquely and declared (modeled) with
 * order, although the order is assumed.
 * 
// AIB : #enumDocumentation()
     * Encapsulates data for business entity ProfessionType.
    // ~AIB
 * @author dev@realmethods.com
 */
public enum ProfessionType
{
																		academic,technical,medical,construction,politics,professional,layworker,legal,social;

//************************************************************************
// Access Methods
//************************************************************************

    public static List<ProfessionType> getValues()
    {
        return Arrays.asList(ProfessionType.values());
    }


    public static ProfessionType getDefaultValue()
    {
        return( academic );
    }

    
//************************************************************************
// Helper Methods
//************************************************************************

//************************************************************************
// static implementations
//************************************************************************
    
    public static ProfessionType whichOne( String name ) 
    {
        if ( name.equalsIgnoreCase("academic" ) )
        {
            return (ProfessionType.academic);
        }
        if ( name.equalsIgnoreCase("technical" ) )
        {
            return (ProfessionType.technical);
        }
        if ( name.equalsIgnoreCase("medical" ) )
        {
            return (ProfessionType.medical);
        }
        if ( name.equalsIgnoreCase("construction" ) )
        {
            return (ProfessionType.construction);
        }
        if ( name.equalsIgnoreCase("politics" ) )
        {
            return (ProfessionType.politics);
        }
        if ( name.equalsIgnoreCase("professional" ) )
        {
            return (ProfessionType.professional);
        }
        if ( name.equalsIgnoreCase("layworker" ) )
        {
            return (ProfessionType.layworker);
        }
        if ( name.equalsIgnoreCase("legal" ) )
        {
            return (ProfessionType.legal);
        }
        if ( name.equalsIgnoreCase("social" ) )
        {
            return (ProfessionType.social);
        }
        else
        {
            return (getDefaultValue());
        }
    }

//************************************************************************
// Protected / Private Methods
//************************************************************************
    
//************************************************************************
// Attributes
//************************************************************************

}

