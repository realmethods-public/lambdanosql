/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity Admin.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.Admin", noClassnameStored = true )
 public class Admin extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public Admin() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the AdminPrimaryKey
     * @return AdminPrimaryKey   
     */
    public AdminPrimaryKey getAdminPrimaryKey() 
    {    
    	AdminPrimaryKey key = new AdminPrimaryKey(); 
		key.setAdminId( this.adminId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the loginId
  	* @return String	
	*/                    		    	    	    
	public String getLoginId() 	    	   
	{
		return this.loginId;		
	}
	
	/**
              	* Assigns the loginId
    	* @param loginId	String
    	*/
    	public void setLoginId( String loginId )
    	{
    		this.loginId = loginId;
    	}	
               /**
    * Returns the password
  	* @return String	
	*/                    		    	    	    
	public String getPassword() 	    	   
	{
		return this.password;		
	}
	
	/**
              	* Assigns the password
    	* @param password	String
    	*/
    	public void setPassword( String password )
    	{
    		this.password = password;
    	}	
               /**
    * Returns the Users
  	* @return Set<User>	
	*/                    		    	    	    
	public Set<User> getUsers() 	    	   
	{
		return this.users;		
	}
	
	/**
              	* Assigns the users
    	* @param users	Set<User>
    	*/
    	public void setUsers( Set<User> users )
    	{
    		this.users = users;
    	}	
               /**
    * Returns the ReferenceEngines
  	* @return Set<ReferenceEngine>	
	*/                    		    	    	    
	public Set<ReferenceEngine> getReferenceEngines() 	    	   
	{
		return this.referenceEngines;		
	}
	
	/**
              	* Assigns the referenceEngines
    	* @param referenceEngines	Set<ReferenceEngine>
    	*/
    	public void setReferenceEngines( Set<ReferenceEngine> referenceEngines )
    	{
    		this.referenceEngines = referenceEngines;
    	}	
               /**
    * Returns the adminId
  	* @return Long	
	*/                    		    	    	    
	public Long getAdminId() 	    	   
	{
		return this.adminId;		
	}
	
	/**
              	* Assigns the adminId
    	* @param adminId	Long
    	*/
    	public void setAdminId( Long adminId )
    	{
    		this.adminId = adminId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	Admin		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( Admin object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Admin:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.adminId = object.getAdminId();
        this.loginId = object.getLoginId();
        this.password = object.getPassword();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	Admin		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( Admin object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Admin:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
        if ( object.getUsers() != null )
        {
    		this.users = new HashSet<User>();
    		User tmp = null;
        	for ( User listEntry : object.getUsers() )
        	{
        		tmp = new User();
        		tmp.copyShallow( listEntry );
        		this.users.add( tmp );
        	}
        }
        else
        	this.users = null;
        if ( object.getReferenceEngines() != null )
        {
    		this.referenceEngines = new HashSet<ReferenceEngine>();
    		ReferenceEngine tmp = null;
        	for ( ReferenceEngine listEntry : object.getReferenceEngines() )
        	{
        		tmp = new ReferenceEngine();
        		tmp.copyShallow( listEntry );
        		this.referenceEngines.add( tmp );
        	}
        }
        else
        	this.referenceEngines = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "adminId = " + this.adminId + ", ");
		returnString.append( "loginId = " + this.loginId + ", ");
		returnString.append( "password = " + this.password + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "Admin" );
		
			identity.append(  "::" );
		identity.append( adminId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("Admin");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof Admin)) 
	        return false;
	        
		Admin bo = (Admin)object;
		
		return( getAdminPrimaryKey().equals( bo.getAdminPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long adminId = null;
 public String loginId = null;
 public String password = null;
protected Set<User> users = null;
protected Set<ReferenceEngine> referenceEngines = null;
// ~AIB

}
