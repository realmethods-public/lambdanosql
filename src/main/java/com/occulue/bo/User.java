/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity User.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.User", noClassnameStored = true )
public class User extends Referrer
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public User() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the UserPrimaryKey
     * @return UserPrimaryKey   
     */
    public UserPrimaryKey getUserPrimaryKey() 
    {    
    	UserPrimaryKey key = new UserPrimaryKey(); 
        return( key );
    } 

        	public Long getUserId()
    	{ 
    		return getReferrerId(); 
    	}
    	
    	public void setUserId( Long id )
    	{ 
    		setReferrerId( id ); 
    	}
    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the password
  	* @return String	
	*/                    		    	    	    
	public String getPassword() 	    	   
	{
		return this.password;		
	}
	
	/**
              	* Assigns the password
    	* @param password	String
    	*/
    	public void setPassword( String password )
    	{
    		this.password = password;
    	}	
               /**
    * Returns the resumeLinkUrl
  	* @return String	
	*/                    		    	    	    
	public String getResumeLinkUrl() 	    	   
	{
		return this.resumeLinkUrl;		
	}
	
	/**
              	* Assigns the resumeLinkUrl
    	* @param resumeLinkUrl	String
    	*/
    	public void setResumeLinkUrl( String resumeLinkUrl )
    	{
    		this.resumeLinkUrl = resumeLinkUrl;
    	}	
               /**
    * Returns the linkedInUrl
  	* @return String	
	*/                    		    	    	    
	public String getLinkedInUrl() 	    	   
	{
		return this.linkedInUrl;		
	}
	
	/**
              	* Assigns the linkedInUrl
    	* @param linkedInUrl	String
    	*/
    	public void setLinkedInUrl( String linkedInUrl )
    	{
    		this.linkedInUrl = linkedInUrl;
    	}	
               /**
    * Returns the ReferenceProviders
  	* @return Set<Referrer>	
	*/                    		    	    	    
	public Set<Referrer> getReferenceProviders() 	    	   
	{
		return this.referenceProviders;		
	}
	
	/**
              	* Assigns the referenceProviders
    	* @param referenceProviders	Set<Referrer>
    	*/
    	public void setReferenceProviders( Set<Referrer> referenceProviders )
    	{
    		this.referenceProviders = referenceProviders;
    	}	
               /**
    * Returns the RefererGroups
  	* @return Set<ReferrerGroup>	
	*/                    		    	    	    
	public Set<ReferrerGroup> getRefererGroups() 	    	   
	{
		return this.refererGroups;		
	}
	
	/**
              	* Assigns the refererGroups
    	* @param refererGroups	Set<ReferrerGroup>
    	*/
    	public void setRefererGroups( Set<ReferrerGroup> refererGroups )
    	{
    		this.refererGroups = refererGroups;
    	}	
               /**
    * Returns the ReferenceReceivers
  	* @return Set<Referrer>	
	*/                    		    	    	    
	public Set<Referrer> getReferenceReceivers() 	    	   
	{
		return this.referenceReceivers;		
	}
	
	/**
              	* Assigns the referenceReceivers
    	* @param referenceReceivers	Set<Referrer>
    	*/
    	public void setReferenceReceivers( Set<Referrer> referenceReceivers )
    	{
    		this.referenceReceivers = referenceReceivers;
    	}	
               /**
    * Returns the ReferenceGroupLinks
  	* @return Set<ReferenceGroupLink>	
	*/                    		    	    	    
	public Set<ReferenceGroupLink> getReferenceGroupLinks() 	    	   
	{
		return this.referenceGroupLinks;		
	}
	
	/**
              	* Assigns the referenceGroupLinks
    	* @param referenceGroupLinks	Set<ReferenceGroupLink>
    	*/
    	public void setReferenceGroupLinks( Set<ReferenceGroupLink> referenceGroupLinks )
    	{
    		this.referenceGroupLinks = referenceGroupLinks;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	User		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( User object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" User:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.password = object.getPassword();
        this.resumeLinkUrl = object.getResumeLinkUrl();
        this.linkedInUrl = object.getLinkedInUrl();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	User		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( User object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" User:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
        if ( object.getReferenceProviders() != null )
        {
    		this.referenceProviders = new HashSet<Referrer>();
    		Referrer tmp = null;
        	for ( Referrer listEntry : object.getReferenceProviders() )
        	{
        		tmp = new Referrer();
        		tmp.copyShallow( listEntry );
        		this.referenceProviders.add( tmp );
        	}
        }
        else
        	this.referenceProviders = null;
        if ( object.getRefererGroups() != null )
        {
    		this.refererGroups = new HashSet<ReferrerGroup>();
    		ReferrerGroup tmp = null;
        	for ( ReferrerGroup listEntry : object.getRefererGroups() )
        	{
        		tmp = new ReferrerGroup();
        		tmp.copyShallow( listEntry );
        		this.refererGroups.add( tmp );
        	}
        }
        else
        	this.refererGroups = null;
        if ( object.getReferenceReceivers() != null )
        {
    		this.referenceReceivers = new HashSet<Referrer>();
    		Referrer tmp = null;
        	for ( Referrer listEntry : object.getReferenceReceivers() )
        	{
        		tmp = new Referrer();
        		tmp.copyShallow( listEntry );
        		this.referenceReceivers.add( tmp );
        	}
        }
        else
        	this.referenceReceivers = null;
        if ( object.getReferenceGroupLinks() != null )
        {
    		this.referenceGroupLinks = new HashSet<ReferenceGroupLink>();
    		ReferenceGroupLink tmp = null;
        	for ( ReferenceGroupLink listEntry : object.getReferenceGroupLinks() )
        	{
        		tmp = new ReferenceGroupLink();
        		tmp.copyShallow( listEntry );
        		this.referenceGroupLinks.add( tmp );
        	}
        }
        else
        	this.referenceGroupLinks = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "password = " + this.password + ", ");
		returnString.append( "resumeLinkUrl = " + this.resumeLinkUrl + ", ");
		returnString.append( "linkedInUrl = " + this.linkedInUrl + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "User" );
		
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("User");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof User)) 
	        return false;
	        
		User bo = (User)object;
		
		return( getUserPrimaryKey().equals( bo.getUserPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
 protected String password = null;
 public String resumeLinkUrl = null;
 public String linkedInUrl = null;
protected Set<Referrer> referenceProviders = null;
protected Set<ReferrerGroup> refererGroups = null;
protected Set<Referrer> referenceReceivers = null;
protected Set<ReferenceGroupLink> referenceGroupLinks = null;
// ~AIB

}
