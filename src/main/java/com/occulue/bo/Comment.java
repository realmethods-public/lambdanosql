/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity Comment.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.Comment", noClassnameStored = true )
 public class Comment extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public Comment() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the CommentPrimaryKey
     * @return CommentPrimaryKey   
     */
    public CommentPrimaryKey getCommentPrimaryKey() 
    {    
    	CommentPrimaryKey key = new CommentPrimaryKey(); 
		key.setCommentId( this.commentId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the commentText
  	* @return String	
	*/                    		    	    	    
	public String getCommentText() 	    	   
	{
		return this.commentText;		
	}
	
	/**
              	* Assigns the commentText
    	* @param commentText	String
    	*/
    	public void setCommentText( String commentText )
    	{
    		this.commentText = commentText;
    	}	
               /**
    * Returns the Source
  	* @return Referrer	
	*/                    		    	    	    
	public Referrer getSource() 	    	   
	{
		return this.source;		
	}
	
	/**
              	* Assigns the source
    	* @param source	Referrer
    	*/
    	public void setSource( Referrer source )
    	{
    		this.source = source;
    	}	
               /**
    * Returns the commentId
  	* @return Long	
	*/                    		    	    	    
	public Long getCommentId() 	    	   
	{
		return this.commentId;		
	}
	
	/**
              	* Assigns the commentId
    	* @param commentId	Long
    	*/
    	public void setCommentId( Long commentId )
    	{
    		this.commentId = commentId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	Comment		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( Comment object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Comment:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.commentId = object.getCommentId();
        this.commentText = object.getCommentText();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	Comment		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( Comment object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Comment:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
    	if ( object.getSource() != null)
    	{
    		this.source = new Referrer();
    		this.source.copyShallow( object.getSource() );
    	}
    	else
    		this.source = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "commentId = " + this.commentId + ", ");
		returnString.append( "commentText = " + this.commentText + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "Comment" );
		
			identity.append(  "::" );
		identity.append( commentId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("Comment");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof Comment)) 
	        return false;
	        
		Comment bo = (Comment)object;
		
		return( getCommentPrimaryKey().equals( bo.getCommentPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long commentId = null;
 public String commentText = null;
protected Referrer source = null;
// ~AIB

}
