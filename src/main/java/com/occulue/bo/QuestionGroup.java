/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity QuestionGroup.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.QuestionGroup", noClassnameStored = true )
 public class QuestionGroup extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public QuestionGroup() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the QuestionGroupPrimaryKey
     * @return QuestionGroupPrimaryKey   
     */
    public QuestionGroupPrimaryKey getQuestionGroupPrimaryKey() 
    {    
    	QuestionGroupPrimaryKey key = new QuestionGroupPrimaryKey(); 
		key.setQuestionGroupId( this.questionGroupId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the aggregateScore
  	* @return java.lang.Double	
	*/                    		    	    	    
	public java.lang.Double getAggregateScore() 	    	   
	{
		return this.aggregateScore;		
	}
	
	/**
              	* Assigns the aggregateScore
    	* @param aggregateScore	java.lang.Double
    	*/
    	public void setAggregateScore( java.lang.Double aggregateScore )
    	{
    		this.aggregateScore = aggregateScore;
    	}	
               /**
    * Returns the weight
  	* @return java.lang.Double	
	*/                    		    	    	    
	public java.lang.Double getWeight() 	    	   
	{
		return this.weight;		
	}
	
	/**
              	* Assigns the weight
    	* @param weight	java.lang.Double
    	*/
    	public void setWeight( java.lang.Double weight )
    	{
    		this.weight = weight;
    	}	
               /**
    * Returns the name
  	* @return String	
	*/                    		    	    	    
	public String getName() 	    	   
	{
		return this.name;		
	}
	
	/**
              	* Assigns the name
    	* @param name	String
    	*/
    	public void setName( String name )
    	{
    		this.name = name;
    	}	
               /**
    * Returns the Questions
  	* @return Set<Question>	
	*/                    		    	    	    
	public Set<Question> getQuestions() 	    	   
	{
		return this.questions;		
	}
	
	/**
              	* Assigns the questions
    	* @param questions	Set<Question>
    	*/
    	public void setQuestions( Set<Question> questions )
    	{
    		this.questions = questions;
    	}	
               /**
    * Returns the QuestionGroups
  	* @return Set<QuestionGroup>	
	*/                    		    	    	    
	public Set<QuestionGroup> getQuestionGroups() 	    	   
	{
		return this.questionGroups;		
	}
	
	/**
              	* Assigns the questionGroups
    	* @param questionGroups	Set<QuestionGroup>
    	*/
    	public void setQuestionGroups( Set<QuestionGroup> questionGroups )
    	{
    		this.questionGroups = questionGroups;
    	}	
               /**
    * Returns the questionGroupId
  	* @return Long	
	*/                    		    	    	    
	public Long getQuestionGroupId() 	    	   
	{
		return this.questionGroupId;		
	}
	
	/**
              	* Assigns the questionGroupId
    	* @param questionGroupId	Long
    	*/
    	public void setQuestionGroupId( Long questionGroupId )
    	{
    		this.questionGroupId = questionGroupId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	QuestionGroup		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( QuestionGroup object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" QuestionGroup:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.questionGroupId = object.getQuestionGroupId();
        this.aggregateScore = object.getAggregateScore();
        this.weight = object.getWeight();
        this.name = object.getName();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	QuestionGroup		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( QuestionGroup object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" QuestionGroup:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
        if ( object.getQuestions() != null )
        {
    		this.questions = new HashSet<Question>();
    		Question tmp = null;
        	for ( Question listEntry : object.getQuestions() )
        	{
        		tmp = new Question();
        		tmp.copyShallow( listEntry );
        		this.questions.add( tmp );
        	}
        }
        else
        	this.questions = null;
        if ( object.getQuestionGroups() != null )
        {
    		this.questionGroups = new HashSet<QuestionGroup>();
    		QuestionGroup tmp = null;
        	for ( QuestionGroup listEntry : object.getQuestionGroups() )
        	{
        		tmp = new QuestionGroup();
        		tmp.copyShallow( listEntry );
        		this.questionGroups.add( tmp );
        	}
        }
        else
        	this.questionGroups = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "questionGroupId = " + this.questionGroupId + ", ");
		returnString.append( "aggregateScore = " + this.aggregateScore + ", ");
		returnString.append( "weight = " + this.weight + ", ");
		returnString.append( "name = " + this.name + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "QuestionGroup" );
		
			identity.append(  "::" );
		identity.append( questionGroupId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("QuestionGroup");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof QuestionGroup)) 
	        return false;
	        
		QuestionGroup bo = (QuestionGroup)object;
		
		return( getQuestionGroupPrimaryKey().equals( bo.getQuestionGroupPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long questionGroupId = null;
 public java.lang.Double aggregateScore = null;
 public java.lang.Double weight = null;
 public String name = null;
protected Set<Question> questions = null;
protected Set<QuestionGroup> questionGroups = null;
// ~AIB

}
