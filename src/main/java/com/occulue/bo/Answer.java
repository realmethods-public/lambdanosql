/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/**
 * Encapsulates data for business entity Answer.
 * 
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.occulue.bo.Answer", noClassnameStored = true )
 public class Answer extends Base
		{

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public Answer() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the AnswerPrimaryKey
     * @return AnswerPrimaryKey   
     */
    public AnswerPrimaryKey getAnswerPrimaryKey() 
    {    
    	AnswerPrimaryKey key = new AnswerPrimaryKey(); 
		key.setAnswerId( this.answerId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
             /**
    * Returns the Question
  	* @return Question	
	*/                    		    	    	    
	public Question getQuestion() 	    	   
	{
		return this.question;		
	}
	
	/**
              	* Assigns the question
    	* @param question	Question
    	*/
    	public void setQuestion( Question question )
    	{
    		this.question = question;
    	}	
               /**
    * Returns the Response
  	* @return TheResponse	
	*/                    		    	    	    
	public TheResponse getResponse() 	    	   
	{
		return this.response;		
	}
	
	/**
              	* Assigns the response
    	* @param response	TheResponse
    	*/
    	public void setResponse( TheResponse response )
    	{
    		this.response = response;
    	}	
               /**
    * Returns the answerId
  	* @return Long	
	*/                    		    	    	    
	public Long getAnswerId() 	    	   
	{
		return this.answerId;		
	}
	
	/**
              	* Assigns the answerId
    	* @param answerId	Long
    	*/
    	public void setAnswerId( Long answerId )
    	{
    		this.answerId = answerId;
    	}	
  
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	Answer		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( Answer object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Answer:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.answerId = object.getAnswerId();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	Answer		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( Answer object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" Answer:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
    	if ( object.getQuestion() != null)
    	{
    		this.question = new Question();
    		this.question.copyShallow( object.getQuestion() );
    	}
    	else
    		this.question = null;
    	if ( object.getResponse() != null)
    	{
    		this.response = new TheResponse();
    		this.response.copyShallow( object.getResponse() );
    	}
    	else
    		this.response = null;
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "answerId = " + this.answerId + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "Answer" );
		
			identity.append(  "::" );
		identity.append( answerId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("Answer");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof Answer)) 
	        return false;
	        
		Answer bo = (Answer)object;
		
		return( getAnswerPrimaryKey().equals( bo.getAnswerPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
protected Long answerId = null;
protected Question question = null;
protected TheResponse response = null;
// ~AIB

}
