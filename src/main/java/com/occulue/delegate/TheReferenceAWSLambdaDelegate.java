/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * TheReference AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of TheReference related services in the case of a TheReference business related service failing.</li>
 * <li>Exposes a simpler, uniform TheReference interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill TheReference business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "TheReference", description = "RESTful API to interact with TheReference resources.")
@Path("/TheReference")
public class TheReferenceAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public TheReferenceAWSLambdaDelegate() {
	}


    /**
     * Creates the provided TheReference
     * @param		businessObject 	TheReference
	 * @param		context		Context	
     * @return     	TheReference
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a TheReference", notes = "Creates TheReference using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static TheReference createTheReference( 
    		@ApiParam(value = "TheReference entity to create", required = true) TheReference businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null TheReference provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        TheReferenceDAO dao  = getTheReferenceDAO();
        
        try
        {
            businessObject = dao.createTheReference( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "TheReferenceAWSLambdaDelegate:createTheReference() - Unable to create TheReference" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the TheReference via a supplied TheReferencePrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	TheReference
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a TheReference", notes = "Gets the TheReference associated with the provided primary key", response = TheReference.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static TheReference getTheReference( 
    		@ApiParam(value = "TheReference primary key", required = true) TheReferencePrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        TheReference businessObject  	= null;                
        TheReferenceDAO dao 			= getTheReferenceDAO();
            
        try
        {
        	businessObject = dao.findTheReference( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate TheReference with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided TheReference
    * @param		businessObject		TheReference
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a TheReference", notes = "Saves TheReference using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static TheReference saveTheReference( 
    		@ApiParam(value = "TheReference entity to save", required = true) TheReference businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null TheReference provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        TheReferencePrimaryKey key = businessObject.getTheReferencePrimaryKey();
                    
        if ( key != null )
        {
            TheReferenceDAO dao = getTheReferenceDAO();

            try
            {                    
                businessObject = (TheReference)dao.saveTheReference( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save TheReference" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseTheReferenceDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create TheReference due to it having a null TheReferencePrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all TheReferences
     * @param		context		Context
     * @return 	ArrayList<TheReference> 
     */
    @ApiOperation(value = "Get all TheReference", notes = "Get all TheReference from storage", responseContainer = "ArrayList", response = TheReference.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<TheReference> getAllTheReference( Context context ) 
    	throws NotFoundException {

        ArrayList<TheReference> array	= null;
        TheReferenceDAO dao 			= getTheReferenceDAO();
        
        try
        {
            array = dao.findAllTheReference();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllTheReference - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseTheReferenceDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	TheReferencePrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a TheReference", notes = "Deletes the TheReference associated with the provided primary key", response = TheReference.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteTheReference( 
			@ApiParam(value = "TheReference primary key", required = true) TheReferencePrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        TheReferenceDAO dao  = getTheReferenceDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteTheReference( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete TheReference using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseTheReferenceDAO( dao );
        }
         		
        return;
     }

// role related methods

    /**
     * Gets the QuestionGroup using the provided primary key of a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @return    	QuestionGroup
     * @exception	NotFoundException
     */
	public static QuestionGroup getQuestionGroup( TheReferencePrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		QuestionGroupPrimaryKey childKey = theReference.getQuestionGroup().getQuestionGroupPrimaryKey(); 
		QuestionGroup child 				= QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the QuestionGroup on a TheReference using the provided primary key of a QuestionGroup
     * @param		parentKey	TheReferencePrimaryKey
     * @param		parentKey	TheReferencePrimaryKey
     * @param		context		Context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static TheReference saveQuestionGroup( TheReferencePrimaryKey parentKey, 
												QuestionGroupPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		QuestionGroup child 				= QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );

		// assign the QuestionGroup
		theReference.setQuestionGroup( child );
	
		// save the TheReference 
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
		
		return( theReference );
	}

    /**
     * Unassigns the QuestionGroup on a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		Context		context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static TheReference deleteQuestionGroup( TheReferencePrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		TheReference theReference 	= getTheReference( parentKey, context );
		
		if ( theReference.getQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = theReference.getQuestionGroup().getQuestionGroupPrimaryKey();
			
			// first null out the QuestionGroup on the parent so there's no constraint during deletion
			theReference.setQuestionGroup( null );
			TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
			
			// now it is safe to delete the QuestionGroup 
			QuestionGroupAWSLambdaDelegate.deleteQuestionGroup( pk, context );
		}

		return( theReference );
	}
		
    /**
     * Gets the User using the provided primary key of a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @return    	User
     * @exception	NotFoundException
     */
	public static User getUser( TheReferencePrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		UserPrimaryKey childKey = theReference.getUser().getUserPrimaryKey(); 
		User child 				= UserAWSLambdaDelegate.getUser( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the User on a TheReference using the provided primary key of a User
     * @param		parentKey	TheReferencePrimaryKey
     * @param		parentKey	TheReferencePrimaryKey
     * @param		context		Context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static TheReference saveUser( TheReferencePrimaryKey parentKey, 
												UserPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		User child 				= UserAWSLambdaDelegate.getUser( childKey, context );

		// assign the User
		theReference.setUser( child );
	
		// save the TheReference 
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
		
		return( theReference );
	}

    /**
     * Unassigns the User on a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		Context		context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static TheReference deleteUser( TheReferencePrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		TheReference theReference 	= getTheReference( parentKey, context );
		
		if ( theReference.getUser() != null )
		{
			UserPrimaryKey pk = theReference.getUser().getUserPrimaryKey();
			
			// first null out the User on the parent so there's no constraint during deletion
			theReference.setUser( null );
			TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
			
			// now it is safe to delete the User 
			UserAWSLambdaDelegate.deleteUser( pk, context );
		}

		return( theReference );
	}
		
    /**
     * Gets the Referrer using the provided primary key of a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @return    	Referrer
     * @exception	NotFoundException
     */
	public static Referrer getReferrer( TheReferencePrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		ReferrerPrimaryKey childKey = theReference.getReferrer().getReferrerPrimaryKey(); 
		Referrer child 				= ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the Referrer on a TheReference using the provided primary key of a Referrer
     * @param		parentKey	TheReferencePrimaryKey
     * @param		parentKey	TheReferencePrimaryKey
     * @param		context		Context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static TheReference saveReferrer( TheReferencePrimaryKey parentKey, 
												ReferrerPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		Referrer child 				= ReferrerAWSLambdaDelegate.getReferrer( childKey, context );

		// assign the Referrer
		theReference.setReferrer( child );
	
		// save the TheReference 
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
		
		return( theReference );
	}

    /**
     * Unassigns the Referrer on a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		Context		context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static TheReference deleteReferrer( TheReferencePrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		TheReference theReference 	= getTheReference( parentKey, context );
		
		if ( theReference.getReferrer() != null )
		{
			ReferrerPrimaryKey pk = theReference.getReferrer().getReferrerPrimaryKey();
			
			// first null out the Referrer on the parent so there's no constraint during deletion
			theReference.setReferrer( null );
			TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
			
			// now it is safe to delete the Referrer 
			ReferrerAWSLambdaDelegate.deleteReferrer( pk, context );
		}

		return( theReference );
	}
		
    /**
     * Gets the LastQuestionAnswered using the provided primary key of a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @return    	Question
     * @exception	NotFoundException
     */
	public static Question getLastQuestionAnswered( TheReferencePrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		QuestionPrimaryKey childKey = theReference.getLastQuestionAnswered().getQuestionPrimaryKey(); 
		Question child 				= QuestionAWSLambdaDelegate.getQuestion( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the LastQuestionAnswered on a TheReference using the provided primary key of a Question
     * @param		parentKey	TheReferencePrimaryKey
     * @param		parentKey	TheReferencePrimaryKey
     * @param		context		Context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static TheReference saveLastQuestionAnswered( TheReferencePrimaryKey parentKey, 
												QuestionPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		TheReference theReference 	= getTheReference( parentKey, context );
		Question child 				= QuestionAWSLambdaDelegate.getQuestion( childKey, context );

		// assign the LastQuestionAnswered
		theReference.setLastQuestionAnswered( child );
	
		// save the TheReference 
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
		
		return( theReference );
	}

    /**
     * Unassigns the LastQuestionAnswered on a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		Context		context
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static TheReference deleteLastQuestionAnswered( TheReferencePrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		TheReference theReference 	= getTheReference( parentKey, context );
		
		if ( theReference.getLastQuestionAnswered() != null )
		{
			QuestionPrimaryKey pk = theReference.getLastQuestionAnswered().getQuestionPrimaryKey();
			
			// first null out the Question on the parent so there's no constraint during deletion
			theReference.setLastQuestionAnswered( null );
			TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
			
			// now it is safe to delete the LastQuestionAnswered 
			QuestionAWSLambdaDelegate.deleteQuestion( pk, context );
		}

		return( theReference );
	}
		
 
    /**
     * Retrieves the Answers on a TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		context		Context
     * @return    	Set<Answer>
     * @exception	NotFoundException
     */
	public static Set<Answer> getAnswers( TheReferencePrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	TheReference theReference 	= getTheReference( parentKey, context );
    	return (theReference.getAnswers());
    }
    
    /**
     * Add the assigned Answer into the Answers of the relevant TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		childKey	AnswerPrimaryKey
	 * @param		context		Context
     * @return    	TheReference
     * @exception	NotFoundException
     */
	public static TheReference addAnswers( 	TheReferencePrimaryKey parentKey, 
									AnswerPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		TheReference theReference 	= getTheReference( parentKey, context );

		// find the Answer
		Answer child = AnswerAWSLambdaDelegate.getAnswer( childKey, context );
		
		// add it to the Answers 
		theReference.getAnswers().add( child );				
		
		// save the TheReference
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );

		return ( theReference );
	}

    /**
     * Saves multiple Answer entities as the Answers to the relevant TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		List<AnswerPrimaryKey> childKeys
     * @return    	TheReference
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public TheReference assignAnswers( TheReferencePrimaryKey parentKey, 
											List<AnswerPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		TheReference theReference 	= getTheReference( parentKey, context );
		
		// clear out the Answers 
		theReference.getAnswers().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			Answer child = null;
			for( AnswerPrimaryKey childKey : childKeys )
			{
				// retrieve the Answer
				child = AnswerAWSLambdaDelegate.getAnswer( childKey, context );

				// add it to the Answers List
				theReference.getAnswers().add( child );
			}
		}
		
		// save the TheReference
		theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );

		return( theReference );
	}

    /**
     * Delete multiple Answer entities as the Answers to the relevant TheReference
     * @param		parentKey	TheReferencePrimaryKey
     * @param		List<AnswerPrimaryKey> childKeys
     * @return    	TheReference
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public TheReference deleteAnswers( TheReferencePrimaryKey parentKey, 
											List<AnswerPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		TheReference theReference 	= getTheReference( parentKey, context );

		if ( childKeys != null )
		{
			Set<Answer> children	= theReference.getAnswers();
			Answer child 			= null;
			
			for( AnswerPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = AnswerAWSLambdaDelegate.getAnswer( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					AnswerAWSLambdaDelegate.deleteAnswer( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of Answer back to the theReference
			theReference.setAnswers( children );			
			// save it 
			theReference = TheReferenceAWSLambdaDelegate.saveTheReference( theReference, context );
		}
		
		return ( theReference );
	}

 	
    /**
     * Returns the TheReference specific DAO.
     *
     * @return      TheReference DAO
     */
    public static TheReferenceDAO getTheReferenceDAO()
    {
        return( new com.occulue.dao.TheReferenceDAO() ); 
    }

    /**
     * Release the TheReferenceDAO back to the FrameworkDAOFactory
     */
    public static void releaseTheReferenceDAO( com.occulue.dao.TheReferenceDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(TheReferenceAWSLambdaDelegate.class.getName());
}

