/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Answer AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Answer related services in the case of a Answer business related service failing.</li>
 * <li>Exposes a simpler, uniform Answer interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Answer business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Answer", description = "RESTful API to interact with Answer resources.")
@Path("/Answer")
public class AnswerAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public AnswerAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Answer
     * @param		businessObject 	Answer
	 * @param		context		Context	
     * @return     	Answer
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Answer", notes = "Creates Answer using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Answer createAnswer( 
    		@ApiParam(value = "Answer entity to create", required = true) Answer businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Answer provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        AnswerDAO dao  = getAnswerDAO();
        
        try
        {
            businessObject = dao.createAnswer( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "AnswerAWSLambdaDelegate:createAnswer() - Unable to create Answer" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Answer via a supplied AnswerPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Answer
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Answer", notes = "Gets the Answer associated with the provided primary key", response = Answer.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Answer getAnswer( 
    		@ApiParam(value = "Answer primary key", required = true) AnswerPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Answer businessObject  	= null;                
        AnswerDAO dao 			= getAnswerDAO();
            
        try
        {
        	businessObject = dao.findAnswer( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Answer with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Answer
    * @param		businessObject		Answer
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Answer", notes = "Saves Answer using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Answer saveAnswer( 
    		@ApiParam(value = "Answer entity to save", required = true) Answer businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Answer provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AnswerPrimaryKey key = businessObject.getAnswerPrimaryKey();
                    
        if ( key != null )
        {
            AnswerDAO dao = getAnswerDAO();

            try
            {                    
                businessObject = (Answer)dao.saveAnswer( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Answer" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseAnswerDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Answer due to it having a null AnswerPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Answers
     * @param		context		Context
     * @return 	ArrayList<Answer> 
     */
    @ApiOperation(value = "Get all Answer", notes = "Get all Answer from storage", responseContainer = "ArrayList", response = Answer.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Answer> getAllAnswer( Context context ) 
    	throws NotFoundException {

        ArrayList<Answer> array	= null;
        AnswerDAO dao 			= getAnswerDAO();
        
        try
        {
            array = dao.findAllAnswer();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllAnswer - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseAnswerDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	AnswerPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Answer", notes = "Deletes the Answer associated with the provided primary key", response = Answer.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteAnswer( 
			@ApiParam(value = "Answer primary key", required = true) AnswerPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        AnswerDAO dao  = getAnswerDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteAnswer( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Answer using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseAnswerDAO( dao );
        }
         		
        return;
     }

// role related methods

    /**
     * Gets the Question using the provided primary key of a Answer
     * @param		parentKey	AnswerPrimaryKey
     * @return    	Question
     * @exception	NotFoundException
     */
	public static Question getQuestion( AnswerPrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		Answer answer 	= getAnswer( parentKey, context );
		QuestionPrimaryKey childKey = answer.getQuestion().getQuestionPrimaryKey(); 
		Question child 				= QuestionAWSLambdaDelegate.getQuestion( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the Question on a Answer using the provided primary key of a Question
     * @param		parentKey	AnswerPrimaryKey
     * @param		parentKey	AnswerPrimaryKey
     * @param		context		Context
     * @return    	Answer
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static Answer saveQuestion( AnswerPrimaryKey parentKey, 
												QuestionPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		Answer answer 	= getAnswer( parentKey, context );
		Question child 				= QuestionAWSLambdaDelegate.getQuestion( childKey, context );

		// assign the Question
		answer.setQuestion( child );
	
		// save the Answer 
		answer = AnswerAWSLambdaDelegate.saveAnswer( answer, context );
		
		return( answer );
	}

    /**
     * Unassigns the Question on a Answer
     * @param		parentKey	AnswerPrimaryKey
     * @param		Context		context
     * @return    	Answer
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static Answer deleteQuestion( AnswerPrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		Answer answer 	= getAnswer( parentKey, context );
		
		if ( answer.getQuestion() != null )
		{
			QuestionPrimaryKey pk = answer.getQuestion().getQuestionPrimaryKey();
			
			// first null out the Question on the parent so there's no constraint during deletion
			answer.setQuestion( null );
			AnswerAWSLambdaDelegate.saveAnswer( answer, context );
			
			// now it is safe to delete the Question 
			QuestionAWSLambdaDelegate.deleteQuestion( pk, context );
		}

		return( answer );
	}
		
    /**
     * Gets the Response using the provided primary key of a Answer
     * @param		parentKey	AnswerPrimaryKey
     * @return    	TheResponse
     * @exception	NotFoundException
     */
	public static TheResponse getResponse( AnswerPrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		Answer answer 	= getAnswer( parentKey, context );
		TheResponsePrimaryKey childKey = answer.getResponse().getTheResponsePrimaryKey(); 
		TheResponse child 				= TheResponseAWSLambdaDelegate.getTheResponse( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the Response on a Answer using the provided primary key of a TheResponse
     * @param		parentKey	AnswerPrimaryKey
     * @param		parentKey	AnswerPrimaryKey
     * @param		context		Context
     * @return    	Answer
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static Answer saveResponse( AnswerPrimaryKey parentKey, 
												TheResponsePrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		Answer answer 	= getAnswer( parentKey, context );
		TheResponse child 				= TheResponseAWSLambdaDelegate.getTheResponse( childKey, context );

		// assign the Response
		answer.setResponse( child );
	
		// save the Answer 
		answer = AnswerAWSLambdaDelegate.saveAnswer( answer, context );
		
		return( answer );
	}

    /**
     * Unassigns the Response on a Answer
     * @param		parentKey	AnswerPrimaryKey
     * @param		Context		context
     * @return    	Answer
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static Answer deleteResponse( AnswerPrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		Answer answer 	= getAnswer( parentKey, context );
		
		if ( answer.getResponse() != null )
		{
			TheResponsePrimaryKey pk = answer.getResponse().getTheResponsePrimaryKey();
			
			// first null out the TheResponse on the parent so there's no constraint during deletion
			answer.setResponse( null );
			AnswerAWSLambdaDelegate.saveAnswer( answer, context );
			
			// now it is safe to delete the Response 
			TheResponseAWSLambdaDelegate.deleteTheResponse( pk, context );
		}

		return( answer );
	}
		
 
 	
    /**
     * Returns the Answer specific DAO.
     *
     * @return      Answer DAO
     */
    public static AnswerDAO getAnswerDAO()
    {
        return( new com.occulue.dao.AnswerDAO() ); 
    }

    /**
     * Release the AnswerDAO back to the FrameworkDAOFactory
     */
    public static void releaseAnswerDAO( com.occulue.dao.AnswerDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(AnswerAWSLambdaDelegate.class.getName());
}

