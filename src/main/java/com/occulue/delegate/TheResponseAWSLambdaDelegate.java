/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * TheResponse AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of TheResponse related services in the case of a TheResponse business related service failing.</li>
 * <li>Exposes a simpler, uniform TheResponse interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill TheResponse business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "TheResponse", description = "RESTful API to interact with TheResponse resources.")
@Path("/TheResponse")
public class TheResponseAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public TheResponseAWSLambdaDelegate() {
	}


    /**
     * Creates the provided TheResponse
     * @param		businessObject 	TheResponse
	 * @param		context		Context	
     * @return     	TheResponse
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a TheResponse", notes = "Creates TheResponse using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static TheResponse createTheResponse( 
    		@ApiParam(value = "TheResponse entity to create", required = true) TheResponse businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null TheResponse provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        TheResponseDAO dao  = getTheResponseDAO();
        
        try
        {
            businessObject = dao.createTheResponse( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "TheResponseAWSLambdaDelegate:createTheResponse() - Unable to create TheResponse" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the TheResponse via a supplied TheResponsePrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	TheResponse
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a TheResponse", notes = "Gets the TheResponse associated with the provided primary key", response = TheResponse.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static TheResponse getTheResponse( 
    		@ApiParam(value = "TheResponse primary key", required = true) TheResponsePrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        TheResponse businessObject  	= null;                
        TheResponseDAO dao 			= getTheResponseDAO();
            
        try
        {
        	businessObject = dao.findTheResponse( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate TheResponse with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided TheResponse
    * @param		businessObject		TheResponse
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a TheResponse", notes = "Saves TheResponse using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static TheResponse saveTheResponse( 
    		@ApiParam(value = "TheResponse entity to save", required = true) TheResponse businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null TheResponse provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        TheResponsePrimaryKey key = businessObject.getTheResponsePrimaryKey();
                    
        if ( key != null )
        {
            TheResponseDAO dao = getTheResponseDAO();

            try
            {                    
                businessObject = (TheResponse)dao.saveTheResponse( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save TheResponse" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseTheResponseDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create TheResponse due to it having a null TheResponsePrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all TheResponses
     * @param		context		Context
     * @return 	ArrayList<TheResponse> 
     */
    @ApiOperation(value = "Get all TheResponse", notes = "Get all TheResponse from storage", responseContainer = "ArrayList", response = TheResponse.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<TheResponse> getAllTheResponse( Context context ) 
    	throws NotFoundException {

        ArrayList<TheResponse> array	= null;
        TheResponseDAO dao 			= getTheResponseDAO();
        
        try
        {
            array = dao.findAllTheResponse();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllTheResponse - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseTheResponseDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	TheResponsePrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a TheResponse", notes = "Deletes the TheResponse associated with the provided primary key", response = TheResponse.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteTheResponse( 
			@ApiParam(value = "TheResponse primary key", required = true) TheResponsePrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        TheResponseDAO dao  = getTheResponseDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteTheResponse( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete TheResponse using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseTheResponseDAO( dao );
        }
         		
        return;
     }

// role related methods

 
 	
    /**
     * Returns the TheResponse specific DAO.
     *
     * @return      TheResponse DAO
     */
    public static TheResponseDAO getTheResponseDAO()
    {
        return( new com.occulue.dao.TheResponseDAO() ); 
    }

    /**
     * Release the TheResponseDAO back to the FrameworkDAOFactory
     */
    public static void releaseTheResponseDAO( com.occulue.dao.TheResponseDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(TheResponseAWSLambdaDelegate.class.getName());
}

