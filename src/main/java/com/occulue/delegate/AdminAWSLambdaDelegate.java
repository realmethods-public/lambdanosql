/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Admin AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Admin related services in the case of a Admin business related service failing.</li>
 * <li>Exposes a simpler, uniform Admin interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Admin business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Admin", description = "RESTful API to interact with Admin resources.")
@Path("/Admin")
public class AdminAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public AdminAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Admin
     * @param		businessObject 	Admin
	 * @param		context		Context	
     * @return     	Admin
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Admin", notes = "Creates Admin using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Admin createAdmin( 
    		@ApiParam(value = "Admin entity to create", required = true) Admin businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Admin provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        AdminDAO dao  = getAdminDAO();
        
        try
        {
            businessObject = dao.createAdmin( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "AdminAWSLambdaDelegate:createAdmin() - Unable to create Admin" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Admin via a supplied AdminPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Admin
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Admin", notes = "Gets the Admin associated with the provided primary key", response = Admin.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Admin getAdmin( 
    		@ApiParam(value = "Admin primary key", required = true) AdminPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Admin businessObject  	= null;                
        AdminDAO dao 			= getAdminDAO();
            
        try
        {
        	businessObject = dao.findAdmin( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Admin with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Admin
    * @param		businessObject		Admin
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Admin", notes = "Saves Admin using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Admin saveAdmin( 
    		@ApiParam(value = "Admin entity to save", required = true) Admin businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Admin provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        AdminPrimaryKey key = businessObject.getAdminPrimaryKey();
                    
        if ( key != null )
        {
            AdminDAO dao = getAdminDAO();

            try
            {                    
                businessObject = (Admin)dao.saveAdmin( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Admin" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseAdminDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Admin due to it having a null AdminPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Admins
     * @param		context		Context
     * @return 	ArrayList<Admin> 
     */
    @ApiOperation(value = "Get all Admin", notes = "Get all Admin from storage", responseContainer = "ArrayList", response = Admin.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Admin> getAllAdmin( Context context ) 
    	throws NotFoundException {

        ArrayList<Admin> array	= null;
        AdminDAO dao 			= getAdminDAO();
        
        try
        {
            array = dao.findAllAdmin();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllAdmin - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseAdminDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	AdminPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Admin", notes = "Deletes the Admin associated with the provided primary key", response = Admin.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteAdmin( 
			@ApiParam(value = "Admin primary key", required = true) AdminPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        AdminDAO dao  = getAdminDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteAdmin( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Admin using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseAdminDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the Users on a Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		context		Context
     * @return    	Set<User>
     * @exception	NotFoundException
     */
	public static Set<User> getUsers( AdminPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	Admin admin 	= getAdmin( parentKey, context );
    	return (admin.getUsers());
    }
    
    /**
     * Add the assigned User into the Users of the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		childKey	UserPrimaryKey
	 * @param		context		Context
     * @return    	Admin
     * @exception	NotFoundException
     */
	public static Admin addUsers( 	AdminPrimaryKey parentKey, 
									UserPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		Admin admin 	= getAdmin( parentKey, context );

		// find the User
		User child = UserAWSLambdaDelegate.getUser( childKey, context );
		
		// add it to the Users 
		admin.getUsers().add( child );				
		
		// save the Admin
		admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );

		return ( admin );
	}

    /**
     * Saves multiple User entities as the Users to the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		List<UserPrimaryKey> childKeys
     * @return    	Admin
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public Admin assignUsers( AdminPrimaryKey parentKey, 
											List<UserPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		Admin admin 	= getAdmin( parentKey, context );
		
		// clear out the Users 
		admin.getUsers().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			User child = null;
			for( UserPrimaryKey childKey : childKeys )
			{
				// retrieve the User
				child = UserAWSLambdaDelegate.getUser( childKey, context );

				// add it to the Users List
				admin.getUsers().add( child );
			}
		}
		
		// save the Admin
		admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );

		return( admin );
	}

    /**
     * Delete multiple User entities as the Users to the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		List<UserPrimaryKey> childKeys
     * @return    	Admin
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public Admin deleteUsers( AdminPrimaryKey parentKey, 
											List<UserPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		Admin admin 	= getAdmin( parentKey, context );

		if ( childKeys != null )
		{
			Set<User> children	= admin.getUsers();
			User child 			= null;
			
			for( UserPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = UserAWSLambdaDelegate.getUser( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					UserAWSLambdaDelegate.deleteUser( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of User back to the admin
			admin.setUsers( children );			
			// save it 
			admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );
		}
		
		return ( admin );
	}

    /**
     * Retrieves the ReferenceEngines on a Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		context		Context
     * @return    	Set<ReferenceEngine>
     * @exception	NotFoundException
     */
	public static Set<ReferenceEngine> getReferenceEngines( AdminPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	Admin admin 	= getAdmin( parentKey, context );
    	return (admin.getReferenceEngines());
    }
    
    /**
     * Add the assigned ReferenceEngine into the ReferenceEngines of the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		childKey	ReferenceEnginePrimaryKey
	 * @param		context		Context
     * @return    	Admin
     * @exception	NotFoundException
     */
	public static Admin addReferenceEngines( 	AdminPrimaryKey parentKey, 
									ReferenceEnginePrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		Admin admin 	= getAdmin( parentKey, context );

		// find the ReferenceEngine
		ReferenceEngine child = ReferenceEngineAWSLambdaDelegate.getReferenceEngine( childKey, context );
		
		// add it to the ReferenceEngines 
		admin.getReferenceEngines().add( child );				
		
		// save the Admin
		admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );

		return ( admin );
	}

    /**
     * Saves multiple ReferenceEngine entities as the ReferenceEngines to the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		List<ReferenceEnginePrimaryKey> childKeys
     * @return    	Admin
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public Admin assignReferenceEngines( AdminPrimaryKey parentKey, 
											List<ReferenceEnginePrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		Admin admin 	= getAdmin( parentKey, context );
		
		// clear out the ReferenceEngines 
		admin.getReferenceEngines().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			ReferenceEngine child = null;
			for( ReferenceEnginePrimaryKey childKey : childKeys )
			{
				// retrieve the ReferenceEngine
				child = ReferenceEngineAWSLambdaDelegate.getReferenceEngine( childKey, context );

				// add it to the ReferenceEngines List
				admin.getReferenceEngines().add( child );
			}
		}
		
		// save the Admin
		admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );

		return( admin );
	}

    /**
     * Delete multiple ReferenceEngine entities as the ReferenceEngines to the relevant Admin
     * @param		parentKey	AdminPrimaryKey
     * @param		List<ReferenceEnginePrimaryKey> childKeys
     * @return    	Admin
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public Admin deleteReferenceEngines( AdminPrimaryKey parentKey, 
											List<ReferenceEnginePrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		Admin admin 	= getAdmin( parentKey, context );

		if ( childKeys != null )
		{
			Set<ReferenceEngine> children	= admin.getReferenceEngines();
			ReferenceEngine child 			= null;
			
			for( ReferenceEnginePrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = ReferenceEngineAWSLambdaDelegate.getReferenceEngine( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					ReferenceEngineAWSLambdaDelegate.deleteReferenceEngine( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of ReferenceEngine back to the admin
			admin.setReferenceEngines( children );			
			// save it 
			admin = AdminAWSLambdaDelegate.saveAdmin( admin, context );
		}
		
		return ( admin );
	}

 	
    /**
     * Returns the Admin specific DAO.
     *
     * @return      Admin DAO
     */
    public static AdminDAO getAdminDAO()
    {
        return( new com.occulue.dao.AdminDAO() ); 
    }

    /**
     * Release the AdminDAO back to the FrameworkDAOFactory
     */
    public static void releaseAdminDAO( com.occulue.dao.AdminDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(AdminAWSLambdaDelegate.class.getName());
}

