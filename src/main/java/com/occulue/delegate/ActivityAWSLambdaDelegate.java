/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Activity AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Activity related services in the case of a Activity business related service failing.</li>
 * <li>Exposes a simpler, uniform Activity interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Activity business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Activity", description = "RESTful API to interact with Activity resources.")
@Path("/Activity")
public class ActivityAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ActivityAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Activity
     * @param		businessObject 	Activity
	 * @param		context		Context	
     * @return     	Activity
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Activity", notes = "Creates Activity using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Activity createActivity( 
    		@ApiParam(value = "Activity entity to create", required = true) Activity businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Activity provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        ActivityDAO dao  = getActivityDAO();
        
        try
        {
            businessObject = dao.createActivity( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "ActivityAWSLambdaDelegate:createActivity() - Unable to create Activity" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseActivityDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Activity via a supplied ActivityPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Activity
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Activity", notes = "Gets the Activity associated with the provided primary key", response = Activity.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Activity getActivity( 
    		@ApiParam(value = "Activity primary key", required = true) ActivityPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Activity businessObject  	= null;                
        ActivityDAO dao 			= getActivityDAO();
            
        try
        {
        	businessObject = dao.findActivity( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Activity with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseActivityDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Activity
    * @param		businessObject		Activity
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Activity", notes = "Saves Activity using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Activity saveActivity( 
    		@ApiParam(value = "Activity entity to save", required = true) Activity businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Activity provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ActivityPrimaryKey key = businessObject.getActivityPrimaryKey();
                    
        if ( key != null )
        {
            ActivityDAO dao = getActivityDAO();

            try
            {                    
                businessObject = (Activity)dao.saveActivity( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Activity" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseActivityDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Activity due to it having a null ActivityPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Activitys
     * @param		context		Context
     * @return 	ArrayList<Activity> 
     */
    @ApiOperation(value = "Get all Activity", notes = "Get all Activity from storage", responseContainer = "ArrayList", response = Activity.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Activity> getAllActivity( Context context ) 
    	throws NotFoundException {

        ArrayList<Activity> array	= null;
        ActivityDAO dao 			= getActivityDAO();
        
        try
        {
            array = dao.findAllActivity();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllActivity - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseActivityDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	ActivityPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Activity", notes = "Deletes the Activity associated with the provided primary key", response = Activity.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteActivity( 
			@ApiParam(value = "Activity primary key", required = true) ActivityPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        ActivityDAO dao  = getActivityDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteActivity( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Activity using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseActivityDAO( dao );
        }
         		
        return;
     }

// role related methods

    /**
     * Gets the User using the provided primary key of a Activity
     * @param		parentKey	ActivityPrimaryKey
     * @return    	User
     * @exception	NotFoundException
     */
	public static User getUser( ActivityPrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		Activity activity 	= getActivity( parentKey, context );
		UserPrimaryKey childKey = activity.getUser().getUserPrimaryKey(); 
		User child 				= UserAWSLambdaDelegate.getUser( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the User on a Activity using the provided primary key of a User
     * @param		parentKey	ActivityPrimaryKey
     * @param		parentKey	ActivityPrimaryKey
     * @param		context		Context
     * @return    	Activity
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static Activity saveUser( ActivityPrimaryKey parentKey, 
												UserPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		Activity activity 	= getActivity( parentKey, context );
		User child 				= UserAWSLambdaDelegate.getUser( childKey, context );

		// assign the User
		activity.setUser( child );
	
		// save the Activity 
		activity = ActivityAWSLambdaDelegate.saveActivity( activity, context );
		
		return( activity );
	}

    /**
     * Unassigns the User on a Activity
     * @param		parentKey	ActivityPrimaryKey
     * @param		Context		context
     * @return    	Activity
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static Activity deleteUser( ActivityPrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		Activity activity 	= getActivity( parentKey, context );
		
		if ( activity.getUser() != null )
		{
			UserPrimaryKey pk = activity.getUser().getUserPrimaryKey();
			
			// first null out the User on the parent so there's no constraint during deletion
			activity.setUser( null );
			ActivityAWSLambdaDelegate.saveActivity( activity, context );
			
			// now it is safe to delete the User 
			UserAWSLambdaDelegate.deleteUser( pk, context );
		}

		return( activity );
	}
		
 
 	
    /**
     * Returns the Activity specific DAO.
     *
     * @return      Activity DAO
     */
    public static ActivityDAO getActivityDAO()
    {
        return( new com.occulue.dao.ActivityDAO() ); 
    }

    /**
     * Release the ActivityDAO back to the FrameworkDAOFactory
     */
    public static void releaseActivityDAO( com.occulue.dao.ActivityDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(ActivityAWSLambdaDelegate.class.getName());
}

