/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * ReferrerGroup AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferrerGroup related services in the case of a ReferrerGroup business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferrerGroup interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferrerGroup business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "ReferrerGroup", description = "RESTful API to interact with ReferrerGroup resources.")
@Path("/ReferrerGroup")
public class ReferrerGroupAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferrerGroupAWSLambdaDelegate() {
	}


    /**
     * Creates the provided ReferrerGroup
     * @param		businessObject 	ReferrerGroup
	 * @param		context		Context	
     * @return     	ReferrerGroup
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a ReferrerGroup", notes = "Creates ReferrerGroup using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferrerGroup createReferrerGroup( 
    		@ApiParam(value = "ReferrerGroup entity to create", required = true) ReferrerGroup businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null ReferrerGroup provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        ReferrerGroupDAO dao  = getReferrerGroupDAO();
        
        try
        {
            businessObject = dao.createReferrerGroup( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "ReferrerGroupAWSLambdaDelegate:createReferrerGroup() - Unable to create ReferrerGroup" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the ReferrerGroup via a supplied ReferrerGroupPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	ReferrerGroup
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a ReferrerGroup", notes = "Gets the ReferrerGroup associated with the provided primary key", response = ReferrerGroup.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ReferrerGroup getReferrerGroup( 
    		@ApiParam(value = "ReferrerGroup primary key", required = true) ReferrerGroupPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        ReferrerGroup businessObject  	= null;                
        ReferrerGroupDAO dao 			= getReferrerGroupDAO();
            
        try
        {
        	businessObject = dao.findReferrerGroup( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate ReferrerGroup with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided ReferrerGroup
    * @param		businessObject		ReferrerGroup
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a ReferrerGroup", notes = "Saves ReferrerGroup using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferrerGroup saveReferrerGroup( 
    		@ApiParam(value = "ReferrerGroup entity to save", required = true) ReferrerGroup businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null ReferrerGroup provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferrerGroupPrimaryKey key = businessObject.getReferrerGroupPrimaryKey();
                    
        if ( key != null )
        {
            ReferrerGroupDAO dao = getReferrerGroupDAO();

            try
            {                    
                businessObject = (ReferrerGroup)dao.saveReferrerGroup( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save ReferrerGroup" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseReferrerGroupDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create ReferrerGroup due to it having a null ReferrerGroupPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all ReferrerGroups
     * @param		context		Context
     * @return 	ArrayList<ReferrerGroup> 
     */
    @ApiOperation(value = "Get all ReferrerGroup", notes = "Get all ReferrerGroup from storage", responseContainer = "ArrayList", response = ReferrerGroup.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<ReferrerGroup> getAllReferrerGroup( Context context ) 
    	throws NotFoundException {

        ArrayList<ReferrerGroup> array	= null;
        ReferrerGroupDAO dao 			= getReferrerGroupDAO();
        
        try
        {
            array = dao.findAllReferrerGroup();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllReferrerGroup - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseReferrerGroupDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	ReferrerGroupPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a ReferrerGroup", notes = "Deletes the ReferrerGroup associated with the provided primary key", response = ReferrerGroup.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteReferrerGroup( 
			@ApiParam(value = "ReferrerGroup primary key", required = true) ReferrerGroupPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        ReferrerGroupDAO dao  = getReferrerGroupDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteReferrerGroup( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete ReferrerGroup using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseReferrerGroupDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the References on a ReferrerGroup
     * @param		parentKey	ReferrerGroupPrimaryKey
     * @param		context		Context
     * @return    	Set<TheReference>
     * @exception	NotFoundException
     */
	public static Set<TheReference> getReferences( ReferrerGroupPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	ReferrerGroup referrerGroup 	= getReferrerGroup( parentKey, context );
    	return (referrerGroup.getReferences());
    }
    
    /**
     * Add the assigned TheReference into the References of the relevant ReferrerGroup
     * @param		parentKey	ReferrerGroupPrimaryKey
     * @param		childKey	TheReferencePrimaryKey
	 * @param		context		Context
     * @return    	ReferrerGroup
     * @exception	NotFoundException
     */
	public static ReferrerGroup addReferences( 	ReferrerGroupPrimaryKey parentKey, 
									TheReferencePrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		ReferrerGroup referrerGroup 	= getReferrerGroup( parentKey, context );

		// find the TheReference
		TheReference child = TheReferenceAWSLambdaDelegate.getTheReference( childKey, context );
		
		// add it to the References 
		referrerGroup.getReferences().add( child );				
		
		// save the ReferrerGroup
		referrerGroup = ReferrerGroupAWSLambdaDelegate.saveReferrerGroup( referrerGroup, context );

		return ( referrerGroup );
	}

    /**
     * Saves multiple TheReference entities as the References to the relevant ReferrerGroup
     * @param		parentKey	ReferrerGroupPrimaryKey
     * @param		List<TheReferencePrimaryKey> childKeys
     * @return    	ReferrerGroup
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public ReferrerGroup assignReferences( ReferrerGroupPrimaryKey parentKey, 
											List<TheReferencePrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		ReferrerGroup referrerGroup 	= getReferrerGroup( parentKey, context );
		
		// clear out the References 
		referrerGroup.getReferences().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			TheReference child = null;
			for( TheReferencePrimaryKey childKey : childKeys )
			{
				// retrieve the TheReference
				child = TheReferenceAWSLambdaDelegate.getTheReference( childKey, context );

				// add it to the References List
				referrerGroup.getReferences().add( child );
			}
		}
		
		// save the ReferrerGroup
		referrerGroup = ReferrerGroupAWSLambdaDelegate.saveReferrerGroup( referrerGroup, context );

		return( referrerGroup );
	}

    /**
     * Delete multiple TheReference entities as the References to the relevant ReferrerGroup
     * @param		parentKey	ReferrerGroupPrimaryKey
     * @param		List<TheReferencePrimaryKey> childKeys
     * @return    	ReferrerGroup
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public ReferrerGroup deleteReferences( ReferrerGroupPrimaryKey parentKey, 
											List<TheReferencePrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		ReferrerGroup referrerGroup 	= getReferrerGroup( parentKey, context );

		if ( childKeys != null )
		{
			Set<TheReference> children	= referrerGroup.getReferences();
			TheReference child 			= null;
			
			for( TheReferencePrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = TheReferenceAWSLambdaDelegate.getTheReference( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					TheReferenceAWSLambdaDelegate.deleteTheReference( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of TheReference back to the referrerGroup
			referrerGroup.setReferences( children );			
			// save it 
			referrerGroup = ReferrerGroupAWSLambdaDelegate.saveReferrerGroup( referrerGroup, context );
		}
		
		return ( referrerGroup );
	}

 	
    /**
     * Returns the ReferrerGroup specific DAO.
     *
     * @return      ReferrerGroup DAO
     */
    public static ReferrerGroupDAO getReferrerGroupDAO()
    {
        return( new com.occulue.dao.ReferrerGroupDAO() ); 
    }

    /**
     * Release the ReferrerGroupDAO back to the FrameworkDAOFactory
     */
    public static void releaseReferrerGroupDAO( com.occulue.dao.ReferrerGroupDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(ReferrerGroupAWSLambdaDelegate.class.getName());
}

