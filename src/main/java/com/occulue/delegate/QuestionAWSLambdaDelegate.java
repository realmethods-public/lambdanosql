/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Question AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Question related services in the case of a Question business related service failing.</li>
 * <li>Exposes a simpler, uniform Question interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Question business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Question", description = "RESTful API to interact with Question resources.")
@Path("/Question")
public class QuestionAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public QuestionAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Question
     * @param		businessObject 	Question
	 * @param		context		Context	
     * @return     	Question
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Question", notes = "Creates Question using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Question createQuestion( 
    		@ApiParam(value = "Question entity to create", required = true) Question businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Question provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        QuestionDAO dao  = getQuestionDAO();
        
        try
        {
            businessObject = dao.createQuestion( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "QuestionAWSLambdaDelegate:createQuestion() - Unable to create Question" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Question via a supplied QuestionPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Question
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Question", notes = "Gets the Question associated with the provided primary key", response = Question.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Question getQuestion( 
    		@ApiParam(value = "Question primary key", required = true) QuestionPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Question businessObject  	= null;                
        QuestionDAO dao 			= getQuestionDAO();
            
        try
        {
        	businessObject = dao.findQuestion( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Question with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Question
    * @param		businessObject		Question
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Question", notes = "Saves Question using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Question saveQuestion( 
    		@ApiParam(value = "Question entity to save", required = true) Question businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Question provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        QuestionPrimaryKey key = businessObject.getQuestionPrimaryKey();
                    
        if ( key != null )
        {
            QuestionDAO dao = getQuestionDAO();

            try
            {                    
                businessObject = (Question)dao.saveQuestion( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Question" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseQuestionDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Question due to it having a null QuestionPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Questions
     * @param		context		Context
     * @return 	ArrayList<Question> 
     */
    @ApiOperation(value = "Get all Question", notes = "Get all Question from storage", responseContainer = "ArrayList", response = Question.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Question> getAllQuestion( Context context ) 
    	throws NotFoundException {

        ArrayList<Question> array	= null;
        QuestionDAO dao 			= getQuestionDAO();
        
        try
        {
            array = dao.findAllQuestion();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllQuestion - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseQuestionDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	QuestionPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Question", notes = "Deletes the Question associated with the provided primary key", response = Question.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteQuestion( 
			@ApiParam(value = "Question primary key", required = true) QuestionPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        QuestionDAO dao  = getQuestionDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteQuestion( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Question using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseQuestionDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the Responses on a Question
     * @param		parentKey	QuestionPrimaryKey
     * @param		context		Context
     * @return    	Set<TheResponse>
     * @exception	NotFoundException
     */
	public static Set<TheResponse> getResponses( QuestionPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	Question question 	= getQuestion( parentKey, context );
    	return (question.getResponses());
    }
    
    /**
     * Add the assigned TheResponse into the Responses of the relevant Question
     * @param		parentKey	QuestionPrimaryKey
     * @param		childKey	TheResponsePrimaryKey
	 * @param		context		Context
     * @return    	Question
     * @exception	NotFoundException
     */
	public static Question addResponses( 	QuestionPrimaryKey parentKey, 
									TheResponsePrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		Question question 	= getQuestion( parentKey, context );

		// find the TheResponse
		TheResponse child = TheResponseAWSLambdaDelegate.getTheResponse( childKey, context );
		
		// add it to the Responses 
		question.getResponses().add( child );				
		
		// save the Question
		question = QuestionAWSLambdaDelegate.saveQuestion( question, context );

		return ( question );
	}

    /**
     * Saves multiple TheResponse entities as the Responses to the relevant Question
     * @param		parentKey	QuestionPrimaryKey
     * @param		List<TheResponsePrimaryKey> childKeys
     * @return    	Question
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public Question assignResponses( QuestionPrimaryKey parentKey, 
											List<TheResponsePrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		Question question 	= getQuestion( parentKey, context );
		
		// clear out the Responses 
		question.getResponses().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			TheResponse child = null;
			for( TheResponsePrimaryKey childKey : childKeys )
			{
				// retrieve the TheResponse
				child = TheResponseAWSLambdaDelegate.getTheResponse( childKey, context );

				// add it to the Responses List
				question.getResponses().add( child );
			}
		}
		
		// save the Question
		question = QuestionAWSLambdaDelegate.saveQuestion( question, context );

		return( question );
	}

    /**
     * Delete multiple TheResponse entities as the Responses to the relevant Question
     * @param		parentKey	QuestionPrimaryKey
     * @param		List<TheResponsePrimaryKey> childKeys
     * @return    	Question
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public Question deleteResponses( QuestionPrimaryKey parentKey, 
											List<TheResponsePrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		Question question 	= getQuestion( parentKey, context );

		if ( childKeys != null )
		{
			Set<TheResponse> children	= question.getResponses();
			TheResponse child 			= null;
			
			for( TheResponsePrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = TheResponseAWSLambdaDelegate.getTheResponse( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					TheResponseAWSLambdaDelegate.deleteTheResponse( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of TheResponse back to the question
			question.setResponses( children );			
			// save it 
			question = QuestionAWSLambdaDelegate.saveQuestion( question, context );
		}
		
		return ( question );
	}

 	
    /**
     * Returns the Question specific DAO.
     *
     * @return      Question DAO
     */
    public static QuestionDAO getQuestionDAO()
    {
        return( new com.occulue.dao.QuestionDAO() ); 
    }

    /**
     * Release the QuestionDAO back to the FrameworkDAOFactory
     */
    public static void releaseQuestionDAO( com.occulue.dao.QuestionDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(QuestionAWSLambdaDelegate.class.getName());
}

