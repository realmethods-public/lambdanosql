/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Comment AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Comment related services in the case of a Comment business related service failing.</li>
 * <li>Exposes a simpler, uniform Comment interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Comment business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Comment", description = "RESTful API to interact with Comment resources.")
@Path("/Comment")
public class CommentAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public CommentAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Comment
     * @param		businessObject 	Comment
	 * @param		context		Context	
     * @return     	Comment
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Comment", notes = "Creates Comment using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Comment createComment( 
    		@ApiParam(value = "Comment entity to create", required = true) Comment businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Comment provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        CommentDAO dao  = getCommentDAO();
        
        try
        {
            businessObject = dao.createComment( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "CommentAWSLambdaDelegate:createComment() - Unable to create Comment" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseCommentDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Comment via a supplied CommentPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Comment
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Comment", notes = "Gets the Comment associated with the provided primary key", response = Comment.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Comment getComment( 
    		@ApiParam(value = "Comment primary key", required = true) CommentPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Comment businessObject  	= null;                
        CommentDAO dao 			= getCommentDAO();
            
        try
        {
        	businessObject = dao.findComment( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Comment with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseCommentDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Comment
    * @param		businessObject		Comment
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Comment", notes = "Saves Comment using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Comment saveComment( 
    		@ApiParam(value = "Comment entity to save", required = true) Comment businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Comment provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        CommentPrimaryKey key = businessObject.getCommentPrimaryKey();
                    
        if ( key != null )
        {
            CommentDAO dao = getCommentDAO();

            try
            {                    
                businessObject = (Comment)dao.saveComment( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Comment" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseCommentDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Comment due to it having a null CommentPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Comments
     * @param		context		Context
     * @return 	ArrayList<Comment> 
     */
    @ApiOperation(value = "Get all Comment", notes = "Get all Comment from storage", responseContainer = "ArrayList", response = Comment.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Comment> getAllComment( Context context ) 
    	throws NotFoundException {

        ArrayList<Comment> array	= null;
        CommentDAO dao 			= getCommentDAO();
        
        try
        {
            array = dao.findAllComment();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllComment - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseCommentDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	CommentPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Comment", notes = "Deletes the Comment associated with the provided primary key", response = Comment.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteComment( 
			@ApiParam(value = "Comment primary key", required = true) CommentPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        CommentDAO dao  = getCommentDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteComment( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Comment using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseCommentDAO( dao );
        }
         		
        return;
     }

// role related methods

    /**
     * Gets the Source using the provided primary key of a Comment
     * @param		parentKey	CommentPrimaryKey
     * @return    	Referrer
     * @exception	NotFoundException
     */
	public static Referrer getSource( CommentPrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		Comment comment 	= getComment( parentKey, context );
		ReferrerPrimaryKey childKey = comment.getSource().getReferrerPrimaryKey(); 
		Referrer child 				= ReferrerAWSLambdaDelegate.getReferrer( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the Source on a Comment using the provided primary key of a Referrer
     * @param		parentKey	CommentPrimaryKey
     * @param		parentKey	CommentPrimaryKey
     * @param		context		Context
     * @return    	Comment
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static Comment saveSource( CommentPrimaryKey parentKey, 
												ReferrerPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		Comment comment 	= getComment( parentKey, context );
		Referrer child 				= ReferrerAWSLambdaDelegate.getReferrer( childKey, context );

		// assign the Source
		comment.setSource( child );
	
		// save the Comment 
		comment = CommentAWSLambdaDelegate.saveComment( comment, context );
		
		return( comment );
	}

    /**
     * Unassigns the Source on a Comment
     * @param		parentKey	CommentPrimaryKey
     * @param		Context		context
     * @return    	Comment
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static Comment deleteSource( CommentPrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		Comment comment 	= getComment( parentKey, context );
		
		if ( comment.getSource() != null )
		{
			ReferrerPrimaryKey pk = comment.getSource().getReferrerPrimaryKey();
			
			// first null out the Referrer on the parent so there's no constraint during deletion
			comment.setSource( null );
			CommentAWSLambdaDelegate.saveComment( comment, context );
			
			// now it is safe to delete the Source 
			ReferrerAWSLambdaDelegate.deleteReferrer( pk, context );
		}

		return( comment );
	}
		
 
 	
    /**
     * Returns the Comment specific DAO.
     *
     * @return      Comment DAO
     */
    public static CommentDAO getCommentDAO()
    {
        return( new com.occulue.dao.CommentDAO() ); 
    }

    /**
     * Release the CommentDAO back to the FrameworkDAOFactory
     */
    public static void releaseCommentDAO( com.occulue.dao.CommentDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(CommentAWSLambdaDelegate.class.getName());
}

