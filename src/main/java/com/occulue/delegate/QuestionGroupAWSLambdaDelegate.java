/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * QuestionGroup AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of QuestionGroup related services in the case of a QuestionGroup business related service failing.</li>
 * <li>Exposes a simpler, uniform QuestionGroup interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill QuestionGroup business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "QuestionGroup", description = "RESTful API to interact with QuestionGroup resources.")
@Path("/QuestionGroup")
public class QuestionGroupAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public QuestionGroupAWSLambdaDelegate() {
	}


    /**
     * Creates the provided QuestionGroup
     * @param		businessObject 	QuestionGroup
	 * @param		context		Context	
     * @return     	QuestionGroup
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a QuestionGroup", notes = "Creates QuestionGroup using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static QuestionGroup createQuestionGroup( 
    		@ApiParam(value = "QuestionGroup entity to create", required = true) QuestionGroup businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null QuestionGroup provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        QuestionGroupDAO dao  = getQuestionGroupDAO();
        
        try
        {
            businessObject = dao.createQuestionGroup( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "QuestionGroupAWSLambdaDelegate:createQuestionGroup() - Unable to create QuestionGroup" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the QuestionGroup via a supplied QuestionGroupPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	QuestionGroup
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a QuestionGroup", notes = "Gets the QuestionGroup associated with the provided primary key", response = QuestionGroup.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static QuestionGroup getQuestionGroup( 
    		@ApiParam(value = "QuestionGroup primary key", required = true) QuestionGroupPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        QuestionGroup businessObject  	= null;                
        QuestionGroupDAO dao 			= getQuestionGroupDAO();
            
        try
        {
        	businessObject = dao.findQuestionGroup( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate QuestionGroup with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided QuestionGroup
    * @param		businessObject		QuestionGroup
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a QuestionGroup", notes = "Saves QuestionGroup using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static QuestionGroup saveQuestionGroup( 
    		@ApiParam(value = "QuestionGroup entity to save", required = true) QuestionGroup businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null QuestionGroup provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        QuestionGroupPrimaryKey key = businessObject.getQuestionGroupPrimaryKey();
                    
        if ( key != null )
        {
            QuestionGroupDAO dao = getQuestionGroupDAO();

            try
            {                    
                businessObject = (QuestionGroup)dao.saveQuestionGroup( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save QuestionGroup" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseQuestionGroupDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create QuestionGroup due to it having a null QuestionGroupPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all QuestionGroups
     * @param		context		Context
     * @return 	ArrayList<QuestionGroup> 
     */
    @ApiOperation(value = "Get all QuestionGroup", notes = "Get all QuestionGroup from storage", responseContainer = "ArrayList", response = QuestionGroup.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<QuestionGroup> getAllQuestionGroup( Context context ) 
    	throws NotFoundException {

        ArrayList<QuestionGroup> array	= null;
        QuestionGroupDAO dao 			= getQuestionGroupDAO();
        
        try
        {
            array = dao.findAllQuestionGroup();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllQuestionGroup - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseQuestionGroupDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	QuestionGroupPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a QuestionGroup", notes = "Deletes the QuestionGroup associated with the provided primary key", response = QuestionGroup.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteQuestionGroup( 
			@ApiParam(value = "QuestionGroup primary key", required = true) QuestionGroupPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        QuestionGroupDAO dao  = getQuestionGroupDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteQuestionGroup( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete QuestionGroup using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseQuestionGroupDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the Questions on a QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		context		Context
     * @return    	Set<Question>
     * @exception	NotFoundException
     */
	public static Set<Question> getQuestions( QuestionGroupPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );
    	return (questionGroup.getQuestions());
    }
    
    /**
     * Add the assigned Question into the Questions of the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		childKey	QuestionPrimaryKey
	 * @param		context		Context
     * @return    	QuestionGroup
     * @exception	NotFoundException
     */
	public static QuestionGroup addQuestions( 	QuestionGroupPrimaryKey parentKey, 
									QuestionPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );

		// find the Question
		Question child = QuestionAWSLambdaDelegate.getQuestion( childKey, context );
		
		// add it to the Questions 
		questionGroup.getQuestions().add( child );				
		
		// save the QuestionGroup
		questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );

		return ( questionGroup );
	}

    /**
     * Saves multiple Question entities as the Questions to the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		List<QuestionPrimaryKey> childKeys
     * @return    	QuestionGroup
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public QuestionGroup assignQuestions( QuestionGroupPrimaryKey parentKey, 
											List<QuestionPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );
		
		// clear out the Questions 
		questionGroup.getQuestions().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			Question child = null;
			for( QuestionPrimaryKey childKey : childKeys )
			{
				// retrieve the Question
				child = QuestionAWSLambdaDelegate.getQuestion( childKey, context );

				// add it to the Questions List
				questionGroup.getQuestions().add( child );
			}
		}
		
		// save the QuestionGroup
		questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );

		return( questionGroup );
	}

    /**
     * Delete multiple Question entities as the Questions to the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		List<QuestionPrimaryKey> childKeys
     * @return    	QuestionGroup
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public QuestionGroup deleteQuestions( QuestionGroupPrimaryKey parentKey, 
											List<QuestionPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );

		if ( childKeys != null )
		{
			Set<Question> children	= questionGroup.getQuestions();
			Question child 			= null;
			
			for( QuestionPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = QuestionAWSLambdaDelegate.getQuestion( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					QuestionAWSLambdaDelegate.deleteQuestion( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of Question back to the questionGroup
			questionGroup.setQuestions( children );			
			// save it 
			questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );
		}
		
		return ( questionGroup );
	}

    /**
     * Retrieves the QuestionGroups on a QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		context		Context
     * @return    	Set<QuestionGroup>
     * @exception	NotFoundException
     */
	public static Set<QuestionGroup> getQuestionGroups( QuestionGroupPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );
    	return (questionGroup.getQuestionGroups());
    }
    
    /**
     * Add the assigned QuestionGroup into the QuestionGroups of the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		childKey	QuestionGroupPrimaryKey
	 * @param		context		Context
     * @return    	QuestionGroup
     * @exception	NotFoundException
     */
	public static QuestionGroup addQuestionGroups( 	QuestionGroupPrimaryKey parentKey, 
									QuestionGroupPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );

		// find the QuestionGroup
		QuestionGroup child = QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );
		
		// add it to the QuestionGroups 
		questionGroup.getQuestionGroups().add( child );				
		
		// save the QuestionGroup
		questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );

		return ( questionGroup );
	}

    /**
     * Saves multiple QuestionGroup entities as the QuestionGroups to the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		List<QuestionGroupPrimaryKey> childKeys
     * @return    	QuestionGroup
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public QuestionGroup assignQuestionGroups( QuestionGroupPrimaryKey parentKey, 
											List<QuestionGroupPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );
		
		// clear out the QuestionGroups 
		questionGroup.getQuestionGroups().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			QuestionGroup child = null;
			for( QuestionGroupPrimaryKey childKey : childKeys )
			{
				// retrieve the QuestionGroup
				child = QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );

				// add it to the QuestionGroups List
				questionGroup.getQuestionGroups().add( child );
			}
		}
		
		// save the QuestionGroup
		questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );

		return( questionGroup );
	}

    /**
     * Delete multiple QuestionGroup entities as the QuestionGroups to the relevant QuestionGroup
     * @param		parentKey	QuestionGroupPrimaryKey
     * @param		List<QuestionGroupPrimaryKey> childKeys
     * @return    	QuestionGroup
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public QuestionGroup deleteQuestionGroups( QuestionGroupPrimaryKey parentKey, 
											List<QuestionGroupPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		QuestionGroup questionGroup 	= getQuestionGroup( parentKey, context );

		if ( childKeys != null )
		{
			Set<QuestionGroup> children	= questionGroup.getQuestionGroups();
			QuestionGroup child 			= null;
			
			for( QuestionGroupPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					QuestionGroupAWSLambdaDelegate.deleteQuestionGroup( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of QuestionGroup back to the questionGroup
			questionGroup.setQuestionGroups( children );			
			// save it 
			questionGroup = QuestionGroupAWSLambdaDelegate.saveQuestionGroup( questionGroup, context );
		}
		
		return ( questionGroup );
	}

 	
    /**
     * Returns the QuestionGroup specific DAO.
     *
     * @return      QuestionGroup DAO
     */
    public static QuestionGroupDAO getQuestionGroupDAO()
    {
        return( new com.occulue.dao.QuestionGroupDAO() ); 
    }

    /**
     * Release the QuestionGroupDAO back to the FrameworkDAOFactory
     */
    public static void releaseQuestionGroupDAO( com.occulue.dao.QuestionGroupDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(QuestionGroupAWSLambdaDelegate.class.getName());
}

