/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * Referrer AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Referrer related services in the case of a Referrer business related service failing.</li>
 * <li>Exposes a simpler, uniform Referrer interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Referrer business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "Referrer", description = "RESTful API to interact with Referrer resources.")
@Path("/Referrer")
public class ReferrerAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferrerAWSLambdaDelegate() {
	}


    /**
     * Creates the provided Referrer
     * @param		businessObject 	Referrer
	 * @param		context		Context	
     * @return     	Referrer
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a Referrer", notes = "Creates Referrer using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Referrer createReferrer( 
    		@ApiParam(value = "Referrer entity to create", required = true) Referrer businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null Referrer provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        ReferrerDAO dao  = getReferrerDAO();
        
        try
        {
            businessObject = dao.createReferrer( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "ReferrerAWSLambdaDelegate:createReferrer() - Unable to create Referrer" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the Referrer via a supplied ReferrerPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	Referrer
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a Referrer", notes = "Gets the Referrer associated with the provided primary key", response = Referrer.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static Referrer getReferrer( 
    		@ApiParam(value = "Referrer primary key", required = true) ReferrerPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        Referrer businessObject  	= null;                
        ReferrerDAO dao 			= getReferrerDAO();
            
        try
        {
        	businessObject = dao.findReferrer( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate Referrer with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided Referrer
    * @param		businessObject		Referrer
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a Referrer", notes = "Saves Referrer using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static Referrer saveReferrer( 
    		@ApiParam(value = "Referrer entity to save", required = true) Referrer businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null Referrer provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferrerPrimaryKey key = businessObject.getReferrerPrimaryKey();
                    
        if ( key != null )
        {
            ReferrerDAO dao = getReferrerDAO();

            try
            {                    
                businessObject = (Referrer)dao.saveReferrer( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save Referrer" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseReferrerDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create Referrer due to it having a null ReferrerPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all Referrers
     * @param		context		Context
     * @return 	ArrayList<Referrer> 
     */
    @ApiOperation(value = "Get all Referrer", notes = "Get all Referrer from storage", responseContainer = "ArrayList", response = Referrer.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<Referrer> getAllReferrer( Context context ) 
    	throws NotFoundException {

        ArrayList<Referrer> array	= null;
        ReferrerDAO dao 			= getReferrerDAO();
        
        try
        {
            array = dao.findAllReferrer();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllReferrer - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseReferrerDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	ReferrerPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a Referrer", notes = "Deletes the Referrer associated with the provided primary key", response = Referrer.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteReferrer( 
			@ApiParam(value = "Referrer primary key", required = true) ReferrerPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        ReferrerDAO dao  = getReferrerDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteReferrer( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete Referrer using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseReferrerDAO( dao );
        }
         		
        return;
     }

// role related methods

 
    /**
     * Retrieves the Comments on a Referrer
     * @param		parentKey	ReferrerPrimaryKey
     * @param		context		Context
     * @return    	Set<Comment>
     * @exception	NotFoundException
     */
	public static Set<Comment> getComments( ReferrerPrimaryKey parentKey, Context context )
		throws NotFoundException
    {
    	Referrer referrer 	= getReferrer( parentKey, context );
    	return (referrer.getComments());
    }
    
    /**
     * Add the assigned Comment into the Comments of the relevant Referrer
     * @param		parentKey	ReferrerPrimaryKey
     * @param		childKey	CommentPrimaryKey
	 * @param		context		Context
     * @return    	Referrer
     * @exception	NotFoundException
     */
	public static Referrer addComments( 	ReferrerPrimaryKey parentKey, 
									CommentPrimaryKey childKey, 
									Context context )
	throws SaveException, NotFoundException
	{
		Referrer referrer 	= getReferrer( parentKey, context );

		// find the Comment
		Comment child = CommentAWSLambdaDelegate.getComment( childKey, context );
		
		// add it to the Comments 
		referrer.getComments().add( child );				
		
		// save the Referrer
		referrer = ReferrerAWSLambdaDelegate.saveReferrer( referrer, context );

		return ( referrer );
	}

    /**
     * Saves multiple Comment entities as the Comments to the relevant Referrer
     * @param		parentKey	ReferrerPrimaryKey
     * @param		List<CommentPrimaryKey> childKeys
     * @return    	Referrer
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public Referrer assignComments( ReferrerPrimaryKey parentKey, 
											List<CommentPrimaryKey> childKeys, 
											Context context )
		throws SaveException, NotFoundException {

		Referrer referrer 	= getReferrer( parentKey, context );
		
		// clear out the Comments 
		referrer.getComments().clear();
		
		// finally, find each child and add
		if ( childKeys != null )
		{
			Comment child = null;
			for( CommentPrimaryKey childKey : childKeys )
			{
				// retrieve the Comment
				child = CommentAWSLambdaDelegate.getComment( childKey, context );

				// add it to the Comments List
				referrer.getComments().add( child );
			}
		}
		
		// save the Referrer
		referrer = ReferrerAWSLambdaDelegate.saveReferrer( referrer, context );

		return( referrer );
	}

    /**
     * Delete multiple Comment entities as the Comments to the relevant Referrer
     * @param		parentKey	ReferrerPrimaryKey
     * @param		List<CommentPrimaryKey> childKeys
     * @return    	Referrer
     * @exception	DeletionException
     * @exception	NotFoundException
     * @exception	SaveException
     */
	public Referrer deleteComments( ReferrerPrimaryKey parentKey, 
											List<CommentPrimaryKey> childKeys, 
											Context context )
		throws DeletionException, NotFoundException, SaveException {		
		Referrer referrer 	= getReferrer( parentKey, context );

		if ( childKeys != null )
		{
			Set<Comment> children	= referrer.getComments();
			Comment child 			= null;
			
			for( CommentPrimaryKey childKey : childKeys )
			{
				try
				{
					// first remove the relevant child from the list
					child = CommentAWSLambdaDelegate.getComment( childKey, context );
					children.remove( child );
					
					// then safe to delete the child				
					CommentAWSLambdaDelegate.deleteComment( childKey, context );
				}
				catch( Exception exc )
				{
					String errMsg = "Deletion failed - " + exc.getMessage();
					context.getLogger().log( errMsg );
					throw new DeletionException( errMsg );
				}
			}
			
			// assign the modified list of Comment back to the referrer
			referrer.setComments( children );			
			// save it 
			referrer = ReferrerAWSLambdaDelegate.saveReferrer( referrer, context );
		}
		
		return ( referrer );
	}

 	
    /**
     * Returns the Referrer specific DAO.
     *
     * @return      Referrer DAO
     */
    public static ReferrerDAO getReferrerDAO()
    {
        return( new com.occulue.dao.ReferrerDAO() ); 
    }

    /**
     * Release the ReferrerDAO back to the FrameworkDAOFactory
     */
    public static void releaseReferrerDAO( com.occulue.dao.ReferrerDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(ReferrerAWSLambdaDelegate.class.getName());
}

