/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.occulue.primarykey.*;
    import com.occulue.dao.*;
    import com.occulue.bo.*;
    
import com.occulue.exception.CreationException;
import com.occulue.exception.DeletionException;
import com.occulue.exception.NotFoundException;
import com.occulue.exception.SaveException;

/**
 * ReferenceEngine AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ReferenceEngine related services in the case of a ReferenceEngine business related service failing.</li>
 * <li>Exposes a simpler, uniform ReferenceEngine interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ReferenceEngine business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
@Api(value = "ReferenceEngine", description = "RESTful API to interact with ReferenceEngine resources.")
@Path("/ReferenceEngine")
public class ReferenceEngineAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ReferenceEngineAWSLambdaDelegate() {
	}


    /**
     * Creates the provided ReferenceEngine
     * @param		businessObject 	ReferenceEngine
	 * @param		context		Context	
     * @return     	ReferenceEngine
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a ReferenceEngine", notes = "Creates ReferenceEngine using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceEngine createReferenceEngine( 
    		@ApiParam(value = "ReferenceEngine entity to create", required = true) ReferenceEngine businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null ReferenceEngine provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        ReferenceEngineDAO dao  = getReferenceEngineDAO();
        
        try
        {
            businessObject = dao.createReferenceEngine( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "ReferenceEngineAWSLambdaDelegate:createReferenceEngine() - Unable to create ReferenceEngine" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseReferenceEngineDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the ReferenceEngine via a supplied ReferenceEnginePrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	ReferenceEngine
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a ReferenceEngine", notes = "Gets the ReferenceEngine associated with the provided primary key", response = ReferenceEngine.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ReferenceEngine getReferenceEngine( 
    		@ApiParam(value = "ReferenceEngine primary key", required = true) ReferenceEnginePrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        ReferenceEngine businessObject  	= null;                
        ReferenceEngineDAO dao 			= getReferenceEngineDAO();
            
        try
        {
        	businessObject = dao.findReferenceEngine( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate ReferenceEngine with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseReferenceEngineDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided ReferenceEngine
    * @param		businessObject		ReferenceEngine
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a ReferenceEngine", notes = "Saves ReferenceEngine using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ReferenceEngine saveReferenceEngine( 
    		@ApiParam(value = "ReferenceEngine entity to save", required = true) ReferenceEngine businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null ReferenceEngine provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ReferenceEnginePrimaryKey key = businessObject.getReferenceEnginePrimaryKey();
                    
        if ( key != null )
        {
            ReferenceEngineDAO dao = getReferenceEngineDAO();

            try
            {                    
                businessObject = (ReferenceEngine)dao.saveReferenceEngine( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save ReferenceEngine" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseReferenceEngineDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create ReferenceEngine due to it having a null ReferenceEnginePrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all ReferenceEngines
     * @param		context		Context
     * @return 	ArrayList<ReferenceEngine> 
     */
    @ApiOperation(value = "Get all ReferenceEngine", notes = "Get all ReferenceEngine from storage", responseContainer = "ArrayList", response = ReferenceEngine.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<ReferenceEngine> getAllReferenceEngine( Context context ) 
    	throws NotFoundException {

        ArrayList<ReferenceEngine> array	= null;
        ReferenceEngineDAO dao 			= getReferenceEngineDAO();
        
        try
        {
            array = dao.findAllReferenceEngine();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllReferenceEngine - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseReferenceEngineDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	ReferenceEnginePrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a ReferenceEngine", notes = "Deletes the ReferenceEngine associated with the provided primary key", response = ReferenceEngine.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteReferenceEngine( 
			@ApiParam(value = "ReferenceEngine primary key", required = true) ReferenceEnginePrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        ReferenceEngineDAO dao  = getReferenceEngineDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteReferenceEngine( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete ReferenceEngine using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseReferenceEngineDAO( dao );
        }
         		
        return;
     }

// role related methods

    /**
     * Gets the MainQuestionGroup using the provided primary key of a ReferenceEngine
     * @param		parentKey	ReferenceEnginePrimaryKey
     * @return    	QuestionGroup
     * @exception	NotFoundException
     */
	public static QuestionGroup getMainQuestionGroup( ReferenceEnginePrimaryKey parentKey, Context context )
		throws NotFoundException {
		
		ReferenceEngine referenceEngine 	= getReferenceEngine( parentKey, context );
		QuestionGroupPrimaryKey childKey = referenceEngine.getMainQuestionGroup().getQuestionGroupPrimaryKey(); 
		QuestionGroup child 				= QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );
		
		return( child );
	}

    /**
     * Assigns the MainQuestionGroup on a ReferenceEngine using the provided primary key of a QuestionGroup
     * @param		parentKey	ReferenceEnginePrimaryKey
     * @param		parentKey	ReferenceEnginePrimaryKey
     * @param		context		Context
     * @return    	ReferenceEngine
     * @exception	SaveException
     * @exception	NotFoundException
     */
	public static ReferenceEngine saveMainQuestionGroup( ReferenceEnginePrimaryKey parentKey, 
												QuestionGroupPrimaryKey childKey,
												Context context)
		throws SaveException, NotFoundException {
		
		ReferenceEngine referenceEngine 	= getReferenceEngine( parentKey, context );
		QuestionGroup child 				= QuestionGroupAWSLambdaDelegate.getQuestionGroup( childKey, context );

		// assign the MainQuestionGroup
		referenceEngine.setMainQuestionGroup( child );
	
		// save the ReferenceEngine 
		referenceEngine = ReferenceEngineAWSLambdaDelegate.saveReferenceEngine( referenceEngine, context );
		
		return( referenceEngine );
	}

    /**
     * Unassigns the MainQuestionGroup on a ReferenceEngine
     * @param		parentKey	ReferenceEnginePrimaryKey
     * @param		Context		context
     * @return    	ReferenceEngine
     * @exception	SaveException
     * @exception	NotFoundException
	 * @exception	SaveException	
     */
	public static ReferenceEngine deleteMainQuestionGroup( ReferenceEnginePrimaryKey parentKey, Context context )
	throws DeletionException, NotFoundException, SaveException {

		ReferenceEngine referenceEngine 	= getReferenceEngine( parentKey, context );
		
		if ( referenceEngine.getMainQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = referenceEngine.getMainQuestionGroup().getQuestionGroupPrimaryKey();
			
			// first null out the QuestionGroup on the parent so there's no constraint during deletion
			referenceEngine.setMainQuestionGroup( null );
			ReferenceEngineAWSLambdaDelegate.saveReferenceEngine( referenceEngine, context );
			
			// now it is safe to delete the MainQuestionGroup 
			QuestionGroupAWSLambdaDelegate.deleteQuestionGroup( pk, context );
		}

		return( referenceEngine );
	}
		
 
 	
    /**
     * Returns the ReferenceEngine specific DAO.
     *
     * @return      ReferenceEngine DAO
     */
    public static ReferenceEngineDAO getReferenceEngineDAO()
    {
        return( new com.occulue.dao.ReferenceEngineDAO() ); 
    }

    /**
     * Release the ReferenceEngineDAO back to the FrameworkDAOFactory
     */
    public static void releaseReferenceEngineDAO( com.occulue.dao.ReferenceEngineDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(ReferenceEngineAWSLambdaDelegate.class.getName());
}

