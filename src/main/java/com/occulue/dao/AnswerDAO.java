/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Answer
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class AnswerDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public AnswerDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Answer from the persistent store, using the provided primary key. 
     * If no match is found, a null Answer is returned.
     * <p>
     * @param       pk
     * @return      Answer
     * @exception   ProcessingException
     */
    public Answer findAnswer( AnswerPrimaryKey pk ) 
    throws ProcessingException
    {
    	Answer businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("AnswerDAO.findAnswer cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Answer.class )
        	       						.field( "answerId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AnswerDAO.findAnswer failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Answer into the persistent store.
     * @param       businessObject
     * @return      newly persisted Answer
     * @exception   ProcessingException
     */
    public Answer createAnswer( Answer businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "answerId" ) ;
    		businessObject.setAnswerId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- AnswerDAO.createAnswer created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AnswerDAO.createAnswer - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Answer to the persistent store.
     *
     * @param       businessObject
     * @return      Answer	stored entity
     * @exception   ProcessingException 
     */
    public Answer saveAnswer( Answer businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AnswerDAO:saveAnswer - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Answer from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteAnswer( AnswerPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findAnswer( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("AnswerDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "AnswerDAO.deleteAnswer failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Answers
     * @return		ArrayList<Answer>
     * @exception   ProcessingException
     */
    public ArrayList<Answer> findAllAnswer()
    throws ProcessingException
    {
		final ArrayList<Answer> list 	= new ArrayList<Answer>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Answer.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "AnswerDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "AnswerDAO.findAllAnswer failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "AnswerDAO:findAllAnswers() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.Answer" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "answerId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Answer.class.getName());
}
