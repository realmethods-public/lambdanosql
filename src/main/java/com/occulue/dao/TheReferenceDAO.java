/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity TheReference
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class TheReferenceDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public TheReferenceDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a TheReference from the persistent store, using the provided primary key. 
     * If no match is found, a null TheReference is returned.
     * <p>
     * @param       pk
     * @return      TheReference
     * @exception   ProcessingException
     */
    public TheReference findTheReference( TheReferencePrimaryKey pk ) 
    throws ProcessingException
    {
    	TheReference businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("TheReferenceDAO.findTheReference cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( TheReference.class )
        	       						.field( "theReferenceId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheReferenceDAO.findTheReference failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new TheReference into the persistent store.
     * @param       businessObject
     * @return      newly persisted TheReference
     * @exception   ProcessingException
     */
    public TheReference createTheReference( TheReference businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "theReferenceId" ) ;
    		businessObject.setTheReferenceId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- TheReferenceDAO.createTheReference created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheReferenceDAO.createTheReference - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided TheReference to the persistent store.
     *
     * @param       businessObject
     * @return      TheReference	stored entity
     * @exception   ProcessingException 
     */
    public TheReference saveTheReference( TheReference businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheReferenceDAO:saveTheReference - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a TheReference from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteTheReference( TheReferencePrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findTheReference( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("TheReferenceDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "TheReferenceDAO.deleteTheReference failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all TheReferences
     * @return		ArrayList<TheReference>
     * @exception   ProcessingException
     */
    public ArrayList<TheReference> findAllTheReference()
    throws ProcessingException
    {
		final ArrayList<TheReference> list 	= new ArrayList<TheReference>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( TheReference.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "TheReferenceDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "TheReferenceDAO.findAllTheReference failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "TheReferenceDAO:findAllTheReferences() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.TheReference" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "theReferenceId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(TheReference.class.getName());
}
