/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity User
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
public class UserDAO extends ReferrerDAO
{
    /**
     * default constructor
     */
    public UserDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a User from the persistent store, using the provided primary key. 
     * If no match is found, a null User is returned.
     * <p>
     * @param       pk
     * @return      User
     * @exception   ProcessingException
     */
    public User findUser( UserPrimaryKey pk ) 
    throws ProcessingException
    {
    	User businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("UserDAO.findUser cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( User.class )
        	       						.field( "referrerId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "UserDAO.findUser failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new User into the persistent store.
     * @param       businessObject
     * @return      newly persisted User
     * @exception   ProcessingException
     */
    public User createUser( User businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referrerId" ) ;
    		businessObject.setUserId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- UserDAO.createUser created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "UserDAO.createUser - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided User to the persistent store.
     *
     * @param       businessObject
     * @return      User	stored entity
     * @exception   ProcessingException 
     */
    public User saveUser( User businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "UserDAO:saveUser - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a User from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteUser( UserPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findUser( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("UserDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "UserDAO.deleteUser failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Users
     * @return		ArrayList<User>
     * @exception   ProcessingException
     */
    public ArrayList<User> findAllUser()
    throws ProcessingException
    {
		final ArrayList<User> list 	= new ArrayList<User>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( User.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "UserDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "UserDAO.findAllUser failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "UserDAO:findAllUsers() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.User" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referrerId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(User.class.getName());
}
