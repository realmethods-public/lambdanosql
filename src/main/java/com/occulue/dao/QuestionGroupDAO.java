/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity QuestionGroup
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class QuestionGroupDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public QuestionGroupDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a QuestionGroup from the persistent store, using the provided primary key. 
     * If no match is found, a null QuestionGroup is returned.
     * <p>
     * @param       pk
     * @return      QuestionGroup
     * @exception   ProcessingException
     */
    public QuestionGroup findQuestionGroup( QuestionGroupPrimaryKey pk ) 
    throws ProcessingException
    {
    	QuestionGroup businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("QuestionGroupDAO.findQuestionGroup cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( QuestionGroup.class )
        	       						.field( "questionGroupId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionGroupDAO.findQuestionGroup failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new QuestionGroup into the persistent store.
     * @param       businessObject
     * @return      newly persisted QuestionGroup
     * @exception   ProcessingException
     */
    public QuestionGroup createQuestionGroup( QuestionGroup businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "questionGroupId" ) ;
    		businessObject.setQuestionGroupId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- QuestionGroupDAO.createQuestionGroup created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionGroupDAO.createQuestionGroup - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided QuestionGroup to the persistent store.
     *
     * @param       businessObject
     * @return      QuestionGroup	stored entity
     * @exception   ProcessingException 
     */
    public QuestionGroup saveQuestionGroup( QuestionGroup businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionGroupDAO:saveQuestionGroup - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a QuestionGroup from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteQuestionGroup( QuestionGroupPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findQuestionGroup( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("QuestionGroupDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "QuestionGroupDAO.deleteQuestionGroup failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all QuestionGroups
     * @return		ArrayList<QuestionGroup>
     * @exception   ProcessingException
     */
    public ArrayList<QuestionGroup> findAllQuestionGroup()
    throws ProcessingException
    {
		final ArrayList<QuestionGroup> list 	= new ArrayList<QuestionGroup>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( QuestionGroup.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "QuestionGroupDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "QuestionGroupDAO.findAllQuestionGroup failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "QuestionGroupDAO:findAllQuestionGroups() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.QuestionGroup" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "questionGroupId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(QuestionGroup.class.getName());
}
