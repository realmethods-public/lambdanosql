/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity ReferrerGroup
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class ReferrerGroupDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ReferrerGroupDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a ReferrerGroup from the persistent store, using the provided primary key. 
     * If no match is found, a null ReferrerGroup is returned.
     * <p>
     * @param       pk
     * @return      ReferrerGroup
     * @exception   ProcessingException
     */
    public ReferrerGroup findReferrerGroup( ReferrerGroupPrimaryKey pk ) 
    throws ProcessingException
    {
    	ReferrerGroup businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ReferrerGroupDAO.findReferrerGroup cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( ReferrerGroup.class )
        	       						.field( "referrerGroupId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerGroupDAO.findReferrerGroup failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new ReferrerGroup into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferrerGroup
     * @exception   ProcessingException
     */
    public ReferrerGroup createReferrerGroup( ReferrerGroup businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referrerGroupId" ) ;
    		businessObject.setReferrerGroupId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ReferrerGroupDAO.createReferrerGroup created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerGroupDAO.createReferrerGroup - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided ReferrerGroup to the persistent store.
     *
     * @param       businessObject
     * @return      ReferrerGroup	stored entity
     * @exception   ProcessingException 
     */
    public ReferrerGroup saveReferrerGroup( ReferrerGroup businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerGroupDAO:saveReferrerGroup - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a ReferrerGroup from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteReferrerGroup( ReferrerGroupPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findReferrerGroup( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ReferrerGroupDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ReferrerGroupDAO.deleteReferrerGroup failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all ReferrerGroups
     * @return		ArrayList<ReferrerGroup>
     * @exception   ProcessingException
     */
    public ArrayList<ReferrerGroup> findAllReferrerGroup()
    throws ProcessingException
    {
		final ArrayList<ReferrerGroup> list 	= new ArrayList<ReferrerGroup>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( ReferrerGroup.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ReferrerGroupDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ReferrerGroupDAO.findAllReferrerGroup failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ReferrerGroupDAO:findAllReferrerGroups() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.ReferrerGroup" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referrerGroupId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(ReferrerGroup.class.getName());
}
