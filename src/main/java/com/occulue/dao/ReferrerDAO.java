/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Referrer
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class ReferrerDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ReferrerDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Referrer from the persistent store, using the provided primary key. 
     * If no match is found, a null Referrer is returned.
     * <p>
     * @param       pk
     * @return      Referrer
     * @exception   ProcessingException
     */
    public Referrer findReferrer( ReferrerPrimaryKey pk ) 
    throws ProcessingException
    {
    	Referrer businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ReferrerDAO.findReferrer cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Referrer.class )
        	       						.field( "referrerId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerDAO.findReferrer failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Referrer into the persistent store.
     * @param       businessObject
     * @return      newly persisted Referrer
     * @exception   ProcessingException
     */
    public Referrer createReferrer( Referrer businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referrerId" ) ;
    		businessObject.setReferrerId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ReferrerDAO.createReferrer created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerDAO.createReferrer - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Referrer to the persistent store.
     *
     * @param       businessObject
     * @return      Referrer	stored entity
     * @exception   ProcessingException 
     */
    public Referrer saveReferrer( Referrer businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferrerDAO:saveReferrer - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Referrer from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteReferrer( ReferrerPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findReferrer( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ReferrerDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ReferrerDAO.deleteReferrer failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Referrers
     * @return		ArrayList<Referrer>
     * @exception   ProcessingException
     */
    public ArrayList<Referrer> findAllReferrer()
    throws ProcessingException
    {
		final ArrayList<Referrer> list 	= new ArrayList<Referrer>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Referrer.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ReferrerDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ReferrerDAO.findAllReferrer failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ReferrerDAO:findAllReferrers() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.Referrer" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referrerId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Referrer.class.getName());
}
