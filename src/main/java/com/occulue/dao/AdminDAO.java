/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Admin
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class AdminDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public AdminDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Admin from the persistent store, using the provided primary key. 
     * If no match is found, a null Admin is returned.
     * <p>
     * @param       pk
     * @return      Admin
     * @exception   ProcessingException
     */
    public Admin findAdmin( AdminPrimaryKey pk ) 
    throws ProcessingException
    {
    	Admin businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("AdminDAO.findAdmin cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Admin.class )
        	       						.field( "adminId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AdminDAO.findAdmin failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Admin into the persistent store.
     * @param       businessObject
     * @return      newly persisted Admin
     * @exception   ProcessingException
     */
    public Admin createAdmin( Admin businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "adminId" ) ;
    		businessObject.setAdminId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- AdminDAO.createAdmin created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AdminDAO.createAdmin - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Admin to the persistent store.
     *
     * @param       businessObject
     * @return      Admin	stored entity
     * @exception   ProcessingException 
     */
    public Admin saveAdmin( Admin businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "AdminDAO:saveAdmin - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Admin from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteAdmin( AdminPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findAdmin( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("AdminDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "AdminDAO.deleteAdmin failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Admins
     * @return		ArrayList<Admin>
     * @exception   ProcessingException
     */
    public ArrayList<Admin> findAllAdmin()
    throws ProcessingException
    {
		final ArrayList<Admin> list 	= new ArrayList<Admin>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Admin.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "AdminDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "AdminDAO.findAllAdmin failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "AdminDAO:findAllAdmins() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.Admin" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "adminId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Admin.class.getName());
}
