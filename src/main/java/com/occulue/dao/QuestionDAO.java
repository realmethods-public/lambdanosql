/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Question
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class QuestionDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public QuestionDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Question from the persistent store, using the provided primary key. 
     * If no match is found, a null Question is returned.
     * <p>
     * @param       pk
     * @return      Question
     * @exception   ProcessingException
     */
    public Question findQuestion( QuestionPrimaryKey pk ) 
    throws ProcessingException
    {
    	Question businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("QuestionDAO.findQuestion cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Question.class )
        	       						.field( "questionId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionDAO.findQuestion failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Question into the persistent store.
     * @param       businessObject
     * @return      newly persisted Question
     * @exception   ProcessingException
     */
    public Question createQuestion( Question businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "questionId" ) ;
    		businessObject.setQuestionId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- QuestionDAO.createQuestion created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionDAO.createQuestion - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Question to the persistent store.
     *
     * @param       businessObject
     * @return      Question	stored entity
     * @exception   ProcessingException 
     */
    public Question saveQuestion( Question businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "QuestionDAO:saveQuestion - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Question from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteQuestion( QuestionPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findQuestion( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("QuestionDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "QuestionDAO.deleteQuestion failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Questions
     * @return		ArrayList<Question>
     * @exception   ProcessingException
     */
    public ArrayList<Question> findAllQuestion()
    throws ProcessingException
    {
		final ArrayList<Question> list 	= new ArrayList<Question>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Question.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "QuestionDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "QuestionDAO.findAllQuestion failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "QuestionDAO:findAllQuestions() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.Question" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "questionId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Question.class.getName());
}
