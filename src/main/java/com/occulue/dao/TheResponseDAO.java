/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity TheResponse
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class TheResponseDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public TheResponseDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a TheResponse from the persistent store, using the provided primary key. 
     * If no match is found, a null TheResponse is returned.
     * <p>
     * @param       pk
     * @return      TheResponse
     * @exception   ProcessingException
     */
    public TheResponse findTheResponse( TheResponsePrimaryKey pk ) 
    throws ProcessingException
    {
    	TheResponse businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("TheResponseDAO.findTheResponse cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( TheResponse.class )
        	       						.field( "theResponseId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheResponseDAO.findTheResponse failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new TheResponse into the persistent store.
     * @param       businessObject
     * @return      newly persisted TheResponse
     * @exception   ProcessingException
     */
    public TheResponse createTheResponse( TheResponse businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "theResponseId" ) ;
    		businessObject.setTheResponseId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- TheResponseDAO.createTheResponse created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheResponseDAO.createTheResponse - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided TheResponse to the persistent store.
     *
     * @param       businessObject
     * @return      TheResponse	stored entity
     * @exception   ProcessingException 
     */
    public TheResponse saveTheResponse( TheResponse businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "TheResponseDAO:saveTheResponse - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a TheResponse from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteTheResponse( TheResponsePrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findTheResponse( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("TheResponseDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "TheResponseDAO.deleteTheResponse failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all TheResponses
     * @return		ArrayList<TheResponse>
     * @exception   ProcessingException
     */
    public ArrayList<TheResponse> findAllTheResponse()
    throws ProcessingException
    {
		final ArrayList<TheResponse> list 	= new ArrayList<TheResponse>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( TheResponse.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "TheResponseDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "TheResponseDAO.findAllTheResponse failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "TheResponseDAO:findAllTheResponses() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.TheResponse" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "theResponseId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(TheResponse.class.getName());
}
