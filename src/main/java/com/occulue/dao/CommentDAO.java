/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity Comment
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class CommentDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public CommentDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a Comment from the persistent store, using the provided primary key. 
     * If no match is found, a null Comment is returned.
     * <p>
     * @param       pk
     * @return      Comment
     * @exception   ProcessingException
     */
    public Comment findComment( CommentPrimaryKey pk ) 
    throws ProcessingException
    {
    	Comment businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("CommentDAO.findComment cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( Comment.class )
        	       						.field( "commentId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "CommentDAO.findComment failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new Comment into the persistent store.
     * @param       businessObject
     * @return      newly persisted Comment
     * @exception   ProcessingException
     */
    public Comment createComment( Comment businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "commentId" ) ;
    		businessObject.setCommentId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- CommentDAO.createComment created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "CommentDAO.createComment - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided Comment to the persistent store.
     *
     * @param       businessObject
     * @return      Comment	stored entity
     * @exception   ProcessingException 
     */
    public Comment saveComment( Comment businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "CommentDAO:saveComment - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a Comment from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteComment( CommentPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findComment( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("CommentDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "CommentDAO.deleteComment failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all Comments
     * @return		ArrayList<Comment>
     * @exception   ProcessingException
     */
    public ArrayList<Comment> findAllComment()
    throws ProcessingException
    {
		final ArrayList<Comment> list 	= new ArrayList<Comment>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( Comment.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "CommentDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "CommentDAO.findAllComment failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "CommentDAO:findAllComments() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.Comment" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "commentId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(Comment.class.getName());
}
