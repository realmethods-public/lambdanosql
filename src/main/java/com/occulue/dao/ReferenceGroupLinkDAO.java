/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.occulue.exception.*;

    import com.occulue.primarykey.*;
    import com.occulue.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity ReferenceGroupLink
 * using the Morphia Java Datastore.
 *
 * @author dev@realmethods.com
 */
 public class ReferenceGroupLinkDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ReferenceGroupLinkDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a ReferenceGroupLink from the persistent store, using the provided primary key. 
     * If no match is found, a null ReferenceGroupLink is returned.
     * <p>
     * @param       pk
     * @return      ReferenceGroupLink
     * @exception   ProcessingException
     */
    public ReferenceGroupLink findReferenceGroupLink( ReferenceGroupLinkPrimaryKey pk ) 
    throws ProcessingException
    {
    	ReferenceGroupLink businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ReferenceGroupLinkDAO.findReferenceGroupLink cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( ReferenceGroupLink.class )
        	       						.field( "referenceGroupLinkId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGroupLinkDAO.findReferenceGroupLink failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new ReferenceGroupLink into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceGroupLink
     * @exception   ProcessingException
     */
    public ReferenceGroupLink createReferenceGroupLink( ReferenceGroupLink businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referenceGroupLinkId" ) ;
    		businessObject.setReferenceGroupLinkId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ReferenceGroupLinkDAO.createReferenceGroupLink created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGroupLinkDAO.createReferenceGroupLink - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided ReferenceGroupLink to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceGroupLink	stored entity
     * @exception   ProcessingException 
     */
    public ReferenceGroupLink saveReferenceGroupLink( ReferenceGroupLink businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGroupLinkDAO:saveReferenceGroupLink - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a ReferenceGroupLink from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteReferenceGroupLink( ReferenceGroupLinkPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findReferenceGroupLink( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ReferenceGroupLinkDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ReferenceGroupLinkDAO.deleteReferenceGroupLink failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all ReferenceGroupLinks
     * @return		ArrayList<ReferenceGroupLink>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceGroupLink> findAllReferenceGroupLink()
    throws ProcessingException
    {
		final ArrayList<ReferenceGroupLink> list 	= new ArrayList<ReferenceGroupLink>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( ReferenceGroupLink.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ReferenceGroupLinkDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ReferenceGroupLinkDAO.findAllReferenceGroupLink failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ReferenceGroupLinkDAO:findAllReferenceGroupLinks() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.occulue.ReferenceGroupLink" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referenceGroupLinkId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(ReferenceGroupLink.class.getName());
}
