/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * Comment PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class CommentPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public CommentPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    commentId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public CommentPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object commentId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.commentId = commentId != null ? new Long( commentId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the commentId.
	* @return    Long
    */    
	public Long getCommentId()
	{
		return( this.commentId );
	}            
	
   /**
	* Assigns the commentId.
	* @return    Long
    */    
	public void setCommentId( Long id )
	{
		this.commentId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( commentId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( commentId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE Comment class.

// AIB : #getKeyFieldDeclarations()
	public Long commentId;
// ~AIB 	        

}
