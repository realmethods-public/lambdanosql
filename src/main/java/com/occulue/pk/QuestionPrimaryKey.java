/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * Question PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class QuestionPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public QuestionPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    questionId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public QuestionPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object questionId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.questionId = questionId != null ? new Long( questionId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the questionId.
	* @return    Long
    */    
	public Long getQuestionId()
	{
		return( this.questionId );
	}            
	
   /**
	* Assigns the questionId.
	* @return    Long
    */    
	public void setQuestionId( Long id )
	{
		this.questionId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( questionId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( questionId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE Question class.

// AIB : #getKeyFieldDeclarations()
	public Long questionId;
// ~AIB 	        

}
