/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * TheResponse PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class TheResponsePrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public TheResponsePrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    theResponseId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public TheResponsePrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object theResponseId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.theResponseId = theResponseId != null ? new Long( theResponseId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the theResponseId.
	* @return    Long
    */    
	public Long getTheResponseId()
	{
		return( this.theResponseId );
	}            
	
   /**
	* Assigns the theResponseId.
	* @return    Long
    */    
	public void setTheResponseId( Long id )
	{
		this.theResponseId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( theResponseId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( theResponseId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE TheResponse class.

// AIB : #getKeyFieldDeclarations()
	public Long theResponseId;
// ~AIB 	        

}
