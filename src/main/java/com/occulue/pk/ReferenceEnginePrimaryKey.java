/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * ReferenceEngine PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class ReferenceEnginePrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public ReferenceEnginePrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    referenceEngineId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public ReferenceEnginePrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object referenceEngineId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.referenceEngineId = referenceEngineId != null ? new Long( referenceEngineId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the referenceEngineId.
	* @return    Long
    */    
	public Long getReferenceEngineId()
	{
		return( this.referenceEngineId );
	}            
	
   /**
	* Assigns the referenceEngineId.
	* @return    Long
    */    
	public void setReferenceEngineId( Long id )
	{
		this.referenceEngineId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( referenceEngineId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( referenceEngineId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE ReferenceEngine class.

// AIB : #getKeyFieldDeclarations()
	public Long referenceEngineId;
// ~AIB 	        

}
