/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * ReferrerGroup PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class ReferrerGroupPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public ReferrerGroupPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    referrerGroupId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public ReferrerGroupPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object referrerGroupId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.referrerGroupId = referrerGroupId != null ? new Long( referrerGroupId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the referrerGroupId.
	* @return    Long
    */    
	public Long getReferrerGroupId()
	{
		return( this.referrerGroupId );
	}            
	
   /**
	* Assigns the referrerGroupId.
	* @return    Long
    */    
	public void setReferrerGroupId( Long id )
	{
		this.referrerGroupId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( referrerGroupId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( referrerGroupId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE ReferrerGroup class.

// AIB : #getKeyFieldDeclarations()
	public Long referrerGroupId;
// ~AIB 	        

}
