/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * ReferenceGroupLink PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class ReferenceGroupLinkPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public ReferenceGroupLinkPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    referenceGroupLinkId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public ReferenceGroupLinkPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object referenceGroupLinkId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.referenceGroupLinkId = referenceGroupLinkId != null ? new Long( referenceGroupLinkId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the referenceGroupLinkId.
	* @return    Long
    */    
	public Long getReferenceGroupLinkId()
	{
		return( this.referenceGroupLinkId );
	}            
	
   /**
	* Assigns the referenceGroupLinkId.
	* @return    Long
    */    
	public void setReferenceGroupLinkId( Long id )
	{
		this.referenceGroupLinkId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( referenceGroupLinkId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( referenceGroupLinkId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE ReferenceGroupLink class.

// AIB : #getKeyFieldDeclarations()
	public Long referenceGroupLinkId;
// ~AIB 	        

}
