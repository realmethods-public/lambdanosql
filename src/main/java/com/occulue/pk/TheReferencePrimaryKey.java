/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.primarykey;

import java.util.*;

/**
 * TheReference PrimaryKey class.
 * 
 * @author    dev@realmethods.com
 */
// AIB : #getPrimaryKeyClassDecl() 
public class TheReferencePrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public TheReferencePrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    theReferenceId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public TheReferencePrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object theReferenceId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.theReferenceId = theReferenceId != null ? new Long( theReferenceId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the theReferenceId.
	* @return    Long
    */    
	public Long getTheReferenceId()
	{
		return( this.theReferenceId );
	}            
	
   /**
	* Assigns the theReferenceId.
	* @return    Long
    */    
	public void setTheReferenceId( Long id )
	{
		this.theReferenceId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( theReferenceId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( theReferenceId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE TheReference class.

// AIB : #getKeyFieldDeclarations()
	public Long theReferenceId;
// ~AIB 	        

}
