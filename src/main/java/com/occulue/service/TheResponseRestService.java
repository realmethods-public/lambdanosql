/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity TheResponse.
 *
 * @author dev@realmethods.com
 */

public class TheResponseRestService extends BaseRestService
{

	public TheResponseRestService()
	{}
	
    /**
     * Handles saving a TheResponse BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected TheResponse save()
    	throws ProcessingException
    {
		// doing it here helps
		getTheResponse();

		LOGGER.info( "TheResponse.save() on - " + theResponse );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the theResponse is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( theResponse != null && theResponse.getTheResponsePrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a TheResponse BO
     * @return		TheResponse
     * @exception	ProcessingException
     */    
    protected TheResponse update()
    	throws ProcessingException
    {
    	// store provided data
        TheResponse tmp = theResponse;

        // load actual data from storage
        loadHelper( theResponse.getTheResponsePrimaryKey() );
    	
    	// copy provided data into actual data
    	theResponse.copyShallow( tmp );
    	
        try
        {                        	        
			// create the TheResponseBusiness Delegate            
			TheResponseBusinessDelegate delegate = TheResponseBusinessDelegate.getTheResponseInstance();
            this.theResponse = delegate.saveTheResponse( theResponse );
            
            if ( this.theResponse != null )
            	LOGGER.info( "TheResponseRestService:update() - successfully updated TheResponse - " + theResponse.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "TheResponseRestService:update() - successfully update TheResponse - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.theResponse;
        
    }

    /**
     * Handles creating a TheResponse BO
     * @return		TheResponse
     */
    protected TheResponse create()
    	throws ProcessingException
    {
        try
        {       
        	theResponse 		= getTheResponse();
			this.theResponse 	= TheResponseBusinessDelegate.getTheResponseInstance().createTheResponse( theResponse );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "TheResponseRestService:create() - exception TheResponse - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.theResponse;
    }


    
    /**
     * Handles deleting a TheResponse BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	TheResponseBusinessDelegate delegate = TheResponseBusinessDelegate.getTheResponseInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long theResponseId  = parseId( "theResponseId" );
        		delegate.delete( new TheResponsePrimaryKey( theResponseId  ) );
        		LOGGER.info( "TheResponseRestService:delete() - successfully deleted TheResponse with key " + theResponse.getTheResponsePrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new TheResponsePrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "TheResponseRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "TheResponseRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a TheResponse BO
     * @param		Long theResponseId
     * @exception	ProcessingException
     * @return		TheResponse
     */    
    protected TheResponse load() 
    	throws ProcessingException
    {    	
        TheResponsePrimaryKey pk 	= null;
		Long theResponseId  = parseId( "theResponseId" );

    	try
        {
    		LOGGER.info( "TheResponse.load pk is " + theResponseId );
    		
        	if ( theResponseId != null )
        	{
        		pk = new TheResponsePrimaryKey( theResponseId );

        		loadHelper( pk );

        		// load the contained instance of TheResponse
	            this.theResponse = TheResponseBusinessDelegate.getTheResponseInstance().getTheResponse( pk );
	            
	            LOGGER.info( "TheResponseRestService:load() - successfully loaded - " + this.theResponse.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "TheResponseRestService:load() - unable to locate the primary key as an attribute or a selection for - " + theResponse.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheResponseRestService:load() - failed to load TheResponse using Id " + theResponseId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return theResponse;

    }

    /**
     * Handles loading all TheResponse business objects
     * @return		List<TheResponse>
     * @exception	ProcessingException
     */
    protected List<TheResponse> loadAll()
    	throws ProcessingException
    {                
        List<TheResponse> theResponseList = null;
        
    	try
        {                        
            // load the TheResponse
            theResponseList = TheResponseBusinessDelegate.getTheResponseInstance().getAllTheResponse();
            
            if ( theResponseList != null )
            	LOGGER.info(  "TheResponseRestService:loadAllTheResponse() - successfully loaded all TheResponses" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheResponseRestService:loadAll() - failed to load all TheResponses - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return theResponseList;
                            
    }






    protected TheResponse loadHelper( TheResponsePrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "TheResponse.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of TheResponse
	            this.theResponse = TheResponseBusinessDelegate.getTheResponseInstance().getTheResponse( pk );
	            
	            LOGGER.info( "TheResponseRestService:loadHelper() - successfully loaded - " + this.theResponse.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "TheResponseRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheResponseRestService:load() - failed to load TheResponse using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return theResponse;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
          default:
        	signalBadRequest();
            throw new ProcessingException("TheResponse.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a TheResponse. Found in the request body.
	 * 
	 * @return TheResponse
	 */
	private TheResponse getTheResponse()
	{
		if ( theResponse == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				theResponse = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), TheResponse.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "TheResponseRestService.getTheResponse() - failed to Json map from String to TheResponse - " + exc.getMessage() );
	        }			
		}
		return( theResponse );
	}
	
	protected String getSubclassName()
	{ return( "TheResponseRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private TheResponse theResponse 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
