/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Question.
 *
 * @author dev@realmethods.com
 */

public class QuestionRestService extends BaseRestService
{

	public QuestionRestService()
	{}
	
    /**
     * Handles saving a Question BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Question save()
    	throws ProcessingException
    {
		// doing it here helps
		getQuestion();

		LOGGER.info( "Question.save() on - " + question );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the question is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( question != null && question.getQuestionPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Question BO
     * @return		Question
     * @exception	ProcessingException
     */    
    protected Question update()
    	throws ProcessingException
    {
    	// store provided data
        Question tmp = question;

        // load actual data from storage
        loadHelper( question.getQuestionPrimaryKey() );
    	
    	// copy provided data into actual data
    	question.copyShallow( tmp );
    	
        try
        {                        	        
			// create the QuestionBusiness Delegate            
			QuestionBusinessDelegate delegate = QuestionBusinessDelegate.getQuestionInstance();
            this.question = delegate.saveQuestion( question );
            
            if ( this.question != null )
            	LOGGER.info( "QuestionRestService:update() - successfully updated Question - " + question.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "QuestionRestService:update() - successfully update Question - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.question;
        
    }

    /**
     * Handles creating a Question BO
     * @return		Question
     */
    protected Question create()
    	throws ProcessingException
    {
        try
        {       
        	question 		= getQuestion();
			this.question 	= QuestionBusinessDelegate.getQuestionInstance().createQuestion( question );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "QuestionRestService:create() - exception Question - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.question;
    }


    
    /**
     * Handles deleting a Question BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	QuestionBusinessDelegate delegate = QuestionBusinessDelegate.getQuestionInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long questionId  = parseId( "questionId" );
        		delegate.delete( new QuestionPrimaryKey( questionId  ) );
        		LOGGER.info( "QuestionRestService:delete() - successfully deleted Question with key " + question.getQuestionPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new QuestionPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "QuestionRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "QuestionRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Question BO
     * @param		Long questionId
     * @exception	ProcessingException
     * @return		Question
     */    
    protected Question load() 
    	throws ProcessingException
    {    	
        QuestionPrimaryKey pk 	= null;
		Long questionId  = parseId( "questionId" );

    	try
        {
    		LOGGER.info( "Question.load pk is " + questionId );
    		
        	if ( questionId != null )
        	{
        		pk = new QuestionPrimaryKey( questionId );

        		loadHelper( pk );

        		// load the contained instance of Question
	            this.question = QuestionBusinessDelegate.getQuestionInstance().getQuestion( pk );
	            
	            LOGGER.info( "QuestionRestService:load() - successfully loaded - " + this.question.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "QuestionRestService:load() - unable to locate the primary key as an attribute or a selection for - " + question.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionRestService:load() - failed to load Question using Id " + questionId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return question;

    }

    /**
     * Handles loading all Question business objects
     * @return		List<Question>
     * @exception	ProcessingException
     */
    protected List<Question> loadAll()
    	throws ProcessingException
    {                
        List<Question> questionList = null;
        
    	try
        {                        
            // load the Question
            questionList = QuestionBusinessDelegate.getQuestionInstance().getAllQuestion();
            
            if ( questionList != null )
            	LOGGER.info(  "QuestionRestService:loadAllQuestion() - successfully loaded all Questions" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionRestService:loadAll() - failed to load all Questions - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return questionList;
                            
    }




    /**
     * save Responses on Question
     * @param		Long questionId
     * @param		Long childId
     * @param		String[] childIds
     * @return		Question
     * @exception	ProcessingException
     */     
	protected Question saveResponses()
		throws ProcessingException
	{
		Long questionId = parseId( "questionId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionPrimaryKey( questionId ) ) == null )
			throw new ProcessingException( "Question.saveResponses() - failed to load parent using Id " + questionId );
		 
		TheResponsePrimaryKey pk 					= null;
		TheResponse child							= null;
		List<TheResponse> childList				= null;
		TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
		QuestionBusinessDelegate parentDelegate = QuestionBusinessDelegate.getQuestionInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new TheResponsePrimaryKey( childId );
			
			try
			{
				// find the TheResponse
				child = childDelegate.getTheResponse( pk );
				LOGGER.info( "LeagueRestService:saveQuestion() - found TheResponse" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "QuestionRestService:saveResponses() failed get child TheResponse using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Responses, check for null
			if ( question.getResponses() != null )
				question.getResponses().add( child );

			LOGGER.info( "LeagueRestService:saveQuestion() - added TheResponse to parent" );

		}
		else
		{
			// clear or create the Responses
			if ( question.getResponses() != null )
				question.getResponses().clear();
			else
				question.setResponses( new HashSet<TheResponse>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new TheResponsePrimaryKey( id );
					try
					{
						// find the TheResponse
						child = childDelegate.getTheResponse( pk );
						// add it to the Responses List
						question.getResponses().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "QuestionRestService:saveResponses() failed get child TheResponse using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the Question
			parentDelegate.saveQuestion( question );
			LOGGER.info( "LeagueRestService:saveQuestion() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "QuestionRestService:saveResponses() failed saving parent Question - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return question;
	}

    /**
     * delete Responses on Question
     * @return		Question
     * @exception	ProcessingException
     */     	
	protected Question deleteResponses()
		throws ProcessingException
	{		
		Long questionId = parseId( "questionId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new QuestionPrimaryKey( questionId ) ) == null )
			throw new ProcessingException( "Question.deleteResponses() - failed to load using Id " + questionId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			TheResponsePrimaryKey pk 					= null;
			TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
			QuestionBusinessDelegate parentDelegate = QuestionBusinessDelegate.getQuestionInstance();
			Set<TheResponse> children					= question.getResponses();
			TheResponse child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new TheResponsePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getTheResponse( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "QuestionRestService:deleteResponses() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of TheResponse back to the question
			question.setResponses( children );
			
			// save it 
			try
			{
				question = parentDelegate.saveQuestion( question );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "QuestionRestService:deleteResponses() failed to save the Question - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return question;
	}



    protected Question loadHelper( QuestionPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Question.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Question
	            this.question = QuestionBusinessDelegate.getQuestionInstance().getQuestion( pk );
	            
	            LOGGER.info( "QuestionRestService:loadHelper() - successfully loaded - " + this.question.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "QuestionRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "QuestionRestService:load() - failed to load Question using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return question;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveResponses":
	        	returnVal = saveResponses().getResponses();
	        	break;
	        case "deleteResponses":
	        	returnVal = deleteResponses().getResponses();
	        	break;
	        case "loadResponses" :
	        	returnVal = load().getResponses();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("Question.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Question. Found in the request body.
	 * 
	 * @return Question
	 */
	private Question getQuestion()
	{
		if ( question == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				question = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Question.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "QuestionRestService.getQuestion() - failed to Json map from String to Question - " + exc.getMessage() );
	        }			
		}
		return( question );
	}
	
	protected String getSubclassName()
	{ return( "QuestionRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Question question 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
