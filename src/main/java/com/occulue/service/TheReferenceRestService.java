/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity TheReference.
 *
 * @author dev@realmethods.com
 */

public class TheReferenceRestService extends BaseRestService
{

	public TheReferenceRestService()
	{}
	
    /**
     * Handles saving a TheReference BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected TheReference save()
    	throws ProcessingException
    {
		// doing it here helps
		getTheReference();

		LOGGER.info( "TheReference.save() on - " + theReference );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the theReference is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( theReference != null && theReference.getTheReferencePrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a TheReference BO
     * @return		TheReference
     * @exception	ProcessingException
     */    
    protected TheReference update()
    	throws ProcessingException
    {
    	// store provided data
        TheReference tmp = theReference;

        // load actual data from storage
        loadHelper( theReference.getTheReferencePrimaryKey() );
    	
    	// copy provided data into actual data
    	theReference.copyShallow( tmp );
    	
        try
        {                        	        
			// create the TheReferenceBusiness Delegate            
			TheReferenceBusinessDelegate delegate = TheReferenceBusinessDelegate.getTheReferenceInstance();
            this.theReference = delegate.saveTheReference( theReference );
            
            if ( this.theReference != null )
            	LOGGER.info( "TheReferenceRestService:update() - successfully updated TheReference - " + theReference.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "TheReferenceRestService:update() - successfully update TheReference - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.theReference;
        
    }

    /**
     * Handles creating a TheReference BO
     * @return		TheReference
     */
    protected TheReference create()
    	throws ProcessingException
    {
        try
        {       
        	theReference 		= getTheReference();
			this.theReference 	= TheReferenceBusinessDelegate.getTheReferenceInstance().createTheReference( theReference );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "TheReferenceRestService:create() - exception TheReference - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.theReference;
    }


    
    /**
     * Handles deleting a TheReference BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	TheReferenceBusinessDelegate delegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long theReferenceId  = parseId( "theReferenceId" );
        		delegate.delete( new TheReferencePrimaryKey( theReferenceId  ) );
        		LOGGER.info( "TheReferenceRestService:delete() - successfully deleted TheReference with key " + theReference.getTheReferencePrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new TheReferencePrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "TheReferenceRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "TheReferenceRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a TheReference BO
     * @param		Long theReferenceId
     * @exception	ProcessingException
     * @return		TheReference
     */    
    protected TheReference load() 
    	throws ProcessingException
    {    	
        TheReferencePrimaryKey pk 	= null;
		Long theReferenceId  = parseId( "theReferenceId" );

    	try
        {
    		LOGGER.info( "TheReference.load pk is " + theReferenceId );
    		
        	if ( theReferenceId != null )
        	{
        		pk = new TheReferencePrimaryKey( theReferenceId );

        		loadHelper( pk );

        		// load the contained instance of TheReference
	            this.theReference = TheReferenceBusinessDelegate.getTheReferenceInstance().getTheReference( pk );
	            
	            LOGGER.info( "TheReferenceRestService:load() - successfully loaded - " + this.theReference.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:load() - unable to locate the primary key as an attribute or a selection for - " + theReference.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheReferenceRestService:load() - failed to load TheReference using Id " + theReferenceId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return theReference;

    }

    /**
     * Handles loading all TheReference business objects
     * @return		List<TheReference>
     * @exception	ProcessingException
     */
    protected List<TheReference> loadAll()
    	throws ProcessingException
    {                
        List<TheReference> theReferenceList = null;
        
    	try
        {                        
            // load the TheReference
            theReferenceList = TheReferenceBusinessDelegate.getTheReferenceInstance().getAllTheReference();
            
            if ( theReferenceList != null )
            	LOGGER.info(  "TheReferenceRestService:loadAllTheReference() - successfully loaded all TheReferences" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheReferenceRestService:loadAll() - failed to load all TheReferences - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return theReferenceList;
                            
    }



    /**
     * save QuestionGroup on TheReference
     * @param		TheReference theReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference saveQuestionGroup()
		throws ProcessingException
	{
		Long theReferenceId 	= parseId( "theReferenceId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionGroupBusinessDelegate childDelegate 	= QuestionGroupBusinessDelegate.getQuestionGroupInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			QuestionGroup child 							= null;

			try
			{
				child = childDelegate.getQuestionGroup( new QuestionGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "TheReferenceRestService:saveQuestionGroup() failed to get QuestionGroup using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			theReference.setQuestionGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:saveQuestionGroup() failed saving parent TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}

    /**
     * delete QuestionGroup on TheReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference deleteQuestionGroup()
		throws ProcessingException
	{
		Long theReferenceId = parseId( "theReferenceId" );
		
 		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );

		if ( theReference.getQuestionGroup() != null )
		{
			QuestionGroupPrimaryKey pk = theReference.getQuestionGroup().getQuestionGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setQuestionGroup( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteQuestionGroup() failed to save TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteQuestionGroup() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}
	
    /**
     * save User on TheReference
     * @param		TheReference theReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference saveUser()
		throws ProcessingException
	{
		Long theReferenceId 	= parseId( "theReferenceId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "TheReferenceRestService:saveUser() failed to get User using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			theReference.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:saveUser() failed saving parent TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}

    /**
     * delete User on TheReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference deleteUser()
		throws ProcessingException
	{
		Long theReferenceId = parseId( "theReferenceId" );
		
 		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );

		if ( theReference.getUser() != null )
		{
			UserPrimaryKey pk = theReference.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setUser( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteUser() failed to save TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteUser() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}
	
    /**
     * save Referrer on TheReference
     * @param		TheReference theReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference saveReferrer()
		throws ProcessingException
	{
		Long theReferenceId 	= parseId( "theReferenceId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "TheReferenceRestService:saveReferrer() failed to get Referrer using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			theReference.setReferrer( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:saveReferrer() failed saving parent TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}

    /**
     * delete Referrer on TheReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference deleteReferrer()
		throws ProcessingException
	{
		Long theReferenceId = parseId( "theReferenceId" );
		
 		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );

		if ( theReference.getReferrer() != null )
		{
			ReferrerPrimaryKey pk = theReference.getReferrer().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setReferrer( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteReferrer() failed to save TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteReferrer() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}
	
    /**
     * save LastQuestionAnswered on TheReference
     * @param		TheReference theReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference saveLastQuestionAnswered()
		throws ProcessingException
	{
		Long theReferenceId 	= parseId( "theReferenceId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "TheReferenceRestService:saveLastQuestionAnswered() failed to get Question using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			theReference.setLastQuestionAnswered( child );
		
			try
			{
				// save it
				parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:saveLastQuestionAnswered() failed saving parent TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}

    /**
     * delete LastQuestionAnswered on TheReference
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference deleteLastQuestionAnswered()
		throws ProcessingException
	{
		Long theReferenceId = parseId( "theReferenceId" );
		
 		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			return( null );

		if ( theReference.getLastQuestionAnswered() != null )
		{
			QuestionPrimaryKey pk = theReference.getLastQuestionAnswered().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			theReference.setLastQuestionAnswered( null );
			try
			{
				TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();

				// save it
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteLastQuestionAnswered() failed to save TheReference - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:deleteLastQuestionAnswered() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}
	

    /**
     * save Answers on TheReference
     * @param		Long theReferenceId
     * @param		Long childId
     * @param		String[] childIds
     * @return		TheReference
     * @exception	ProcessingException
     */     
	protected TheReference saveAnswers()
		throws ProcessingException
	{
		Long theReferenceId = parseId( "theReferenceId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			throw new ProcessingException( "TheReference.saveAnswers() - failed to load parent using Id " + theReferenceId );
		 
		AnswerPrimaryKey pk 					= null;
		Answer child							= null;
		List<Answer> childList				= null;
		AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
		TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new AnswerPrimaryKey( childId );
			
			try
			{
				// find the Answer
				child = childDelegate.getAnswer( pk );
				LOGGER.info( "LeagueRestService:saveTheReference() - found Answer" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:saveAnswers() failed get child Answer using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Answers, check for null
			if ( theReference.getAnswers() != null )
				theReference.getAnswers().add( child );

			LOGGER.info( "LeagueRestService:saveTheReference() - added Answer to parent" );

		}
		else
		{
			// clear or create the Answers
			if ( theReference.getAnswers() != null )
				theReference.getAnswers().clear();
			else
				theReference.setAnswers( new HashSet<Answer>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new AnswerPrimaryKey( id );
					try
					{
						// find the Answer
						child = childDelegate.getAnswer( pk );
						// add it to the Answers List
						theReference.getAnswers().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "TheReferenceRestService:saveAnswers() failed get child Answer using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the TheReference
			parentDelegate.saveTheReference( theReference );
			LOGGER.info( "LeagueRestService:saveTheReference() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "TheReferenceRestService:saveAnswers() failed saving parent TheReference - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return theReference;
	}

    /**
     * delete Answers on TheReference
     * @return		TheReference
     * @exception	ProcessingException
     */     	
	protected TheReference deleteAnswers()
		throws ProcessingException
	{		
		Long theReferenceId = parseId( "theReferenceId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new TheReferencePrimaryKey( theReferenceId ) ) == null )
			throw new ProcessingException( "TheReference.deleteAnswers() - failed to load using Id " + theReferenceId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			AnswerPrimaryKey pk 					= null;
			AnswerBusinessDelegate childDelegate 	= AnswerBusinessDelegate.getAnswerInstance();
			TheReferenceBusinessDelegate parentDelegate = TheReferenceBusinessDelegate.getTheReferenceInstance();
			Set<Answer> children					= theReference.getAnswers();
			Answer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new AnswerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getAnswer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "TheReferenceRestService:deleteAnswers() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Answer back to the theReference
			theReference.setAnswers( children );
			
			// save it 
			try
			{
				theReference = parentDelegate.saveTheReference( theReference );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "TheReferenceRestService:deleteAnswers() failed to save the TheReference - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return theReference;
	}



    protected TheReference loadHelper( TheReferencePrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "TheReference.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of TheReference
	            this.theReference = TheReferenceBusinessDelegate.getTheReferenceInstance().getTheReference( pk );
	            
	            LOGGER.info( "TheReferenceRestService:loadHelper() - successfully loaded - " + this.theReference.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "TheReferenceRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "TheReferenceRestService:load() - failed to load TheReference using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return theReference;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveAnswers":
	        	returnVal = saveAnswers().getAnswers();
	        	break;
	        case "deleteAnswers":
	        	returnVal = deleteAnswers().getAnswers();
	        	break;
	        case "loadAnswers" :
	        	returnVal = load().getAnswers();
	        	break;
 	        case "saveQuestionGroup":
	        	returnVal = saveQuestionGroup().getQuestionGroup();
	        	break;
	        case "deleteQuestionGroup":
	        	returnVal = deleteQuestionGroup().getQuestionGroup();
	        	break;
	        case "loadQuestionGroup" :
	        	returnVal = load().getQuestionGroup();
	        	break;
	        case "saveUser":
	        	returnVal = saveUser().getUser();
	        	break;
	        case "deleteUser":
	        	returnVal = deleteUser().getUser();
	        	break;
	        case "loadUser" :
	        	returnVal = load().getUser();
	        	break;
	        case "saveReferrer":
	        	returnVal = saveReferrer().getReferrer();
	        	break;
	        case "deleteReferrer":
	        	returnVal = deleteReferrer().getReferrer();
	        	break;
	        case "loadReferrer" :
	        	returnVal = load().getReferrer();
	        	break;
	        case "saveLastQuestionAnswered":
	        	returnVal = saveLastQuestionAnswered().getLastQuestionAnswered();
	        	break;
	        case "deleteLastQuestionAnswered":
	        	returnVal = deleteLastQuestionAnswered().getLastQuestionAnswered();
	        	break;
	        case "loadLastQuestionAnswered" :
	        	returnVal = load().getLastQuestionAnswered();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("TheReference.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a TheReference. Found in the request body.
	 * 
	 * @return TheReference
	 */
	private TheReference getTheReference()
	{
		if ( theReference == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				theReference = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), TheReference.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "TheReferenceRestService.getTheReference() - failed to Json map from String to TheReference - " + exc.getMessage() );
	        }			
		}
		return( theReference );
	}
	
	protected String getSubclassName()
	{ return( "TheReferenceRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private TheReference theReference 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
