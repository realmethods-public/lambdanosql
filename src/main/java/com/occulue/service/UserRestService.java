/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity User.
 *
 * @author dev@realmethods.com
 */

public class UserRestService extends BaseRestService
{

	public UserRestService()
	{}
	
    /**
     * Handles saving a User BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected User save()
    	throws ProcessingException
    {
		// doing it here helps
		getUser();

		LOGGER.info( "User.save() on - " + user );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the user is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( user != null && user.getUserPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a User BO
     * @return		User
     * @exception	ProcessingException
     */    
    protected User update()
    	throws ProcessingException
    {
    	// store provided data
        User tmp = user;

        // load actual data from storage
        loadHelper( user.getUserPrimaryKey() );
    	
    	// copy provided data into actual data
    	user.copyShallow( tmp );
    	
        try
        {                        	        
			// create the UserBusiness Delegate            
			UserBusinessDelegate delegate = UserBusinessDelegate.getUserInstance();
            this.user = delegate.saveUser( user );
            
            if ( this.user != null )
            	LOGGER.info( "UserRestService:update() - successfully updated User - " + user.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "UserRestService:update() - successfully update User - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.user;
        
    }

    /**
     * Handles creating a User BO
     * @return		User
     */
    protected User create()
    	throws ProcessingException
    {
        try
        {       
        	user 		= getUser();
			this.user 	= UserBusinessDelegate.getUserInstance().createUser( user );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "UserRestService:create() - exception User - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.user;
    }


    
    /**
     * Handles deleting a User BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	UserBusinessDelegate delegate = UserBusinessDelegate.getUserInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long userId  = parseId( "referrerId" );
        		delegate.delete( new UserPrimaryKey( userId  ) );
        		LOGGER.info( "UserRestService:delete() - successfully deleted User with key " + user.getUserPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new UserPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "UserRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "UserRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a User BO
     * @param		Long userId
     * @exception	ProcessingException
     * @return		User
     */    
    protected User load() 
    	throws ProcessingException
    {    	
        UserPrimaryKey pk 	= null;
		Long userId  = parseId( "referrerId" );

    	try
        {
    		LOGGER.info( "User.load pk is " + userId );
    		
        	if ( userId != null )
        	{
        		pk = new UserPrimaryKey( userId );

        		loadHelper( pk );

        		// load the contained instance of User
	            this.user = UserBusinessDelegate.getUserInstance().getUser( pk );
	            
	            LOGGER.info( "UserRestService:load() - successfully loaded - " + this.user.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:load() - unable to locate the primary key as an attribute or a selection for - " + user.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "UserRestService:load() - failed to load User using Id " + userId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return user;

    }

    /**
     * Handles loading all User business objects
     * @return		List<User>
     * @exception	ProcessingException
     */
    protected List<User> loadAll()
    	throws ProcessingException
    {                
        List<User> userList = null;
        
    	try
        {                        
            // load the User
            userList = UserBusinessDelegate.getUserInstance().getAllUser();
            
            if ( userList != null )
            	LOGGER.info(  "UserRestService:loadAllUser() - successfully loaded all Users" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "UserRestService:loadAll() - failed to load all Users - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return userList;
                            
    }




    /**
     * save ReferenceProviders on User
     * @param		Long userId
     * @param		Long childId
     * @param		String[] childIds
     * @return		User
     * @exception	ProcessingException
     */     
	protected User saveReferenceProviders()
		throws ProcessingException
	{
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.saveReferenceProviders() - failed to load parent using Id " + userId );
		 
		ReferrerPrimaryKey pk 					= null;
		Referrer child							= null;
		List<Referrer> childList				= null;
		ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerPrimaryKey( childId );
			
			try
			{
				// find the Referrer
				child = childDelegate.getReferrer( pk );
				LOGGER.info( "LeagueRestService:saveUser() - found Referrer" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:saveReferenceProviders() failed get child Referrer using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the ReferenceProviders, check for null
			if ( user.getReferenceProviders() != null )
				user.getReferenceProviders().add( child );

			LOGGER.info( "LeagueRestService:saveUser() - added Referrer to parent" );

		}
		else
		{
			// clear or create the ReferenceProviders
			if ( user.getReferenceProviders() != null )
				user.getReferenceProviders().clear();
			else
				user.setReferenceProviders( new HashSet<Referrer>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerPrimaryKey( id );
					try
					{
						// find the Referrer
						child = childDelegate.getReferrer( pk );
						// add it to the ReferenceProviders List
						user.getReferenceProviders().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "UserRestService:saveReferenceProviders() failed get child Referrer using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
			LOGGER.info( "LeagueRestService:saveUser() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "UserRestService:saveReferenceProviders() failed saving parent User - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return user;
	}

    /**
     * delete ReferenceProviders on User
     * @return		User
     * @exception	ProcessingException
     */     	
	protected User deleteReferenceProviders()
		throws ProcessingException
	{		
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.deleteReferenceProviders() - failed to load using Id " + userId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			ReferrerPrimaryKey pk 					= null;
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<Referrer> children					= user.getReferenceProviders();
			Referrer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "UserRestService:deleteReferenceProviders() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceProviders( children );
			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "UserRestService:deleteReferenceProviders() failed to save the User - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return user;
	}

    /**
     * save RefererGroups on User
     * @param		Long userId
     * @param		Long childId
     * @param		String[] childIds
     * @return		User
     * @exception	ProcessingException
     */     
	protected User saveRefererGroups()
		throws ProcessingException
	{
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.saveRefererGroups() - failed to load parent using Id " + userId );
		 
		ReferrerGroupPrimaryKey pk 					= null;
		ReferrerGroup child							= null;
		List<ReferrerGroup> childList				= null;
		ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerGroupPrimaryKey( childId );
			
			try
			{
				// find the ReferrerGroup
				child = childDelegate.getReferrerGroup( pk );
				LOGGER.info( "LeagueRestService:saveUser() - found ReferrerGroup" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:saveRefererGroups() failed get child ReferrerGroup using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the RefererGroups, check for null
			if ( user.getRefererGroups() != null )
				user.getRefererGroups().add( child );

			LOGGER.info( "LeagueRestService:saveUser() - added ReferrerGroup to parent" );

		}
		else
		{
			// clear or create the RefererGroups
			if ( user.getRefererGroups() != null )
				user.getRefererGroups().clear();
			else
				user.setRefererGroups( new HashSet<ReferrerGroup>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerGroupPrimaryKey( id );
					try
					{
						// find the ReferrerGroup
						child = childDelegate.getReferrerGroup( pk );
						// add it to the RefererGroups List
						user.getRefererGroups().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "UserRestService:saveRefererGroups() failed get child ReferrerGroup using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
			LOGGER.info( "LeagueRestService:saveUser() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "UserRestService:saveRefererGroups() failed saving parent User - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return user;
	}

    /**
     * delete RefererGroups on User
     * @return		User
     * @exception	ProcessingException
     */     	
	protected User deleteRefererGroups()
		throws ProcessingException
	{		
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.deleteRefererGroups() - failed to load using Id " + userId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			ReferrerGroupPrimaryKey pk 					= null;
			ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<ReferrerGroup> children					= user.getRefererGroups();
			ReferrerGroup child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerGroupPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrerGroup( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "UserRestService:deleteRefererGroups() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of ReferrerGroup back to the user
			user.setRefererGroups( children );
			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "UserRestService:deleteRefererGroups() failed to save the User - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return user;
	}

    /**
     * save ReferenceReceivers on User
     * @param		Long userId
     * @param		Long childId
     * @param		String[] childIds
     * @return		User
     * @exception	ProcessingException
     */     
	protected User saveReferenceReceivers()
		throws ProcessingException
	{
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.saveReferenceReceivers() - failed to load parent using Id " + userId );
		 
		ReferrerPrimaryKey pk 					= null;
		Referrer child							= null;
		List<Referrer> childList				= null;
		ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferrerPrimaryKey( childId );
			
			try
			{
				// find the Referrer
				child = childDelegate.getReferrer( pk );
				LOGGER.info( "LeagueRestService:saveUser() - found Referrer" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:saveReferenceReceivers() failed get child Referrer using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the ReferenceReceivers, check for null
			if ( user.getReferenceReceivers() != null )
				user.getReferenceReceivers().add( child );

			LOGGER.info( "LeagueRestService:saveUser() - added Referrer to parent" );

		}
		else
		{
			// clear or create the ReferenceReceivers
			if ( user.getReferenceReceivers() != null )
				user.getReferenceReceivers().clear();
			else
				user.setReferenceReceivers( new HashSet<Referrer>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferrerPrimaryKey( id );
					try
					{
						// find the Referrer
						child = childDelegate.getReferrer( pk );
						// add it to the ReferenceReceivers List
						user.getReferenceReceivers().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "UserRestService:saveReferenceReceivers() failed get child Referrer using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
			LOGGER.info( "LeagueRestService:saveUser() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "UserRestService:saveReferenceReceivers() failed saving parent User - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return user;
	}

    /**
     * delete ReferenceReceivers on User
     * @return		User
     * @exception	ProcessingException
     */     	
	protected User deleteReferenceReceivers()
		throws ProcessingException
	{		
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.deleteReferenceReceivers() - failed to load using Id " + userId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			ReferrerPrimaryKey pk 					= null;
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<Referrer> children					= user.getReferenceReceivers();
			Referrer child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferrerPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferrer( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "UserRestService:deleteReferenceReceivers() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Referrer back to the user
			user.setReferenceReceivers( children );
			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "UserRestService:deleteReferenceReceivers() failed to save the User - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return user;
	}

    /**
     * save ReferenceGroupLinks on User
     * @param		Long userId
     * @param		Long childId
     * @param		String[] childIds
     * @return		User
     * @exception	ProcessingException
     */     
	protected User saveReferenceGroupLinks()
		throws ProcessingException
	{
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.saveReferenceGroupLinks() - failed to load parent using Id " + userId );
		 
		ReferenceGroupLinkPrimaryKey pk 					= null;
		ReferenceGroupLink child							= null;
		List<ReferenceGroupLink> childList				= null;
		ReferenceGroupLinkBusinessDelegate childDelegate 	= ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
		UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferenceGroupLinkPrimaryKey( childId );
			
			try
			{
				// find the ReferenceGroupLink
				child = childDelegate.getReferenceGroupLink( pk );
				LOGGER.info( "LeagueRestService:saveUser() - found ReferenceGroupLink" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:saveReferenceGroupLinks() failed get child ReferenceGroupLink using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the ReferenceGroupLinks, check for null
			if ( user.getReferenceGroupLinks() != null )
				user.getReferenceGroupLinks().add( child );

			LOGGER.info( "LeagueRestService:saveUser() - added ReferenceGroupLink to parent" );

		}
		else
		{
			// clear or create the ReferenceGroupLinks
			if ( user.getReferenceGroupLinks() != null )
				user.getReferenceGroupLinks().clear();
			else
				user.setReferenceGroupLinks( new HashSet<ReferenceGroupLink>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferenceGroupLinkPrimaryKey( id );
					try
					{
						// find the ReferenceGroupLink
						child = childDelegate.getReferenceGroupLink( pk );
						// add it to the ReferenceGroupLinks List
						user.getReferenceGroupLinks().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "UserRestService:saveReferenceGroupLinks() failed get child ReferenceGroupLink using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the User
			parentDelegate.saveUser( user );
			LOGGER.info( "LeagueRestService:saveUser() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "UserRestService:saveReferenceGroupLinks() failed saving parent User - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return user;
	}

    /**
     * delete ReferenceGroupLinks on User
     * @return		User
     * @exception	ProcessingException
     */     	
	protected User deleteReferenceGroupLinks()
		throws ProcessingException
	{		
		Long userId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new UserPrimaryKey( userId ) ) == null )
			throw new ProcessingException( "User.deleteReferenceGroupLinks() - failed to load using Id " + userId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			ReferenceGroupLinkPrimaryKey pk 					= null;
			ReferenceGroupLinkBusinessDelegate childDelegate 	= ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
			UserBusinessDelegate parentDelegate = UserBusinessDelegate.getUserInstance();
			Set<ReferenceGroupLink> children					= user.getReferenceGroupLinks();
			ReferenceGroupLink child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferenceGroupLinkPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferenceGroupLink( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "UserRestService:deleteReferenceGroupLinks() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of ReferenceGroupLink back to the user
			user.setReferenceGroupLinks( children );
			
			// save it 
			try
			{
				user = parentDelegate.saveUser( user );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "UserRestService:deleteReferenceGroupLinks() failed to save the User - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return user;
	}



    protected User loadHelper( UserPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "User.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of User
	            this.user = UserBusinessDelegate.getUserInstance().getUser( pk );
	            
	            LOGGER.info( "UserRestService:loadHelper() - successfully loaded - " + this.user.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "UserRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "UserRestService:load() - failed to load User using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return user;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveReferenceProviders":
	        	returnVal = saveReferenceProviders().getReferenceProviders();
	        	break;
	        case "deleteReferenceProviders":
	        	returnVal = deleteReferenceProviders().getReferenceProviders();
	        	break;
	        case "loadReferenceProviders" :
	        	returnVal = load().getReferenceProviders();
	        	break;
	        case "saveRefererGroups":
	        	returnVal = saveRefererGroups().getRefererGroups();
	        	break;
	        case "deleteRefererGroups":
	        	returnVal = deleteRefererGroups().getRefererGroups();
	        	break;
	        case "loadRefererGroups" :
	        	returnVal = load().getRefererGroups();
	        	break;
	        case "saveReferenceReceivers":
	        	returnVal = saveReferenceReceivers().getReferenceReceivers();
	        	break;
	        case "deleteReferenceReceivers":
	        	returnVal = deleteReferenceReceivers().getReferenceReceivers();
	        	break;
	        case "loadReferenceReceivers" :
	        	returnVal = load().getReferenceReceivers();
	        	break;
	        case "saveReferenceGroupLinks":
	        	returnVal = saveReferenceGroupLinks().getReferenceGroupLinks();
	        	break;
	        case "deleteReferenceGroupLinks":
	        	returnVal = deleteReferenceGroupLinks().getReferenceGroupLinks();
	        	break;
	        case "loadReferenceGroupLinks" :
	        	returnVal = load().getReferenceGroupLinks();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("User.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a User. Found in the request body.
	 * 
	 * @return User
	 */
	private User getUser()
	{
		if ( user == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				user = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), User.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "UserRestService.getUser() - failed to Json map from String to User - " + exc.getMessage() );
	        }			
		}
		return( user );
	}
	
	protected String getSubclassName()
	{ return( "UserRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private User user 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
