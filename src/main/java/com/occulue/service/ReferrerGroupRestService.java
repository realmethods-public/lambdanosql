/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity ReferrerGroup.
 *
 * @author dev@realmethods.com
 */

public class ReferrerGroupRestService extends BaseRestService
{

	public ReferrerGroupRestService()
	{}
	
    /**
     * Handles saving a ReferrerGroup BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected ReferrerGroup save()
    	throws ProcessingException
    {
		// doing it here helps
		getReferrerGroup();

		LOGGER.info( "ReferrerGroup.save() on - " + referrerGroup );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the referrerGroup is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referrerGroup != null && referrerGroup.getReferrerGroupPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a ReferrerGroup BO
     * @return		ReferrerGroup
     * @exception	ProcessingException
     */    
    protected ReferrerGroup update()
    	throws ProcessingException
    {
    	// store provided data
        ReferrerGroup tmp = referrerGroup;

        // load actual data from storage
        loadHelper( referrerGroup.getReferrerGroupPrimaryKey() );
    	
    	// copy provided data into actual data
    	referrerGroup.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferrerGroupBusiness Delegate            
			ReferrerGroupBusinessDelegate delegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
            this.referrerGroup = delegate.saveReferrerGroup( referrerGroup );
            
            if ( this.referrerGroup != null )
            	LOGGER.info( "ReferrerGroupRestService:update() - successfully updated ReferrerGroup - " + referrerGroup.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferrerGroupRestService:update() - successfully update ReferrerGroup - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referrerGroup;
        
    }

    /**
     * Handles creating a ReferrerGroup BO
     * @return		ReferrerGroup
     */
    protected ReferrerGroup create()
    	throws ProcessingException
    {
        try
        {       
        	referrerGroup 		= getReferrerGroup();
			this.referrerGroup 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance().createReferrerGroup( referrerGroup );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferrerGroupRestService:create() - exception ReferrerGroup - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referrerGroup;
    }


    
    /**
     * Handles deleting a ReferrerGroup BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	ReferrerGroupBusinessDelegate delegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long referrerGroupId  = parseId( "referrerGroupId" );
        		delegate.delete( new ReferrerGroupPrimaryKey( referrerGroupId  ) );
        		LOGGER.info( "ReferrerGroupRestService:delete() - successfully deleted ReferrerGroup with key " + referrerGroup.getReferrerGroupPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferrerGroupPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "ReferrerGroupRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "ReferrerGroupRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a ReferrerGroup BO
     * @param		Long referrerGroupId
     * @exception	ProcessingException
     * @return		ReferrerGroup
     */    
    protected ReferrerGroup load() 
    	throws ProcessingException
    {    	
        ReferrerGroupPrimaryKey pk 	= null;
		Long referrerGroupId  = parseId( "referrerGroupId" );

    	try
        {
    		LOGGER.info( "ReferrerGroup.load pk is " + referrerGroupId );
    		
        	if ( referrerGroupId != null )
        	{
        		pk = new ReferrerGroupPrimaryKey( referrerGroupId );

        		loadHelper( pk );

        		// load the contained instance of ReferrerGroup
	            this.referrerGroup = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getReferrerGroup( pk );
	            
	            LOGGER.info( "ReferrerGroupRestService:load() - successfully loaded - " + this.referrerGroup.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferrerGroupRestService:load() - unable to locate the primary key as an attribute or a selection for - " + referrerGroup.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerGroupRestService:load() - failed to load ReferrerGroup using Id " + referrerGroupId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referrerGroup;

    }

    /**
     * Handles loading all ReferrerGroup business objects
     * @return		List<ReferrerGroup>
     * @exception	ProcessingException
     */
    protected List<ReferrerGroup> loadAll()
    	throws ProcessingException
    {                
        List<ReferrerGroup> referrerGroupList = null;
        
    	try
        {                        
            // load the ReferrerGroup
            referrerGroupList = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getAllReferrerGroup();
            
            if ( referrerGroupList != null )
            	LOGGER.info(  "ReferrerGroupRestService:loadAllReferrerGroup() - successfully loaded all ReferrerGroups" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerGroupRestService:loadAll() - failed to load all ReferrerGroups - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return referrerGroupList;
                            
    }




    /**
     * save References on ReferrerGroup
     * @param		Long referrerGroupId
     * @param		Long childId
     * @param		String[] childIds
     * @return		ReferrerGroup
     * @exception	ProcessingException
     */     
	protected ReferrerGroup saveReferences()
		throws ProcessingException
	{
		Long referrerGroupId = parseId( "referrerGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferrerGroupPrimaryKey( referrerGroupId ) ) == null )
			throw new ProcessingException( "ReferrerGroup.saveReferences() - failed to load parent using Id " + referrerGroupId );
		 
		TheReferencePrimaryKey pk 					= null;
		TheReference child							= null;
		List<TheReference> childList				= null;
		TheReferenceBusinessDelegate childDelegate 	= TheReferenceBusinessDelegate.getTheReferenceInstance();
		ReferrerGroupBusinessDelegate parentDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new TheReferencePrimaryKey( childId );
			
			try
			{
				// find the TheReference
				child = childDelegate.getTheReference( pk );
				LOGGER.info( "LeagueRestService:saveReferrerGroup() - found TheReference" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferrerGroupRestService:saveReferences() failed get child TheReference using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the References, check for null
			if ( referrerGroup.getReferences() != null )
				referrerGroup.getReferences().add( child );

			LOGGER.info( "LeagueRestService:saveReferrerGroup() - added TheReference to parent" );

		}
		else
		{
			// clear or create the References
			if ( referrerGroup.getReferences() != null )
				referrerGroup.getReferences().clear();
			else
				referrerGroup.setReferences( new HashSet<TheReference>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new TheReferencePrimaryKey( id );
					try
					{
						// find the TheReference
						child = childDelegate.getTheReference( pk );
						// add it to the References List
						referrerGroup.getReferences().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "ReferrerGroupRestService:saveReferences() failed get child TheReference using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the ReferrerGroup
			parentDelegate.saveReferrerGroup( referrerGroup );
			LOGGER.info( "LeagueRestService:saveReferrerGroup() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "ReferrerGroupRestService:saveReferences() failed saving parent ReferrerGroup - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return referrerGroup;
	}

    /**
     * delete References on ReferrerGroup
     * @return		ReferrerGroup
     * @exception	ProcessingException
     */     	
	protected ReferrerGroup deleteReferences()
		throws ProcessingException
	{		
		Long referrerGroupId = parseId( "referrerGroupId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferrerGroupPrimaryKey( referrerGroupId ) ) == null )
			throw new ProcessingException( "ReferrerGroup.deleteReferences() - failed to load using Id " + referrerGroupId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			TheReferencePrimaryKey pk 					= null;
			TheReferenceBusinessDelegate childDelegate 	= TheReferenceBusinessDelegate.getTheReferenceInstance();
			ReferrerGroupBusinessDelegate parentDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			Set<TheReference> children					= referrerGroup.getReferences();
			TheReference child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new TheReferencePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getTheReference( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "ReferrerGroupRestService:deleteReferences() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of TheReference back to the referrerGroup
			referrerGroup.setReferences( children );
			
			// save it 
			try
			{
				referrerGroup = parentDelegate.saveReferrerGroup( referrerGroup );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "ReferrerGroupRestService:deleteReferences() failed to save the ReferrerGroup - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referrerGroup;
	}



    protected ReferrerGroup loadHelper( ReferrerGroupPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "ReferrerGroup.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of ReferrerGroup
	            this.referrerGroup = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getReferrerGroup( pk );
	            
	            LOGGER.info( "ReferrerGroupRestService:loadHelper() - successfully loaded - " + this.referrerGroup.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferrerGroupRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerGroupRestService:load() - failed to load ReferrerGroup using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referrerGroup;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveReferences":
	        	returnVal = saveReferences().getReferences();
	        	break;
	        case "deleteReferences":
	        	returnVal = deleteReferences().getReferences();
	        	break;
	        case "loadReferences" :
	        	returnVal = load().getReferences();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("ReferrerGroup.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a ReferrerGroup. Found in the request body.
	 * 
	 * @return ReferrerGroup
	 */
	private ReferrerGroup getReferrerGroup()
	{
		if ( referrerGroup == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				referrerGroup = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), ReferrerGroup.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "ReferrerGroupRestService.getReferrerGroup() - failed to Json map from String to ReferrerGroup - " + exc.getMessage() );
	        }			
		}
		return( referrerGroup );
	}
	
	protected String getSubclassName()
	{ return( "ReferrerGroupRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private ReferrerGroup referrerGroup 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
