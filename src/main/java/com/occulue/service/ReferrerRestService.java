/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Referrer.
 *
 * @author dev@realmethods.com
 */

public class ReferrerRestService extends BaseRestService
{

	public ReferrerRestService()
	{}
	
    /**
     * Handles saving a Referrer BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Referrer save()
    	throws ProcessingException
    {
		// doing it here helps
		getReferrer();

		LOGGER.info( "Referrer.save() on - " + referrer );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the referrer is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referrer != null && referrer.getReferrerPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Referrer BO
     * @return		Referrer
     * @exception	ProcessingException
     */    
    protected Referrer update()
    	throws ProcessingException
    {
    	// store provided data
        Referrer tmp = referrer;

        // load actual data from storage
        loadHelper( referrer.getReferrerPrimaryKey() );
    	
    	// copy provided data into actual data
    	referrer.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferrerBusiness Delegate            
			ReferrerBusinessDelegate delegate = ReferrerBusinessDelegate.getReferrerInstance();
            this.referrer = delegate.saveReferrer( referrer );
            
            if ( this.referrer != null )
            	LOGGER.info( "ReferrerRestService:update() - successfully updated Referrer - " + referrer.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferrerRestService:update() - successfully update Referrer - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referrer;
        
    }

    /**
     * Handles creating a Referrer BO
     * @return		Referrer
     */
    protected Referrer create()
    	throws ProcessingException
    {
        try
        {       
        	referrer 		= getReferrer();
			this.referrer 	= ReferrerBusinessDelegate.getReferrerInstance().createReferrer( referrer );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferrerRestService:create() - exception Referrer - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referrer;
    }


    
    /**
     * Handles deleting a Referrer BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	ReferrerBusinessDelegate delegate = ReferrerBusinessDelegate.getReferrerInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long referrerId  = parseId( "referrerId" );
        		delegate.delete( new ReferrerPrimaryKey( referrerId  ) );
        		LOGGER.info( "ReferrerRestService:delete() - successfully deleted Referrer with key " + referrer.getReferrerPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferrerPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "ReferrerRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "ReferrerRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Referrer BO
     * @param		Long referrerId
     * @exception	ProcessingException
     * @return		Referrer
     */    
    protected Referrer load() 
    	throws ProcessingException
    {    	
        ReferrerPrimaryKey pk 	= null;
		Long referrerId  = parseId( "referrerId" );

    	try
        {
    		LOGGER.info( "Referrer.load pk is " + referrerId );
    		
        	if ( referrerId != null )
        	{
        		pk = new ReferrerPrimaryKey( referrerId );

        		loadHelper( pk );

        		// load the contained instance of Referrer
	            this.referrer = ReferrerBusinessDelegate.getReferrerInstance().getReferrer( pk );
	            
	            LOGGER.info( "ReferrerRestService:load() - successfully loaded - " + this.referrer.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferrerRestService:load() - unable to locate the primary key as an attribute or a selection for - " + referrer.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerRestService:load() - failed to load Referrer using Id " + referrerId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referrer;

    }

    /**
     * Handles loading all Referrer business objects
     * @return		List<Referrer>
     * @exception	ProcessingException
     */
    protected List<Referrer> loadAll()
    	throws ProcessingException
    {                
        List<Referrer> referrerList = null;
        
    	try
        {                        
            // load the Referrer
            referrerList = ReferrerBusinessDelegate.getReferrerInstance().getAllReferrer();
            
            if ( referrerList != null )
            	LOGGER.info(  "ReferrerRestService:loadAllReferrer() - successfully loaded all Referrers" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerRestService:loadAll() - failed to load all Referrers - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return referrerList;
                            
    }




    /**
     * save Comments on Referrer
     * @param		Long referrerId
     * @param		Long childId
     * @param		String[] childIds
     * @return		Referrer
     * @exception	ProcessingException
     */     
	protected Referrer saveComments()
		throws ProcessingException
	{
		Long referrerId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferrerPrimaryKey( referrerId ) ) == null )
			throw new ProcessingException( "Referrer.saveComments() - failed to load parent using Id " + referrerId );
		 
		CommentPrimaryKey pk 					= null;
		Comment child							= null;
		List<Comment> childList				= null;
		CommentBusinessDelegate childDelegate 	= CommentBusinessDelegate.getCommentInstance();
		ReferrerBusinessDelegate parentDelegate = ReferrerBusinessDelegate.getReferrerInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new CommentPrimaryKey( childId );
			
			try
			{
				// find the Comment
				child = childDelegate.getComment( pk );
				LOGGER.info( "LeagueRestService:saveReferrer() - found Comment" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferrerRestService:saveComments() failed get child Comment using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Comments, check for null
			if ( referrer.getComments() != null )
				referrer.getComments().add( child );

			LOGGER.info( "LeagueRestService:saveReferrer() - added Comment to parent" );

		}
		else
		{
			// clear or create the Comments
			if ( referrer.getComments() != null )
				referrer.getComments().clear();
			else
				referrer.setComments( new HashSet<Comment>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new CommentPrimaryKey( id );
					try
					{
						// find the Comment
						child = childDelegate.getComment( pk );
						// add it to the Comments List
						referrer.getComments().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "ReferrerRestService:saveComments() failed get child Comment using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the Referrer
			parentDelegate.saveReferrer( referrer );
			LOGGER.info( "LeagueRestService:saveReferrer() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "ReferrerRestService:saveComments() failed saving parent Referrer - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return referrer;
	}

    /**
     * delete Comments on Referrer
     * @return		Referrer
     * @exception	ProcessingException
     */     	
	protected Referrer deleteComments()
		throws ProcessingException
	{		
		Long referrerId = parseId( "referrerId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new ReferrerPrimaryKey( referrerId ) ) == null )
			throw new ProcessingException( "Referrer.deleteComments() - failed to load using Id " + referrerId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			CommentPrimaryKey pk 					= null;
			CommentBusinessDelegate childDelegate 	= CommentBusinessDelegate.getCommentInstance();
			ReferrerBusinessDelegate parentDelegate = ReferrerBusinessDelegate.getReferrerInstance();
			Set<Comment> children					= referrer.getComments();
			Comment child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new CommentPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getComment( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "ReferrerRestService:deleteComments() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of Comment back to the referrer
			referrer.setComments( children );
			
			// save it 
			try
			{
				referrer = parentDelegate.saveReferrer( referrer );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "ReferrerRestService:deleteComments() failed to save the Referrer - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referrer;
	}



    protected Referrer loadHelper( ReferrerPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Referrer.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Referrer
	            this.referrer = ReferrerBusinessDelegate.getReferrerInstance().getReferrer( pk );
	            
	            LOGGER.info( "ReferrerRestService:loadHelper() - successfully loaded - " + this.referrer.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferrerRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferrerRestService:load() - failed to load Referrer using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referrer;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveComments":
	        	returnVal = saveComments().getComments();
	        	break;
	        case "deleteComments":
	        	returnVal = deleteComments().getComments();
	        	break;
	        case "loadComments" :
	        	returnVal = load().getComments();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("Referrer.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Referrer. Found in the request body.
	 * 
	 * @return Referrer
	 */
	private Referrer getReferrer()
	{
		if ( referrer == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				referrer = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Referrer.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "ReferrerRestService.getReferrer() - failed to Json map from String to Referrer - " + exc.getMessage() );
	        }			
		}
		return( referrer );
	}
	
	protected String getSubclassName()
	{ return( "ReferrerRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Referrer referrer 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
