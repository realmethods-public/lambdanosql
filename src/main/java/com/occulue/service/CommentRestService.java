/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Comment.
 *
 * @author dev@realmethods.com
 */

public class CommentRestService extends BaseRestService
{

	public CommentRestService()
	{}
	
    /**
     * Handles saving a Comment BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Comment save()
    	throws ProcessingException
    {
		// doing it here helps
		getComment();

		LOGGER.info( "Comment.save() on - " + comment );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the comment is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( comment != null && comment.getCommentPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Comment BO
     * @return		Comment
     * @exception	ProcessingException
     */    
    protected Comment update()
    	throws ProcessingException
    {
    	// store provided data
        Comment tmp = comment;

        // load actual data from storage
        loadHelper( comment.getCommentPrimaryKey() );
    	
    	// copy provided data into actual data
    	comment.copyShallow( tmp );
    	
        try
        {                        	        
			// create the CommentBusiness Delegate            
			CommentBusinessDelegate delegate = CommentBusinessDelegate.getCommentInstance();
            this.comment = delegate.saveComment( comment );
            
            if ( this.comment != null )
            	LOGGER.info( "CommentRestService:update() - successfully updated Comment - " + comment.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "CommentRestService:update() - successfully update Comment - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.comment;
        
    }

    /**
     * Handles creating a Comment BO
     * @return		Comment
     */
    protected Comment create()
    	throws ProcessingException
    {
        try
        {       
        	comment 		= getComment();
			this.comment 	= CommentBusinessDelegate.getCommentInstance().createComment( comment );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "CommentRestService:create() - exception Comment - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.comment;
    }


    
    /**
     * Handles deleting a Comment BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	CommentBusinessDelegate delegate = CommentBusinessDelegate.getCommentInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long commentId  = parseId( "commentId" );
        		delegate.delete( new CommentPrimaryKey( commentId  ) );
        		LOGGER.info( "CommentRestService:delete() - successfully deleted Comment with key " + comment.getCommentPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new CommentPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "CommentRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "CommentRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Comment BO
     * @param		Long commentId
     * @exception	ProcessingException
     * @return		Comment
     */    
    protected Comment load() 
    	throws ProcessingException
    {    	
        CommentPrimaryKey pk 	= null;
		Long commentId  = parseId( "commentId" );

    	try
        {
    		LOGGER.info( "Comment.load pk is " + commentId );
    		
        	if ( commentId != null )
        	{
        		pk = new CommentPrimaryKey( commentId );

        		loadHelper( pk );

        		// load the contained instance of Comment
	            this.comment = CommentBusinessDelegate.getCommentInstance().getComment( pk );
	            
	            LOGGER.info( "CommentRestService:load() - successfully loaded - " + this.comment.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "CommentRestService:load() - unable to locate the primary key as an attribute or a selection for - " + comment.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "CommentRestService:load() - failed to load Comment using Id " + commentId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return comment;

    }

    /**
     * Handles loading all Comment business objects
     * @return		List<Comment>
     * @exception	ProcessingException
     */
    protected List<Comment> loadAll()
    	throws ProcessingException
    {                
        List<Comment> commentList = null;
        
    	try
        {                        
            // load the Comment
            commentList = CommentBusinessDelegate.getCommentInstance().getAllComment();
            
            if ( commentList != null )
            	LOGGER.info(  "CommentRestService:loadAllComment() - successfully loaded all Comments" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "CommentRestService:loadAll() - failed to load all Comments - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return commentList;
                            
    }



    /**
     * save Source on Comment
     * @param		Comment comment
     * @return		Comment
     * @exception	ProcessingException
     */     
	protected Comment saveSource()
		throws ProcessingException
	{
		Long commentId 	= parseId( "commentId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new CommentPrimaryKey( commentId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerBusinessDelegate childDelegate 	= ReferrerBusinessDelegate.getReferrerInstance();
			CommentBusinessDelegate parentDelegate = CommentBusinessDelegate.getCommentInstance();			
			Referrer child 							= null;

			try
			{
				child = childDelegate.getReferrer( new ReferrerPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "CommentRestService:saveSource() failed to get Referrer using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			comment.setSource( child );
		
			try
			{
				// save it
				parentDelegate.saveComment( comment );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "CommentRestService:saveSource() failed saving parent Comment - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return comment;
	}

    /**
     * delete Source on Comment
     * @return		Comment
     * @exception	ProcessingException
     */     
	protected Comment deleteSource()
		throws ProcessingException
	{
		Long commentId = parseId( "commentId" );
		
 		if ( loadHelper( new CommentPrimaryKey( commentId ) ) == null )
			return( null );

		if ( comment.getSource() != null )
		{
			ReferrerPrimaryKey pk = comment.getSource().getReferrerPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			comment.setSource( null );
			try
			{
				CommentBusinessDelegate parentDelegate = CommentBusinessDelegate.getCommentInstance();

				// save it
				comment = parentDelegate.saveComment( comment );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "CommentRestService:deleteSource() failed to save Comment - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerBusinessDelegate childDelegate = ReferrerBusinessDelegate.getReferrerInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "CommentRestService:deleteSource() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return comment;
	}
	



    protected Comment loadHelper( CommentPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Comment.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Comment
	            this.comment = CommentBusinessDelegate.getCommentInstance().getComment( pk );
	            
	            LOGGER.info( "CommentRestService:loadHelper() - successfully loaded - " + this.comment.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "CommentRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "CommentRestService:load() - failed to load Comment using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return comment;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
 	        case "saveSource":
	        	returnVal = saveSource().getSource();
	        	break;
	        case "deleteSource":
	        	returnVal = deleteSource().getSource();
	        	break;
	        case "loadSource" :
	        	returnVal = load().getSource();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("Comment.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Comment. Found in the request body.
	 * 
	 * @return Comment
	 */
	private Comment getComment()
	{
		if ( comment == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				comment = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Comment.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "CommentRestService.getComment() - failed to Json map from String to Comment - " + exc.getMessage() );
	        }			
		}
		return( comment );
	}
	
	protected String getSubclassName()
	{ return( "CommentRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Comment comment 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
