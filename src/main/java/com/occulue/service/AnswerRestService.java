/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Answer.
 *
 * @author dev@realmethods.com
 */

public class AnswerRestService extends BaseRestService
{

	public AnswerRestService()
	{}
	
    /**
     * Handles saving a Answer BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Answer save()
    	throws ProcessingException
    {
		// doing it here helps
		getAnswer();

		LOGGER.info( "Answer.save() on - " + answer );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the answer is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( answer != null && answer.getAnswerPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Answer BO
     * @return		Answer
     * @exception	ProcessingException
     */    
    protected Answer update()
    	throws ProcessingException
    {
    	// store provided data
        Answer tmp = answer;

        // load actual data from storage
        loadHelper( answer.getAnswerPrimaryKey() );
    	
    	// copy provided data into actual data
    	answer.copyShallow( tmp );
    	
        try
        {                        	        
			// create the AnswerBusiness Delegate            
			AnswerBusinessDelegate delegate = AnswerBusinessDelegate.getAnswerInstance();
            this.answer = delegate.saveAnswer( answer );
            
            if ( this.answer != null )
            	LOGGER.info( "AnswerRestService:update() - successfully updated Answer - " + answer.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "AnswerRestService:update() - successfully update Answer - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.answer;
        
    }

    /**
     * Handles creating a Answer BO
     * @return		Answer
     */
    protected Answer create()
    	throws ProcessingException
    {
        try
        {       
        	answer 		= getAnswer();
			this.answer 	= AnswerBusinessDelegate.getAnswerInstance().createAnswer( answer );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "AnswerRestService:create() - exception Answer - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.answer;
    }


    
    /**
     * Handles deleting a Answer BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	AnswerBusinessDelegate delegate = AnswerBusinessDelegate.getAnswerInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long answerId  = parseId( "answerId" );
        		delegate.delete( new AnswerPrimaryKey( answerId  ) );
        		LOGGER.info( "AnswerRestService:delete() - successfully deleted Answer with key " + answer.getAnswerPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new AnswerPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "AnswerRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "AnswerRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Answer BO
     * @param		Long answerId
     * @exception	ProcessingException
     * @return		Answer
     */    
    protected Answer load() 
    	throws ProcessingException
    {    	
        AnswerPrimaryKey pk 	= null;
		Long answerId  = parseId( "answerId" );

    	try
        {
    		LOGGER.info( "Answer.load pk is " + answerId );
    		
        	if ( answerId != null )
        	{
        		pk = new AnswerPrimaryKey( answerId );

        		loadHelper( pk );

        		// load the contained instance of Answer
	            this.answer = AnswerBusinessDelegate.getAnswerInstance().getAnswer( pk );
	            
	            LOGGER.info( "AnswerRestService:load() - successfully loaded - " + this.answer.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:load() - unable to locate the primary key as an attribute or a selection for - " + answer.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AnswerRestService:load() - failed to load Answer using Id " + answerId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return answer;

    }

    /**
     * Handles loading all Answer business objects
     * @return		List<Answer>
     * @exception	ProcessingException
     */
    protected List<Answer> loadAll()
    	throws ProcessingException
    {                
        List<Answer> answerList = null;
        
    	try
        {                        
            // load the Answer
            answerList = AnswerBusinessDelegate.getAnswerInstance().getAllAnswer();
            
            if ( answerList != null )
            	LOGGER.info(  "AnswerRestService:loadAllAnswer() - successfully loaded all Answers" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AnswerRestService:loadAll() - failed to load all Answers - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return answerList;
                            
    }



    /**
     * save Question on Answer
     * @param		Answer answer
     * @return		Answer
     * @exception	ProcessingException
     */     
	protected Answer saveQuestion()
		throws ProcessingException
	{
		Long answerId 	= parseId( "answerId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new AnswerPrimaryKey( answerId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			QuestionBusinessDelegate childDelegate 	= QuestionBusinessDelegate.getQuestionInstance();
			AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();			
			Question child 							= null;

			try
			{
				child = childDelegate.getQuestion( new QuestionPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "AnswerRestService:saveQuestion() failed to get Question using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			answer.setQuestion( child );
		
			try
			{
				// save it
				parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:saveQuestion() failed saving parent Answer - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return answer;
	}

    /**
     * delete Question on Answer
     * @return		Answer
     * @exception	ProcessingException
     */     
	protected Answer deleteQuestion()
		throws ProcessingException
	{
		Long answerId = parseId( "answerId" );
		
 		if ( loadHelper( new AnswerPrimaryKey( answerId ) ) == null )
			return( null );

		if ( answer.getQuestion() != null )
		{
			QuestionPrimaryKey pk = answer.getQuestion().getQuestionPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			answer.setQuestion( null );
			try
			{
				AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();

				// save it
				answer = parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:deleteQuestion() failed to save Answer - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				QuestionBusinessDelegate childDelegate = QuestionBusinessDelegate.getQuestionInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:deleteQuestion() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return answer;
	}
	
    /**
     * save Response on Answer
     * @param		Answer answer
     * @return		Answer
     * @exception	ProcessingException
     */     
	protected Answer saveResponse()
		throws ProcessingException
	{
		Long answerId 	= parseId( "answerId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new AnswerPrimaryKey( answerId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			TheResponseBusinessDelegate childDelegate 	= TheResponseBusinessDelegate.getTheResponseInstance();
			AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();			
			TheResponse child 							= null;

			try
			{
				child = childDelegate.getTheResponse( new TheResponsePrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "AnswerRestService:saveResponse() failed to get TheResponse using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			answer.setResponse( child );
		
			try
			{
				// save it
				parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:saveResponse() failed saving parent Answer - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return answer;
	}

    /**
     * delete Response on Answer
     * @return		Answer
     * @exception	ProcessingException
     */     
	protected Answer deleteResponse()
		throws ProcessingException
	{
		Long answerId = parseId( "answerId" );
		
 		if ( loadHelper( new AnswerPrimaryKey( answerId ) ) == null )
			return( null );

		if ( answer.getResponse() != null )
		{
			TheResponsePrimaryKey pk = answer.getResponse().getTheResponsePrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			answer.setResponse( null );
			try
			{
				AnswerBusinessDelegate parentDelegate = AnswerBusinessDelegate.getAnswerInstance();

				// save it
				answer = parentDelegate.saveAnswer( answer );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:deleteResponse() failed to save Answer - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				TheResponseBusinessDelegate childDelegate = TheResponseBusinessDelegate.getTheResponseInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:deleteResponse() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return answer;
	}
	



    protected Answer loadHelper( AnswerPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Answer.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Answer
	            this.answer = AnswerBusinessDelegate.getAnswerInstance().getAnswer( pk );
	            
	            LOGGER.info( "AnswerRestService:loadHelper() - successfully loaded - " + this.answer.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "AnswerRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AnswerRestService:load() - failed to load Answer using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return answer;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
 	        case "saveQuestion":
	        	returnVal = saveQuestion().getQuestion();
	        	break;
	        case "deleteQuestion":
	        	returnVal = deleteQuestion().getQuestion();
	        	break;
	        case "loadQuestion" :
	        	returnVal = load().getQuestion();
	        	break;
	        case "saveResponse":
	        	returnVal = saveResponse().getResponse();
	        	break;
	        case "deleteResponse":
	        	returnVal = deleteResponse().getResponse();
	        	break;
	        case "loadResponse" :
	        	returnVal = load().getResponse();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("Answer.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Answer. Found in the request body.
	 * 
	 * @return Answer
	 */
	private Answer getAnswer()
	{
		if ( answer == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				answer = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Answer.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "AnswerRestService.getAnswer() - failed to Json map from String to Answer - " + exc.getMessage() );
	        }			
		}
		return( answer );
	}
	
	protected String getSubclassName()
	{ return( "AnswerRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Answer answer 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
