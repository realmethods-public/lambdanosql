/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity ReferenceGroupLink.
 *
 * @author dev@realmethods.com
 */

public class ReferenceGroupLinkRestService extends BaseRestService
{

	public ReferenceGroupLinkRestService()
	{}
	
    /**
     * Handles saving a ReferenceGroupLink BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected ReferenceGroupLink save()
    	throws ProcessingException
    {
		// doing it here helps
		getReferenceGroupLink();

		LOGGER.info( "ReferenceGroupLink.save() on - " + referenceGroupLink );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the referenceGroupLink is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( referenceGroupLink != null && referenceGroupLink.getReferenceGroupLinkPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a ReferenceGroupLink BO
     * @return		ReferenceGroupLink
     * @exception	ProcessingException
     */    
    protected ReferenceGroupLink update()
    	throws ProcessingException
    {
    	// store provided data
        ReferenceGroupLink tmp = referenceGroupLink;

        // load actual data from storage
        loadHelper( referenceGroupLink.getReferenceGroupLinkPrimaryKey() );
    	
    	// copy provided data into actual data
    	referenceGroupLink.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ReferenceGroupLinkBusiness Delegate            
			ReferenceGroupLinkBusinessDelegate delegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();
            this.referenceGroupLink = delegate.saveReferenceGroupLink( referenceGroupLink );
            
            if ( this.referenceGroupLink != null )
            	LOGGER.info( "ReferenceGroupLinkRestService:update() - successfully updated ReferenceGroupLink - " + referenceGroupLink.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferenceGroupLinkRestService:update() - successfully update ReferenceGroupLink - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referenceGroupLink;
        
    }

    /**
     * Handles creating a ReferenceGroupLink BO
     * @return		ReferenceGroupLink
     */
    protected ReferenceGroupLink create()
    	throws ProcessingException
    {
        try
        {       
        	referenceGroupLink 		= getReferenceGroupLink();
			this.referenceGroupLink 	= ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().createReferenceGroupLink( referenceGroupLink );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ReferenceGroupLinkRestService:create() - exception ReferenceGroupLink - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.referenceGroupLink;
    }


    
    /**
     * Handles deleting a ReferenceGroupLink BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	ReferenceGroupLinkBusinessDelegate delegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long referenceGroupLinkId  = parseId( "referenceGroupLinkId" );
        		delegate.delete( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId  ) );
        		LOGGER.info( "ReferenceGroupLinkRestService:delete() - successfully deleted ReferenceGroupLink with key " + referenceGroupLink.getReferenceGroupLinkPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ReferenceGroupLinkPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "ReferenceGroupLinkRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "ReferenceGroupLinkRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a ReferenceGroupLink BO
     * @param		Long referenceGroupLinkId
     * @exception	ProcessingException
     * @return		ReferenceGroupLink
     */    
    protected ReferenceGroupLink load() 
    	throws ProcessingException
    {    	
        ReferenceGroupLinkPrimaryKey pk 	= null;
		Long referenceGroupLinkId  = parseId( "referenceGroupLinkId" );

    	try
        {
    		LOGGER.info( "ReferenceGroupLink.load pk is " + referenceGroupLinkId );
    		
        	if ( referenceGroupLinkId != null )
        	{
        		pk = new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId );

        		loadHelper( pk );

        		// load the contained instance of ReferenceGroupLink
	            this.referenceGroupLink = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getReferenceGroupLink( pk );
	            
	            LOGGER.info( "ReferenceGroupLinkRestService:load() - successfully loaded - " + this.referenceGroupLink.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:load() - unable to locate the primary key as an attribute or a selection for - " + referenceGroupLink.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGroupLinkRestService:load() - failed to load ReferenceGroupLink using Id " + referenceGroupLinkId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referenceGroupLink;

    }

    /**
     * Handles loading all ReferenceGroupLink business objects
     * @return		List<ReferenceGroupLink>
     * @exception	ProcessingException
     */
    protected List<ReferenceGroupLink> loadAll()
    	throws ProcessingException
    {                
        List<ReferenceGroupLink> referenceGroupLinkList = null;
        
    	try
        {                        
            // load the ReferenceGroupLink
            referenceGroupLinkList = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getAllReferenceGroupLink();
            
            if ( referenceGroupLinkList != null )
            	LOGGER.info(  "ReferenceGroupLinkRestService:loadAllReferenceGroupLink() - successfully loaded all ReferenceGroupLinks" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGroupLinkRestService:loadAll() - failed to load all ReferenceGroupLinks - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return referenceGroupLinkList;
                            
    }



    /**
     * save ReferrerGroup on ReferenceGroupLink
     * @param		ReferenceGroupLink referenceGroupLink
     * @return		ReferenceGroupLink
     * @exception	ProcessingException
     */     
	protected ReferenceGroupLink saveReferrerGroup()
		throws ProcessingException
	{
		Long referenceGroupLinkId 	= parseId( "referenceGroupLinkId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			ReferrerGroupBusinessDelegate childDelegate 	= ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
			ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();			
			ReferrerGroup child 							= null;

			try
			{
				child = childDelegate.getReferrerGroup( new ReferrerGroupPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGroupLinkRestService:saveReferrerGroup() failed to get ReferrerGroup using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGroupLink.setReferrerGroup( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:saveReferrerGroup() failed saving parent ReferenceGroupLink - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGroupLink;
	}

    /**
     * delete ReferrerGroup on ReferenceGroupLink
     * @return		ReferenceGroupLink
     * @exception	ProcessingException
     */     
	protected ReferenceGroupLink deleteReferrerGroup()
		throws ProcessingException
	{
		Long referenceGroupLinkId = parseId( "referenceGroupLinkId" );
		
 		if ( loadHelper( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId ) ) == null )
			return( null );

		if ( referenceGroupLink.getReferrerGroup() != null )
		{
			ReferrerGroupPrimaryKey pk = referenceGroupLink.getReferrerGroup().getReferrerGroupPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGroupLink.setReferrerGroup( null );
			try
			{
				ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();

				// save it
				referenceGroupLink = parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:deleteReferrerGroup() failed to save ReferenceGroupLink - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				ReferrerGroupBusinessDelegate childDelegate = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:deleteReferrerGroup() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGroupLink;
	}
	
    /**
     * save LinkProvider on ReferenceGroupLink
     * @param		ReferenceGroupLink referenceGroupLink
     * @return		ReferenceGroupLink
     * @exception	ProcessingException
     */     
	protected ReferenceGroupLink saveLinkProvider()
		throws ProcessingException
	{
		Long referenceGroupLinkId 	= parseId( "referenceGroupLinkId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ReferenceGroupLinkRestService:saveLinkProvider() failed to get User using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			referenceGroupLink.setLinkProvider( child );
		
			try
			{
				// save it
				parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:saveLinkProvider() failed saving parent ReferenceGroupLink - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGroupLink;
	}

    /**
     * delete LinkProvider on ReferenceGroupLink
     * @return		ReferenceGroupLink
     * @exception	ProcessingException
     */     
	protected ReferenceGroupLink deleteLinkProvider()
		throws ProcessingException
	{
		Long referenceGroupLinkId = parseId( "referenceGroupLinkId" );
		
 		if ( loadHelper( new ReferenceGroupLinkPrimaryKey( referenceGroupLinkId ) ) == null )
			return( null );

		if ( referenceGroupLink.getLinkProvider() != null )
		{
			UserPrimaryKey pk = referenceGroupLink.getLinkProvider().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			referenceGroupLink.setLinkProvider( null );
			try
			{
				ReferenceGroupLinkBusinessDelegate parentDelegate = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance();

				// save it
				referenceGroupLink = parentDelegate.saveReferenceGroupLink( referenceGroupLink );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:deleteLinkProvider() failed to save ReferenceGroupLink - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:deleteLinkProvider() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return referenceGroupLink;
	}
	



    protected ReferenceGroupLink loadHelper( ReferenceGroupLinkPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "ReferenceGroupLink.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of ReferenceGroupLink
	            this.referenceGroupLink = ReferenceGroupLinkBusinessDelegate.getReferenceGroupLinkInstance().getReferenceGroupLink( pk );
	            
	            LOGGER.info( "ReferenceGroupLinkRestService:loadHelper() - successfully loaded - " + this.referenceGroupLink.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ReferenceGroupLinkRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ReferenceGroupLinkRestService:load() - failed to load ReferenceGroupLink using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return referenceGroupLink;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
 	        case "saveReferrerGroup":
	        	returnVal = saveReferrerGroup().getReferrerGroup();
	        	break;
	        case "deleteReferrerGroup":
	        	returnVal = deleteReferrerGroup().getReferrerGroup();
	        	break;
	        case "loadReferrerGroup" :
	        	returnVal = load().getReferrerGroup();
	        	break;
	        case "saveLinkProvider":
	        	returnVal = saveLinkProvider().getLinkProvider();
	        	break;
	        case "deleteLinkProvider":
	        	returnVal = deleteLinkProvider().getLinkProvider();
	        	break;
	        case "loadLinkProvider" :
	        	returnVal = load().getLinkProvider();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("ReferenceGroupLink.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a ReferenceGroupLink. Found in the request body.
	 * 
	 * @return ReferenceGroupLink
	 */
	private ReferenceGroupLink getReferenceGroupLink()
	{
		if ( referenceGroupLink == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				referenceGroupLink = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), ReferenceGroupLink.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "ReferenceGroupLinkRestService.getReferenceGroupLink() - failed to Json map from String to ReferenceGroupLink - " + exc.getMessage() );
	        }			
		}
		return( referenceGroupLink );
	}
	
	protected String getSubclassName()
	{ return( "ReferenceGroupLinkRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private ReferenceGroupLink referenceGroupLink 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
