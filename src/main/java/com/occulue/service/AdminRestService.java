/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Admin.
 *
 * @author dev@realmethods.com
 */

public class AdminRestService extends BaseRestService
{

	public AdminRestService()
	{}
	
    /**
     * Handles saving a Admin BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Admin save()
    	throws ProcessingException
    {
		// doing it here helps
		getAdmin();

		LOGGER.info( "Admin.save() on - " + admin );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the admin is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( admin != null && admin.getAdminPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Admin BO
     * @return		Admin
     * @exception	ProcessingException
     */    
    protected Admin update()
    	throws ProcessingException
    {
    	// store provided data
        Admin tmp = admin;

        // load actual data from storage
        loadHelper( admin.getAdminPrimaryKey() );
    	
    	// copy provided data into actual data
    	admin.copyShallow( tmp );
    	
        try
        {                        	        
			// create the AdminBusiness Delegate            
			AdminBusinessDelegate delegate = AdminBusinessDelegate.getAdminInstance();
            this.admin = delegate.saveAdmin( admin );
            
            if ( this.admin != null )
            	LOGGER.info( "AdminRestService:update() - successfully updated Admin - " + admin.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "AdminRestService:update() - successfully update Admin - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.admin;
        
    }

    /**
     * Handles creating a Admin BO
     * @return		Admin
     */
    protected Admin create()
    	throws ProcessingException
    {
        try
        {       
        	admin 		= getAdmin();
			this.admin 	= AdminBusinessDelegate.getAdminInstance().createAdmin( admin );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "AdminRestService:create() - exception Admin - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.admin;
    }


    
    /**
     * Handles deleting a Admin BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	AdminBusinessDelegate delegate = AdminBusinessDelegate.getAdminInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long adminId  = parseId( "adminId" );
        		delegate.delete( new AdminPrimaryKey( adminId  ) );
        		LOGGER.info( "AdminRestService:delete() - successfully deleted Admin with key " + admin.getAdminPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new AdminPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "AdminRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "AdminRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Admin BO
     * @param		Long adminId
     * @exception	ProcessingException
     * @return		Admin
     */    
    protected Admin load() 
    	throws ProcessingException
    {    	
        AdminPrimaryKey pk 	= null;
		Long adminId  = parseId( "adminId" );

    	try
        {
    		LOGGER.info( "Admin.load pk is " + adminId );
    		
        	if ( adminId != null )
        	{
        		pk = new AdminPrimaryKey( adminId );

        		loadHelper( pk );

        		// load the contained instance of Admin
	            this.admin = AdminBusinessDelegate.getAdminInstance().getAdmin( pk );
	            
	            LOGGER.info( "AdminRestService:load() - successfully loaded - " + this.admin.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "AdminRestService:load() - unable to locate the primary key as an attribute or a selection for - " + admin.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AdminRestService:load() - failed to load Admin using Id " + adminId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return admin;

    }

    /**
     * Handles loading all Admin business objects
     * @return		List<Admin>
     * @exception	ProcessingException
     */
    protected List<Admin> loadAll()
    	throws ProcessingException
    {                
        List<Admin> adminList = null;
        
    	try
        {                        
            // load the Admin
            adminList = AdminBusinessDelegate.getAdminInstance().getAllAdmin();
            
            if ( adminList != null )
            	LOGGER.info(  "AdminRestService:loadAllAdmin() - successfully loaded all Admins" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AdminRestService:loadAll() - failed to load all Admins - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return adminList;
                            
    }




    /**
     * save Users on Admin
     * @param		Long adminId
     * @param		Long childId
     * @param		String[] childIds
     * @return		Admin
     * @exception	ProcessingException
     */     
	protected Admin saveUsers()
		throws ProcessingException
	{
		Long adminId = parseId( "adminId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new AdminPrimaryKey( adminId ) ) == null )
			throw new ProcessingException( "Admin.saveUsers() - failed to load parent using Id " + adminId );
		 
		UserPrimaryKey pk 					= null;
		User child							= null;
		List<User> childList				= null;
		UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
		AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new UserPrimaryKey( childId );
			
			try
			{
				// find the User
				child = childDelegate.getUser( pk );
				LOGGER.info( "LeagueRestService:saveAdmin() - found User" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AdminRestService:saveUsers() failed get child User using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the Users, check for null
			if ( admin.getUsers() != null )
				admin.getUsers().add( child );

			LOGGER.info( "LeagueRestService:saveAdmin() - added User to parent" );

		}
		else
		{
			// clear or create the Users
			if ( admin.getUsers() != null )
				admin.getUsers().clear();
			else
				admin.setUsers( new HashSet<User>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new UserPrimaryKey( id );
					try
					{
						// find the User
						child = childDelegate.getUser( pk );
						// add it to the Users List
						admin.getUsers().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "AdminRestService:saveUsers() failed get child User using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the Admin
			parentDelegate.saveAdmin( admin );
			LOGGER.info( "LeagueRestService:saveAdmin() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "AdminRestService:saveUsers() failed saving parent Admin - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return admin;
	}

    /**
     * delete Users on Admin
     * @return		Admin
     * @exception	ProcessingException
     */     	
	protected Admin deleteUsers()
		throws ProcessingException
	{		
		Long adminId = parseId( "adminId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new AdminPrimaryKey( adminId ) ) == null )
			throw new ProcessingException( "Admin.deleteUsers() - failed to load using Id " + adminId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			UserPrimaryKey pk 					= null;
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();
			Set<User> children					= admin.getUsers();
			User child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new UserPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getUser( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "AdminRestService:deleteUsers() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of User back to the admin
			admin.setUsers( children );
			
			// save it 
			try
			{
				admin = parentDelegate.saveAdmin( admin );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "AdminRestService:deleteUsers() failed to save the Admin - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return admin;
	}

    /**
     * save ReferenceEngines on Admin
     * @param		Long adminId
     * @param		Long childId
     * @param		String[] childIds
     * @return		Admin
     * @exception	ProcessingException
     */     
	protected Admin saveReferenceEngines()
		throws ProcessingException
	{
		Long adminId = parseId( "adminId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new AdminPrimaryKey( adminId ) ) == null )
			throw new ProcessingException( "Admin.saveReferenceEngines() - failed to load parent using Id " + adminId );
		 
		ReferenceEnginePrimaryKey pk 					= null;
		ReferenceEngine child							= null;
		List<ReferenceEngine> childList				= null;
		ReferenceEngineBusinessDelegate childDelegate 	= ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
		AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();		
		
		Long[] childIds = getChildIds();
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferenceEnginePrimaryKey( childId );
			
			try
			{
				// find the ReferenceEngine
				child = childDelegate.getReferenceEngine( pk );
				LOGGER.info( "LeagueRestService:saveAdmin() - found ReferenceEngine" );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "AdminRestService:saveReferenceEngines() failed get child ReferenceEngine using id " + childId  + "- " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			// add it to the ReferenceEngines, check for null
			if ( admin.getReferenceEngines() != null )
				admin.getReferenceEngines().add( child );

			LOGGER.info( "LeagueRestService:saveAdmin() - added ReferenceEngine to parent" );

		}
		else
		{
			// clear or create the ReferenceEngines
			if ( admin.getReferenceEngines() != null )
				admin.getReferenceEngines().clear();
			else
				admin.setReferenceEngines( new HashSet<ReferenceEngine>() );
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferenceEnginePrimaryKey( id );
					try
					{
						// find the ReferenceEngine
						child = childDelegate.getReferenceEngine( pk );
						// add it to the ReferenceEngines List
						admin.getReferenceEngines().add( child );
					}
					catch( Exception exc )
					{
			        	signalBadRequest();

						String errMsg = "AdminRestService:saveReferenceEngines() failed get child ReferenceEngine using id " + id  + "- " + exc.getMessage();
						LOGGER.severe( errMsg );
						throw new ProcessingException( errMsg );
					}
				}
			}
		}

		try
		{
			// save the Admin
			parentDelegate.saveAdmin( admin );
			LOGGER.info( "LeagueRestService:saveAdmin() - saved successfully" );

		}
		catch( Exception exc )
		{
        	signalBadRequest();

			String errMsg = "AdminRestService:saveReferenceEngines() failed saving parent Admin - " + exc.getMessage();
			LOGGER.severe( errMsg );
			throw new ProcessingException( errMsg );
		}

		return admin;
	}

    /**
     * delete ReferenceEngines on Admin
     * @return		Admin
     * @exception	ProcessingException
     */     	
	protected Admin deleteReferenceEngines()
		throws ProcessingException
	{		
		Long adminId = parseId( "adminId" );
		Long childId = parseId( "childId" );
		
		if ( loadHelper( new AdminPrimaryKey( adminId ) ) == null )
			throw new ProcessingException( "Admin.deleteReferenceEngines() - failed to load using Id " + adminId );

		Long[] childIds = getChildIds();
		
		if ( childIds.length > 0 )
		{
			ReferenceEnginePrimaryKey pk 					= null;
			ReferenceEngineBusinessDelegate childDelegate 	= ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
			AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();
			Set<ReferenceEngine> children					= admin.getReferenceEngines();
			ReferenceEngine child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferenceEnginePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferenceEngine( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					signalBadRequest();
					String errMsg = "AdminRestService:deleteReferenceEngines() failed - " + exc.getMessage();
					LOGGER.severe( errMsg );
					throw new ProcessingException( errMsg );
				}
			}
			
			// assign the modified list of ReferenceEngine back to the admin
			admin.setReferenceEngines( children );
			
			// save it 
			try
			{
				admin = parentDelegate.saveAdmin( admin );
			}
			catch( Throwable exc )
			{
				signalBadRequest();
				
				String errMsg = "AdminRestService:deleteReferenceEngines() failed to save the Admin - " + exc.getMessage();				
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return admin;
	}



    protected Admin loadHelper( AdminPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Admin.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Admin
	            this.admin = AdminBusinessDelegate.getAdminInstance().getAdmin( pk );
	            
	            LOGGER.info( "AdminRestService:loadHelper() - successfully loaded - " + this.admin.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "AdminRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "AdminRestService:load() - failed to load Admin using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return admin;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
	        case "saveUsers":
	        	returnVal = saveUsers().getUsers();
	        	break;
	        case "deleteUsers":
	        	returnVal = deleteUsers().getUsers();
	        	break;
	        case "loadUsers" :
	        	returnVal = load().getUsers();
	        	break;
	        case "saveReferenceEngines":
	        	returnVal = saveReferenceEngines().getReferenceEngines();
	        	break;
	        case "deleteReferenceEngines":
	        	returnVal = deleteReferenceEngines().getReferenceEngines();
	        	break;
	        case "loadReferenceEngines" :
	        	returnVal = load().getReferenceEngines();
	        	break;
          default:
        	signalBadRequest();
            throw new ProcessingException("Admin.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Admin. Found in the request body.
	 * 
	 * @return Admin
	 */
	private Admin getAdmin()
	{
		if ( admin == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				admin = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Admin.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "AdminRestService.getAdmin() - failed to Json map from String to Admin - " + exc.getMessage() );
	        }			
		}
		return( admin );
	}
	
	protected String getSubclassName()
	{ return( "AdminRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Admin admin 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
