/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import java.text.SimpleDateFormat;
import java.util.logging.Logger;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import static spark.Spark.get;
import static spark.Spark.post;

import spark.Request;
import spark.Response;
import spark.Route;

import com.occulue.bo.Base;
import com.occulue.bo.*;
import com.occulue.common.JsonTransformer;
import com.occulue.delegate.*;
import com.occulue.primarykey.*;
import com.occulue.exception.ProcessingException;

/** 
 * Implements Struts action processing for business entity Activity.
 *
 * @author dev@realmethods.com
 */

public class ActivityRestService extends BaseRestService
{

	public ActivityRestService()
	{}
	
    /**
     * Handles saving a Activity BO.  if not key provided, calls create, otherwise calls save
     * @exception	ProcessingException
     */
    protected Activity save()
    	throws ProcessingException
    {
		// doing it here helps
		getActivity();

		LOGGER.info( "Activity.save() on - " + activity );
		
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Returns true if the activity is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( activity != null && activity.getActivityPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    /**
     * Handles updating a Activity BO
     * @return		Activity
     * @exception	ProcessingException
     */    
    protected Activity update()
    	throws ProcessingException
    {
    	// store provided data
        Activity tmp = activity;

        // load actual data from storage
        loadHelper( activity.getActivityPrimaryKey() );
    	
    	// copy provided data into actual data
    	activity.copyShallow( tmp );
    	
        try
        {                        	        
			// create the ActivityBusiness Delegate            
			ActivityBusinessDelegate delegate = ActivityBusinessDelegate.getActivityInstance();
            this.activity = delegate.saveActivity( activity );
            
            if ( this.activity != null )
            	LOGGER.info( "ActivityRestService:update() - successfully updated Activity - " + activity.toString() );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ActivityRestService:update() - successfully update Activity - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.activity;
        
    }

    /**
     * Handles creating a Activity BO
     * @return		Activity
     */
    protected Activity create()
    	throws ProcessingException
    {
        try
        {       
        	activity 		= getActivity();
			this.activity 	= ActivityBusinessDelegate.getActivityInstance().createActivity( activity );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	
        	String errMsg = "ActivityRestService:create() - exception Activity - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
        
        return this.activity;
    }


    
    /**
     * Handles deleting a Activity BO
     * @exception	ProcessingException
     */
    protected void delete() 
    	throws ProcessingException
    {                
        try
        {
        	ActivityBusinessDelegate delegate = ActivityBusinessDelegate.getActivityInstance();

        	Long[] childIds = getChildIds();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		Long activityId  = parseId( "activityId" );
        		delegate.delete( new ActivityPrimaryKey( activityId  ) );
        		LOGGER.info( "ActivityRestService:delete() - successfully deleted Activity with key " + activity.getActivityPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new ActivityPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	signalBadRequest();

	                	String errMsg = "ActivityRestService:delete() - " + exc.getMessage();
	                	LOGGER.severe( errMsg );
	                	throw new ProcessingException( errMsg );
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	signalBadRequest();
        	String errMsg = "ActivityRestService:delete() - " + exc.getMessage();
        	LOGGER.severe( errMsg );
        	throw new ProcessingException( errMsg );
        }
	}        
	
    /**
     * Handles loading a Activity BO
     * @param		Long activityId
     * @exception	ProcessingException
     * @return		Activity
     */    
    protected Activity load() 
    	throws ProcessingException
    {    	
        ActivityPrimaryKey pk 	= null;
		Long activityId  = parseId( "activityId" );

    	try
        {
    		LOGGER.info( "Activity.load pk is " + activityId );
    		
        	if ( activityId != null )
        	{
        		pk = new ActivityPrimaryKey( activityId );

        		loadHelper( pk );

        		// load the contained instance of Activity
	            this.activity = ActivityBusinessDelegate.getActivityInstance().getActivity( pk );
	            
	            LOGGER.info( "ActivityRestService:load() - successfully loaded - " + this.activity.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ActivityRestService:load() - unable to locate the primary key as an attribute or a selection for - " + activity.toString();				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ActivityRestService:load() - failed to load Activity using Id " + activityId + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return activity;

    }

    /**
     * Handles loading all Activity business objects
     * @return		List<Activity>
     * @exception	ProcessingException
     */
    protected List<Activity> loadAll()
    	throws ProcessingException
    {                
        List<Activity> activityList = null;
        
    	try
        {                        
            // load the Activity
            activityList = ActivityBusinessDelegate.getActivityInstance().getAllActivity();
            
            if ( activityList != null )
            	LOGGER.info(  "ActivityRestService:loadAllActivity() - successfully loaded all Activitys" );
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ActivityRestService:loadAll() - failed to load all Activitys - " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );            
        }

        return activityList;
                            
    }



    /**
     * save User on Activity
     * @param		Activity activity
     * @return		Activity
     * @exception	ProcessingException
     */     
	protected Activity saveUser()
		throws ProcessingException
	{
		Long activityId 	= parseId( "activityId" );
		Long childId 					= parseId( "childId" );
		
		if ( loadHelper( new ActivityPrimaryKey( activityId ) ) == null )
			return( null );
		
		if ( childId != null )
		{
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			ActivityBusinessDelegate parentDelegate = ActivityBusinessDelegate.getActivityInstance();			
			User child 							= null;

			try
			{
				child = childDelegate.getUser( new UserPrimaryKey( childId ) );
			}
            catch( Throwable exc )
            {
            	signalBadRequest();

            	String errMsg = "ActivityRestService:saveUser() failed to get User using id " + childId + " - " + exc.getMessage();
            	LOGGER.severe( errMsg);
            	throw new ProcessingException( errMsg );
            }
	
			activity.setUser( child );
		
			try
			{
				// save it
				parentDelegate.saveActivity( activity );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ActivityRestService:saveUser() failed saving parent Activity - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return activity;
	}

    /**
     * delete User on Activity
     * @return		Activity
     * @exception	ProcessingException
     */     
	protected Activity deleteUser()
		throws ProcessingException
	{
		Long activityId = parseId( "activityId" );
		
 		if ( loadHelper( new ActivityPrimaryKey( activityId ) ) == null )
			return( null );

		if ( activity.getUser() != null )
		{
			UserPrimaryKey pk = activity.getUser().getUserPrimaryKey();
			
			// null out the parent first so there's no constraint during deletion
			activity.setUser( null );
			try
			{
				ActivityBusinessDelegate parentDelegate = ActivityBusinessDelegate.getActivityInstance();

				// save it
				activity = parentDelegate.saveActivity( activity );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ActivityRestService:deleteUser() failed to save Activity - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
			
			try
			{
				// safe to delete the child			
				UserBusinessDelegate childDelegate = UserBusinessDelegate.getUserInstance();
				childDelegate.delete( pk );
			}
			catch( Exception exc )
			{
	        	signalBadRequest();

				String errMsg = "ActivityRestService:deleteUser() failed  - " + exc.getMessage();
				LOGGER.severe( errMsg );
				throw new ProcessingException( errMsg );
			}
		}
		
		return activity;
	}
	



    protected Activity loadHelper( ActivityPrimaryKey pk )
    		throws ProcessingException
    {
    	try
        {
    		LOGGER.info( "Activity.loadHelper primary key is " + pk);
    		
        	if ( pk != null )
        	{
        		// load the contained instance of Activity
	            this.activity = ActivityBusinessDelegate.getActivityInstance().getActivity( pk );
	            
	            LOGGER.info( "ActivityRestService:loadHelper() - successfully loaded - " + this.activity.toString() );             
			}
			else
			{
	        	signalBadRequest();

				String errMsg = "ActivityRestService:loadHelper() - null primary key provided.";				
				LOGGER.severe( errMsg );
	            throw new ProcessingException( errMsg );
			}	            
        }
        catch( Throwable exc )
        {
        	signalBadRequest();

        	String errMsg = "ActivityRestService:load() - failed to load Activity using pk " + pk + ", " + exc.getMessage();				
			LOGGER.severe( errMsg );
            throw new ProcessingException( errMsg );
        }

        return activity;

    }

    // overloads from BaseRestService
    
    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
	public Object handleExec( String action, spark.Response response, spark.Request request )
		throws ProcessingException
	{
		// store locally
		this.response = response;
		this.request = request;
				
		if ( action == null )
		{
			signalBadRequest();
			throw new ProcessingException();
		}

		Object returnVal = null;

		switch (action) {
	        case "save":
	        	returnVal = save();
	            break;
	        case "load":
	        	returnVal = load();
	            break;
	        case "delete":
	        	delete();
	            break;
	        case "loadAll":
	        case "viewAll":
	        	returnVal = loadAll();
	            break;
 	        case "saveUser":
	        	returnVal = saveUser().getUser();
	        	break;
	        case "deleteUser":
	        	returnVal = deleteUser().getUser();
	        	break;
	        case "loadUser" :
	        	returnVal = load().getUser();
	        	break;
         default:
        	signalBadRequest();
            throw new ProcessingException("Activity.execute(...) - unable to handle action " + action);
		}
		
		return returnVal;
	}
	
	/**
	 * Uses ObjectMapper to map from Json to a Activity. Found in the request body.
	 * 
	 * @return Activity
	 */
	private Activity getActivity()
	{
		if ( activity == null )
		{
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
				activity = mapper.readValue(java.net.URLDecoder.decode(request.queryString(),"UTF-8"), Activity.class);
				
	        } catch (Exception exc) 
	        {
	            signalBadRequest();
	            LOGGER.severe( "ActivityRestService.getActivity() - failed to Json map from String to Activity - " + exc.getMessage() );
	        }			
		}
		return( activity );
	}
	
	protected String getSubclassName()
	{ return( "ActivityRestService" ); }
	
//************************************************************************    
// Attributes
//************************************************************************
    private Activity activity 			= null;
    private static final Logger LOGGER 	= Logger.getLogger(BaseRestService.class.getName());
    
}
