/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test Admin class.
 *
 * @author    dev@realmethods.com
 */
public class AdminTest
{

// constructors

    public AdminTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Admin, through a AdminTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on AdminTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on AdminTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Admin.
     *
     * @return    Admin
     */
    public Admin testCreate()
    throws Throwable
    {
        Admin businessObject = null;

    	{
	        LOGGER.info( "AdminTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Admin");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Admin" );
	
	        try 
	        {            
	            businessObject = AdminBusinessDelegate.getAdminInstance().createAdmin( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (AdminPrimaryKey)businessObject.getAdminPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Admin with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Admin.
     *
     * @return    Admin  
     */
    public Admin testRead()
    throws Throwable
    {
        LOGGER.info( "AdminTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Admin" );

        Admin businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Admin with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = AdminBusinessDelegate.getAdminInstance().getAdmin( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Admin " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Admin.
     *
     * @return    Admin
     */
    public Admin testUpdate()
    throws Throwable
    {

        LOGGER.info( "AdminTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Admin." );

        StringBuilder msg = new StringBuilder( "Failed to update a Admin : " );        
        Admin businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Admin." );
            
            // for use later on...
            thePrimaryKey = (AdminPrimaryKey)businessObject.getAdminPrimaryKey();
            
            AdminBusinessDelegate proxy = AdminBusinessDelegate.getAdminInstance();            
            businessObject = proxy.saveAdmin( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Admin - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Admin.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "AdminTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Admin." );
        
        try
        {
            AdminBusinessDelegate.getAdminInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Admin with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Admin with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Admins.
     *
     * @return    Collection
     */
    public ArrayList<Admin> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "AdminTest:testGetAll() - Retrieving Collection of Admins:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Admin : " );        
        ArrayList<Admin> collection  = null;

        try
        {
            // call the static get method on the AdminBusinessDelegate
            collection = AdminBusinessDelegate.getAdminInstance().getAllAdmin();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Admin currentBO  = null;            
	            Iterator<Admin> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getAdminPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public AdminTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Admin
     * 
     * @return    Admin
     */    
    protected Admin getNewBO()
    {
        Admin newBO = new Admin();

// AIB : \#defaultBOOutput() 
newBO.setLoginId(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setPassword(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected AdminPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Admin.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
