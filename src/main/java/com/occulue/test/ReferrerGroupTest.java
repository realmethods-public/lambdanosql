/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test ReferrerGroup class.
 *
 * @author    dev@realmethods.com
 */
public class ReferrerGroupTest
{

// constructors

    public ReferrerGroupTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a ReferrerGroup, through a ReferrerGroupTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ReferrerGroupTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ReferrerGroupTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new ReferrerGroup.
     *
     * @return    ReferrerGroup
     */
    public ReferrerGroup testCreate()
    throws Throwable
    {
        ReferrerGroup businessObject = null;

    	{
	        LOGGER.info( "ReferrerGroupTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a ReferrerGroup");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a ReferrerGroup" );
	
	        try 
	        {            
	            businessObject = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().createReferrerGroup( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ReferrerGroupPrimaryKey)businessObject.getReferrerGroupPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a ReferrerGroup with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a ReferrerGroup.
     *
     * @return    ReferrerGroup  
     */
    public ReferrerGroup testRead()
    throws Throwable
    {
        LOGGER.info( "ReferrerGroupTest:testRead()" );
        LOGGER.info( "-- Reading a previously created ReferrerGroup" );

        ReferrerGroup businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read ReferrerGroup with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getReferrerGroup( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found ReferrerGroup " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a ReferrerGroup.
     *
     * @return    ReferrerGroup
     */
    public ReferrerGroup testUpdate()
    throws Throwable
    {

        LOGGER.info( "ReferrerGroupTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a ReferrerGroup." );

        StringBuilder msg = new StringBuilder( "Failed to update a ReferrerGroup : " );        
        ReferrerGroup businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created ReferrerGroup." );
            
            // for use later on...
            thePrimaryKey = (ReferrerGroupPrimaryKey)businessObject.getReferrerGroupPrimaryKey();
            
            ReferrerGroupBusinessDelegate proxy = ReferrerGroupBusinessDelegate.getReferrerGroupInstance();            
            businessObject = proxy.saveReferrerGroup( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved ReferrerGroup - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a ReferrerGroup.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ReferrerGroupTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created ReferrerGroup." );
        
        try
        {
            ReferrerGroupBusinessDelegate.getReferrerGroupInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted ReferrerGroup with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete ReferrerGroup with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all ReferrerGroups.
     *
     * @return    Collection
     */
    public ArrayList<ReferrerGroup> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ReferrerGroupTest:testGetAll() - Retrieving Collection of ReferrerGroups:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all ReferrerGroup : " );        
        ArrayList<ReferrerGroup> collection  = null;

        try
        {
            // call the static get method on the ReferrerGroupBusinessDelegate
            collection = ReferrerGroupBusinessDelegate.getReferrerGroupInstance().getAllReferrerGroup();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            ReferrerGroup currentBO  = null;            
	            Iterator<ReferrerGroup> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getReferrerGroupPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ReferrerGroupTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate ReferrerGroup
     * 
     * @return    ReferrerGroup
     */    
    protected ReferrerGroup getNewBO()
    {
        ReferrerGroup newBO = new ReferrerGroup();

// AIB : \#defaultBOOutput() 
newBO.setName(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setDateTimeLastViewedExternally(
    java.util.Calendar.getInstance().getTime()
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ReferrerGroupPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(ReferrerGroup.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
