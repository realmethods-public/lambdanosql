/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test TheReference class.
 *
 * @author    dev@realmethods.com
 */
public class TheReferenceTest
{

// constructors

    public TheReferenceTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a TheReference, through a TheReferenceTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on TheReferenceTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on TheReferenceTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new TheReference.
     *
     * @return    TheReference
     */
    public TheReference testCreate()
    throws Throwable
    {
        TheReference businessObject = null;

    	{
	        LOGGER.info( "TheReferenceTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a TheReference");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a TheReference" );
	
	        try 
	        {            
	            businessObject = TheReferenceBusinessDelegate.getTheReferenceInstance().createTheReference( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (TheReferencePrimaryKey)businessObject.getTheReferencePrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a TheReference with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a TheReference.
     *
     * @return    TheReference  
     */
    public TheReference testRead()
    throws Throwable
    {
        LOGGER.info( "TheReferenceTest:testRead()" );
        LOGGER.info( "-- Reading a previously created TheReference" );

        TheReference businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read TheReference with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = TheReferenceBusinessDelegate.getTheReferenceInstance().getTheReference( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found TheReference " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a TheReference.
     *
     * @return    TheReference
     */
    public TheReference testUpdate()
    throws Throwable
    {

        LOGGER.info( "TheReferenceTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a TheReference." );

        StringBuilder msg = new StringBuilder( "Failed to update a TheReference : " );        
        TheReference businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created TheReference." );
            
            // for use later on...
            thePrimaryKey = (TheReferencePrimaryKey)businessObject.getTheReferencePrimaryKey();
            
            TheReferenceBusinessDelegate proxy = TheReferenceBusinessDelegate.getTheReferenceInstance();            
            businessObject = proxy.saveTheReference( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved TheReference - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a TheReference.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "TheReferenceTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created TheReference." );
        
        try
        {
            TheReferenceBusinessDelegate.getTheReferenceInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted TheReference with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete TheReference with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all TheReferences.
     *
     * @return    Collection
     */
    public ArrayList<TheReference> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "TheReferenceTest:testGetAll() - Retrieving Collection of TheReferences:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all TheReference : " );        
        ArrayList<TheReference> collection  = null;

        try
        {
            // call the static get method on the TheReferenceBusinessDelegate
            collection = TheReferenceBusinessDelegate.getTheReferenceInstance().getAllTheReference();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            TheReference currentBO  = null;            
	            Iterator<TheReference> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getTheReferencePrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public TheReferenceTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate TheReference
     * 
     * @return    TheReference
     */    
    protected TheReference getNewBO()
    {
        TheReference newBO = new TheReference();

// AIB : \#defaultBOOutput() 
newBO.setDateLastUpdated(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setActive(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setDateTimeLastViewedExternally(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setMakeViewableToUser(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setDateLastSent(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setNumDaysToExpire(
    new Integer( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setStatus(
    ReferenceStatus.getDefaultValue()
 );
newBO.setType(
    Purpose.getDefaultValue()
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected TheReferencePrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(TheReference.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
