/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.occulue.primarykey.*;
    import com.occulue.delegate.*;
    import com.occulue.bo.*;
    
/**
 * Test Activity class.
 *
 * @author    dev@realmethods.com
 */
public class ActivityTest
{

// constructors

    public ActivityTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Activity, through a ActivityTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ActivityTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ActivityTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Activity.
     *
     * @return    Activity
     */
    public Activity testCreate()
    throws Throwable
    {
        Activity businessObject = null;

    	{
	        LOGGER.info( "ActivityTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Activity");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Activity" );
	
	        try 
	        {            
	            businessObject = ActivityBusinessDelegate.getActivityInstance().createActivity( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ActivityPrimaryKey)businessObject.getActivityPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Activity with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Activity.
     *
     * @return    Activity  
     */
    public Activity testRead()
    throws Throwable
    {
        LOGGER.info( "ActivityTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Activity" );

        Activity businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Activity with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ActivityBusinessDelegate.getActivityInstance().getActivity( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Activity " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Activity.
     *
     * @return    Activity
     */
    public Activity testUpdate()
    throws Throwable
    {

        LOGGER.info( "ActivityTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Activity." );

        StringBuilder msg = new StringBuilder( "Failed to update a Activity : " );        
        Activity businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Activity." );
            
            // for use later on...
            thePrimaryKey = (ActivityPrimaryKey)businessObject.getActivityPrimaryKey();
            
            ActivityBusinessDelegate proxy = ActivityBusinessDelegate.getActivityInstance();            
            businessObject = proxy.saveActivity( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Activity - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Activity.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ActivityTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Activity." );
        
        try
        {
            ActivityBusinessDelegate.getActivityInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Activity with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Activity with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Activitys.
     *
     * @return    Collection
     */
    public ArrayList<Activity> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ActivityTest:testGetAll() - Retrieving Collection of Activitys:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Activity : " );        
        ArrayList<Activity> collection  = null;

        try
        {
            // call the static get method on the ActivityBusinessDelegate
            collection = ActivityBusinessDelegate.getActivityInstance().getAllActivity();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Activity currentBO  = null;            
	            Iterator<Activity> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getActivityPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ActivityTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Activity
     * 
     * @return    Activity
     */    
    protected Activity getNewBO()
    {
        Activity newBO = new Activity();

// AIB : \#defaultBOOutput() 
newBO.setRefObjId(
    new Long( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setCreateDateTime(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setType(
    ActivityType.getDefaultValue()
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ActivityPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Activity.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}
